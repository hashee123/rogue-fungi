﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestOnDrag : MonoBehaviour
{

    private Touch touch;
    private Transform position;
    private Vector3 touchPosition;
    private Rigidbody2D rb;
    public float moveSpeed = 50f;
    private Vector3 direction;
    public Text speedText;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        position = GetComponent<Transform>();
        speedText.text = "Speed: " + moveSpeed.ToString();
    }

    private void Update()
    {

        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);
            touchPosition = Camera.main.ScreenToWorldPoint(touch.position);
            touchPosition.z = 0f;
            //position.position = touchPosition;
            direction = (touchPosition - transform.position);
            rb.velocity = new Vector2(direction.x, direction.y) * moveSpeed;

            if (touch.phase == TouchPhase.Ended)
            {
                rb.velocity = Vector2.zero;
            }
        }
    }

    public void ResetPosition()
    {
        position.position = new Vector3(0f, 0f, 0f);
    }

    public void IncreaseSpeed()
    {
        moveSpeed += 10f;
        speedText.text = "Speed: " + moveSpeed.ToString();
    }

    public void DecreaseSpeed()
    {
        moveSpeed -= 10f;
        speedText.text = "Speed: " + moveSpeed.ToString();
    }

}
