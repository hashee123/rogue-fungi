﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

//BetterStreamingAssets.Initialize();

[XmlRoot(ElementName = "dialogue")]
public class Dialogue
{
    [XmlElement(ElementName = "state")]
    public DialogueState[] States;
}

[XmlRoot(ElementName = "state")]
public class DialogueState
{
    [XmlArray(ElementName = "unique"), XmlArrayItem(ElementName = "interaction")]
    public string[] Unique;

    [XmlArray(ElementName = "repeat"), XmlArrayItem(ElementName = "interaction")]
    public string[] Repeat;
}

public class XmlUtils
{
    public static T ImportXml<T>(string fileText)
    {
        try
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            //string fileText = Resources.Load<TextAsset>(path).text;
            /*#if UNITY_EDITOR
                string fileText = Resources.Load<TextAsset>(path).text;
            #elif UNITY_ANDROID
                WWW www = new WWW(path);
                while (!www.isDone) { }
                string fileText = www.text;
            #endif*/

            // Debug.Log(fileText);
            // byte[] fileContent = Encoding.UTF8.GetBytes(fileText);
            // MemoryStream memoryStream = new MemoryStream(fileContent);
	    // XmlReaderSettings settings = new XmlReaderSettings();
            // using (XmlReader reader = XmlReader.Create(memoryStream, settings))
            // {
            //     return (T)serializer.Deserialize(reader);
            // }
	    using (var reader = new System.IO.StringReader(fileText))
	    {
		return (T)serializer.Deserialize(reader);
	    }
        }
        catch (Exception e)
        {
            Debug.LogError("Exception importing xml file: " + e);
            return default;
        }
    }
}


