﻿<?xml version="1.0" encoding="utf-8"?>
<dialogue>
  <state>
    <unique>
      <interaction>Ora ora;Visitas são raras por aqui; Por favor, sinta-se à vontade</interaction>
      <interaction>Você denovo; Compania é sempre bom</interaction>
    </unique>
    <repeat>
      <interaction>Tudo está à venda;Dê uma olhada</interaction>
      <interaction>Compre alguma coisa; He he he</interaction>
    </repeat>
  </state>
  <state>
    <unique>
      <interaction>Você derrotou a cobra;Parabéns</interaction>
      <interaction>Uau;Que coisa</interaction>
    </unique>
    <repeat>
      <interaction>Compra mais ai;Hehe</interaction>
      <interaction>Capitalismo;Dinheiro</interaction>
    </repeat>
  </state>
</dialogue>
