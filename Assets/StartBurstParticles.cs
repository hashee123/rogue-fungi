﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartBurstParticles : MonoBehaviour
{
        [SerializeField] private ParticleSystem burstParticleSystem;
 
        void Awake()
        {
            burstParticleSystem = burstParticleSystem.GetComponent<ParticleSystem>();
            burstParticleSystem.Stop();
         }
 
        public void PlayBurstParticle()
        {
            burstParticleSystem.Play();
         }
}
