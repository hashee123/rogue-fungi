﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class UpgradeManager : MonoBehaviour
{
	public int cost; // custo atual do upgrade
	public Sprite[] stars; // imagem das estrelas para colocar nas cartas

	[Header("Important Scripts")]
	public DeckVisualizer deckVisualizer;

	[Header("Upgrade UI")]
	public WarningUI warningUI;
	public GameObject well; // objeto do poco com animacao
	public RectTransform wishingWellT; // transform do poco dos desejos desligado
	public GameObject cardIcon; // icone de carta
	public TextMeshProUGUI instructions; // texto com as instrucoes, para modificarmos o custo
	public Text price; // texto com o preco
	public GameObject[] slots; // objetos dos slots
	public GameObject[] slotShadows; // sombras atras dos slots que ligam apos fazermos um upgrade
	public GameObject confirmationPanel; // painel com antes e depois do upgrade
	public GameObject confirmationBackground; // background do confirmation panel
	public RectTransform beforeUpgradePosition; // transform da posicao da carta antes do upgrade
	public RectTransform afterUpgradePosition; // transform da posicao da carta depois do upgrade
	public Button upgradeButton;

	public GameObject upgradeScreen; // Para o GameUIManager para ele saber trocar do poço para o mapa
	public GameObject mapScreen; // Para o GameUIManager para ele saber trocar do poço para o mapa

	[Header("Selected Cards")]
	public string[] selectedCardNames;
	public int[] selectedCardIndex;

	private GameObject[] cardsInstantiated; // cartas criadas
	private GameObject cardBeforeUpgrade; // carta que mostra o antes
	private GameObject cardAfterUpgrade; // carta que mostra o depois
	private CardStatus selectedCard; // carta atualmente selecionada para o upgrade
	private RectTransform cardUpgradedRT; // carta que recebeu o upgrade
	public GameObject particles; // particulas ativadas quando usamos o upgrade

	public void Awake()
	{
		GameManager.Instance.upgradeManager = this;
		cardsInstantiated = new GameObject[3];

		if(GameManager.Instance.loadedCardsUpgrade == 1)
		{
			selectedCardNames = GameManager.Instance.selectedCardNames;
			selectedCardIndex = GameManager.Instance.selectedCardIndex;
			LoadSelectedCards();
			GameManager.Instance.loadedCardsUpgrade = 0;
		}
		else
		{
			selectedCardNames = new string[3]{null, null, null};
			selectedCardIndex = new int[3]{-1, -1, -1};
			ChooseUpgradeCards();
		}

		GameManager.Instance.SaveGame();
	}

	public void ConfigureInstantiatedCard(GameObject card, int index)
	{
		card.transform.localScale = new Vector3(1, 1, 1);
		card.GetComponent<IDragger>().upgradeManagerScript = this;
		card.GetComponent<Animator>().enabled = false;
		CardStatus cardStatus = card.GetComponent<CardStatus>();
		cardStatus.indexOnDeck = index;
		card.SetActive(true);
	}

	public void LoadSelectedCards()
	{
		int cardsSelected = 0;
		for (int i = 0; i < 3; i++)
		{
			if (selectedCardNames[i] != null)
			{
				Logging.Log(selectedCardNames[i]);
				GameObject cardPrefab = GameManager.GetCardFromBundle(selectedCardNames[i]);
				GameObject card = Instantiate(cardPrefab, slots[i].transform, false);
				ConfigureInstantiatedCard(card, selectedCardIndex[i]);
				cardsInstantiated[i] = card;
				cardsSelected++;
			}
		}

		if (cardsSelected == 0)	warningUI.NoCardToUpgrade();
	}
	public void ChooseUpgradeCards()
	{
		// Sorteia 3 indices de cartas para fazer upgrade
		int deckSize = DeckBehaviour.Instance.deckList.Count;
		int cardsSelected = 0;

		List<int> choices = new List<int>();
		for (int choice = 0; choice < deckSize; choice++)
		{
			choices.Add(choice);
		}

		for (int i = 0; i < 3; i++)
		{
			while (choices.Count > 0)
			{
				int choice = Random.Range(0, choices.Count);
				int index = choices[choice];
				choices.RemoveAt(choice);
				if (DeckBehaviour.Instance.levelOfEachCard[index] < 2)
				{
					// Cria e configura a carta selecionada
					GameObject card = Instantiate(DeckBehaviour.Instance.deckList[index], slots[i].transform, false);
					ConfigureInstantiatedCard(card, index);
					selectedCardNames[i] = DeckBehaviour.Instance.deckList[index].name;
					selectedCardIndex[i] = index;
					cardsInstantiated[i] = card;
					cardsSelected++;
					break;
				}
			}
		}

		if (cardsSelected == 0)	warningUI.NoCardToUpgrade();
	}

	public void ResetUI()
	{
		Logging.Log("Disable");

		for (int i = 0; i < 3; i++)
		{
			Destroy(cardsInstantiated[i]);
			slotShadows[i].SetActive(false);
		}

		//instructions.gameObject.SetActive(true);
		UpgradeButtonUsability(true);
        warningUI.Reset();
	}

	public void ShowBeforeAndAfterUpgrade(CardStatus cardStatus)
	{
		confirmationPanel.SetActive(true);
		confirmationPanel.GetComponent<CanvasGroup>().DOFade(1f, 0.3f);
		selectedCard = cardStatus;

		cardBeforeUpgrade = Instantiate(cardStatus.card.cardPrefab, beforeUpgradePosition, false);
		CardStatus cardBeforeUpgradeStatus = cardBeforeUpgrade.GetComponent<CardStatus>();
		cardBeforeUpgrade.GetComponent<Animator>().enabled = false;
		cardBeforeUpgrade.transform.localScale = new Vector3(1, 1, 1);
		IDragger cardBeforeIdragger = cardBeforeUpgrade.GetComponent<IDragger>();

		cardBeforeIdragger.cardSpriteRenderers[0].sortingOrder = 11;
		cardBeforeIdragger.cardSpriteRenderers[1].sortingOrder = 11;
		cardBeforeIdragger.cardSpriteRenderers[2].sortingOrder = 12;
		cardBeforeIdragger.cardSpriteRenderers[3].sortingOrder = 12;

		foreach (TextMeshPro textMeshProUGUI in cardBeforeIdragger.cardTexts)
			textMeshProUGUI.sortingOrder = 13;
		
		TextMeshPro[] textDescriptions = cardBeforeIdragger.cardDescriptions.GetComponentsInChildren<TextMeshPro>();
		foreach (TextMeshPro desc in textDescriptions)
			desc.sortingOrder = 13;
		
		cardBeforeIdragger.selectionAreaImage.raycastTarget = false;
		cardBeforeUpgrade.SetActive(true);

		cardAfterUpgrade = Instantiate(cardStatus.card.cardPrefab, afterUpgradePosition, false);
		CardStatus cardAfterUpgradeStatus = cardAfterUpgrade.GetComponent<CardStatus>();
		for (int i = 0; i < cardAfterUpgradeStatus.hasUpgradeOnString.Length; i++)
			if (cardAfterUpgradeStatus.hasUpgradeOnString[i])
				for (int j = 0; j < cardAfterUpgradeStatus.cardEffects.Count; j++)
					cardAfterUpgradeStatus.cardEffects[j].dhos[i] = true; // "Do Highlight On String (dhos)"Show highlight on string

		cardAfterUpgrade.GetComponent<Animator>().enabled = false;
		cardAfterUpgrade.transform.localScale = new Vector3(1, 1, 1);
		IDragger cardAfterIdragger = cardAfterUpgrade.GetComponent<IDragger>();

		cardAfterIdragger.cardSpriteRenderers[0].sortingOrder = 11;
		cardAfterIdragger.cardSpriteRenderers[1].sortingOrder = 11;
		cardAfterIdragger.cardSpriteRenderers[2].sortingOrder = 12;
		cardAfterIdragger.cardSpriteRenderers[3].sortingOrder = 12;

		foreach (TextMeshPro textMeshProUGUI in cardAfterIdragger.cardTexts)
			textMeshProUGUI.sortingOrder = 13;

		textDescriptions = cardAfterIdragger.cardDescriptions.GetComponentsInChildren<TextMeshPro>();
		foreach (TextMeshPro desc in textDescriptions)
			desc.sortingOrder = 13;

		cardAfterIdragger.selectionAreaImage.raycastTarget = false;
		cardAfterUpgrade.SetActive(true);

		UpgradeAndHighlightCard(cardAfterUpgradeStatus);
	}

	public void HideBeforeAndAfterUpgrade()
	{
        confirmationPanel.GetComponent<CanvasGroup>().DOFade(0f, 0.3f).onComplete = HideBeforeAndAfterUpgradeDelayed;
        Destroy(cardBeforeUpgrade);
        Destroy(cardAfterUpgrade);
    }

	public void HideBeforeAndAfterUpgradeDelayed()
	{
        confirmationPanel.SetActive(false); 
    }

	public void UpgradeSelectedCard()
	{
		PermanentCardUpgrade(selectedCard);
		HideBeforeAndAfterUpgrade();
	}

	public void PermanentCardUpgrade(CardStatus cardStatus)
	{
		// Usado no local de upgrades
		// Configura a carta para ser instanciada com o nivel certo
		DeckBehaviour.Instance.levelOfEachCard[cardStatus.indexOnDeck]++;

		for (int i = 0; i < 3; i++)
		{
			if (cardsInstantiated[i] == null) continue;

			IDragger cardIdragger = cardsInstantiated[i].GetComponent<IDragger>();
			cardIdragger.selectionAreaImage.raycastTarget = false;

			if (cardsInstantiated[i] != cardStatus.gameObject)
			{
				//slotShadows[i].SetActive(true);
				cardsInstantiated[i].GetComponent<Transform>().DOMoveX(700f, 2f).SetEase(Ease.InOutQuint);
            } else
			{
				cardUpgradedRT = cardsInstantiated[i].GetComponent<RectTransform>();
				Transform cardUpgradedT = cardsInstantiated[i].GetComponent<Transform>();
				/*
                // Ativa o efeito de particulas
				particles.transform.position = cardUpgradedRT.transform.position;
                particles.SetActive(true);

                // Coloca a carta upada no centro da tela
                cardsInstantiated[i].GetComponent<Transform>().DOMove(new Vector3(0f, -30f, 1f), 2f).SetEase(Ease.InOutQuint);
				cardsInstantiated[i].GetComponent<Transform>().DOScaleY(1.5f, 2f);
                cardsInstantiated[i].GetComponent<Transform>().DOScaleX(1.5f, 2f).onComplete = CardUpgradedFloatingUP;*/

				// Desce a carta selecionada
				cardUpgradedT.DOMove(new Vector3(-(cardUpgradedT.position.x), -500f, 0f), 2f).SetEase(Ease.InOutQuint);
				cardUpgradedT.DORotate(new Vector3(0f, 0f, -(cardUpgradedT.position.x)), 1.6f)
					.SetEase(Ease.InOutQuint).onComplete = CardToWishing;

            }

        }

		// Desativa o texto de instructions
		instructions.gameObject.SetActive(false);

		// Ativa o efeito de particulas
		//particles.SetActive(true);

		UpgradeCard(cardStatus); // so para aparecer a mudanca visualmente
	}

	// Manda o icon card para dentro do poco
	private void CardToWishing()
	{
		cardIcon.SetActive(true);

		Vector3[] waypoints = {
			wishingWellT.position + new Vector3(-90f, -400f, 0.0f),
			wishingWellT.position + new Vector3(-50f, -100f, 0.0f),
			wishingWellT.position
        };

        cardIcon.transform.DOPath(waypoints, 1.2f, PathType.CatmullRom).SetEase(Ease.OutQuint);
		cardIcon.GetComponent<SpriteRenderer>().DOFade(0f, 1.3f).SetEase(Ease.InQuint).onComplete = ActiveWellAnimation;
		cardIcon.transform.DORotate(new Vector3(0f, 0f, 0f), 1.2f);
		cardIcon.transform.DOScale(new Vector3(9f, 9f, 1f), 1.2f);
		
	}

	// Ativa a animacao do poco espirrando
	private void ActiveWellAnimation()
	{
        well.GetComponent<Animator>().SetBool("Upgrade", true);

		StartCoroutine(CardToSky());
    }

	// Animacao da carta indo para o ceu
	private IEnumerator CardToSky()
	{
		yield return new WaitForSeconds(0.5f);

        cardIcon.transform.position = wishingWellT.position;
		cardIcon.GetComponent<SpriteRenderer>().DOFade(255f, 0.4f).SetEase(Ease.InQuint);
        cardIcon.transform.DOMoveY(500f, 1f).onComplete = CardToCenter;	
	}

	// Animacao da carta indo para o centro da tela
	private void CardToCenter()
	{
        cardUpgradedRT.position = new Vector3(0f, 500f, 0f);
        cardUpgradedRT.rotation = new Quaternion(0f, 0f, 0f, 0f);
        cardUpgradedRT.DOScaleX(1.5f, 2f);
        cardUpgradedRT.DOScaleY(1.5f, 2f);
        cardUpgradedRT.DOMoveY(0f, 2f).onComplete = CardUpgradedFloatingUP;
    }

	// Faz a carta flutuar para cima na tela depois de ter sido escolhida
	private void CardUpgradedFloatingUP()
	{
		cardUpgradedRT.DOAnchorPosY(cardUpgradedRT.anchoredPosition.y + 100f, 3f).SetEase(Ease.InOutSine).onComplete = CardUpgradedFloatingDOWN;
	}

	// Faz a carta flutuar para baixo
	private void CardUpgradedFloatingDOWN()
	{
        cardUpgradedRT.DOAnchorPosY(cardUpgradedRT.anchoredPosition.y - 100f, 3f).SetEase(Ease.InOutSine).onComplete = CardUpgradedFloatingUP;
    }

	private void UpgradeAndHighlightCard(CardStatus cardStatus)
	{
		// Usado para fazer o upgrade real das cartas
		Logging.Log("Upgrade com highlight");
		cardStatus.level++;
		cardStatus.stars.sprite = stars[cardStatus.level];
		foreach (CardsEffects effect in cardStatus.cardEffects)
			effect.Upgrade(cardStatus.level, true);
	}

	public static void UpgradeCard(CardStatus cardStatus)
	{
		// Usado para fazer o upgrade real das cartas
		Logging.Log("Upgrade realizado");
		cardStatus.level++;
		// DeckBehaviour.Instance.levelOfEachCard[cardStatus.indexOnDeck]++;
		cardStatus.stars.sprite = GameManager.GetSpriteFromBundle((cardStatus.level + 1) + "_Stars");
		foreach (CardsEffects effect in cardStatus.cardEffects)
			effect.Upgrade(cardStatus.level, false);
	}

	public void DestroyInstantiatedCards()
	{
		foreach (GameObject card in cardsInstantiated) Destroy(card);
	}

	public void BackToMap() 
	{
		GameUIManager.Instance.FromUpgradeScreenToMapScreen();
	}

	// Usado para desativar a funcao de botao para nao clicar duas vezes
	public void UpgradeButtonUsability(bool active)
	{
		upgradeButton.enabled = active;
	}
}
