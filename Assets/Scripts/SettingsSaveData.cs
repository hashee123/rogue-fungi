using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class SettingsSaveData
{
	public static SettingsSaveData Instance { get; set; }

	public int qualitySettings = 2;
	public int languageSettings = 0;
	public float volumeGeneralSettings = 1f;
	public float volumeSoundEffectsSettings = 0.5f;
	public float volumeMusicSettings = 0.5f;
}
