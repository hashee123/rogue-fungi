using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class Logging
{
	[System.Diagnostics.Conditional("ENABLE_LOG")]
	static public void Log(object message)
	{
		UnityEngine.Debug.Log(message);
	}

	[System.Diagnostics.Conditional("ENABLE_LOG")]
	static public void Log(object message1, UnityEngine.Object message2)
	{
		UnityEngine.Debug.Log(message1, message2);
	}

	[System.Diagnostics.Conditional("ENABLE_LOG")]
	static public void LogError(object message)
	{
		UnityEngine.Debug.LogError(message);
	}

	[System.Diagnostics.Conditional("ENABLE_LOG")]
	static public void LogWarning(object message)
	{
		UnityEngine.Debug.LogWarning(message);
	}
}
