using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Components;
using UnityEngine.Localization.Tables;

public static class EffectsExplainerTable
{
	public static string[] tableEntries;

	public static bool IsInTable(string effectName)
	{
		for (int i = 0; i < tableEntries.Length; i++)
		{
			if (effectName == tableEntries[i])
			{
				return true;
			}
		}

		return false;
	}
}
