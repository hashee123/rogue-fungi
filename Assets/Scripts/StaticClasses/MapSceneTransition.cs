using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MapSceneTransitionData
{
	public int actualPlace = -1;
	public int actualMap = -1;
	public bool alreadyGotInformation = false;
	public int mapSize;
	public bool[] placesOnMapEnabled = null;
	public bool needToEnableNextPlace = false;
	public bool[] visitedPlaces = null;
}

public static class MapSceneTransition
{
	public static MapSceneTransitionData mapSceneTransitionData = new MapSceneTransitionData();

	public static void ResetClass()
	{
		mapSceneTransitionData.actualPlace = -1;
		mapSceneTransitionData.actualMap = -1;
		mapSceneTransitionData.alreadyGotInformation = false;
		mapSceneTransitionData.mapSize = 0;
		mapSceneTransitionData.placesOnMapEnabled = null;
		mapSceneTransitionData.needToEnableNextPlace = false;
	}
}
