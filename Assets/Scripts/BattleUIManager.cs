﻿/* Autor: Klinsmann Silva Hengles Cordeiro
   Agosto de 2020
   Programa que cuida da UI durante a batalha 
*/

using System.Threading;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.Localization.Components;
using DG.Tweening;

public class BattleUIManager : MonoBehaviour
{
	[Header("General")]
	public Transform rightEdgeOfScreen;
	public BattleUIParticles battleUIParticles;
	public MapBehaviour mapBehaviourScript;
	public BattleManager battleManagerScript;
	public Image[] playerCardSlotsImages;
	public Image playerMagicCircle;
	public Text textDeckNumberofCards; // texto que vai mostrar o numero de cartas no deck
	public Transform graveyardIconEndPosition;
	public ButtonJuicy deckButtonJuice;
	public Text textGraveyardNumberOfCards; // texto com o numero de cartas
	public Transform graveyardIconSpawnPosition;
	public ButtonJuicy textGraveyardButtonJuice;
	public ButtonJuicy graveyardIconButtonJuice;
	public GameObject winScreen; // tela de vitoria
	public GameObject statRewardsScreen; // tela de recompensa em xp e cogumelos
	public GameObject loseScreen; // tela de derrota
	public GameObject endScreen; // tela de fim de jogo
	public GameObject mapUI;
	public GameObject initialGameUI;
	public GameObject battleUI;
	public GameObject rewardUI;
	public RewardManager rewardManagerScript;
	public GameObject eventEffectLayout;
	public Text eventEffectLayoutText;
	public WarningUI warningUI; // UI que mostra avisos, como Sem pontos de acao o suficiente
	public GameObject turnTransitionObject;
	public LocalizeStringEvent localizedTurnTransitionString;
	public AnimationClip turnAnimationClip;
	public Camera battleCamera;
	public Text gainedShrooms;
	public Text gainedXP;
	public Text gainedShroomsRun;
	public Text gainedXPRun;
	public GameObject cardIcon;
	public GameObject graveyardCardIcon;

	public int thisBattleShrooms;
	public int thisBattleXP;

	public GameObject blackImage; // image to make other thins darker on victory screen

	[Header("Effect Explainer")]
	public EffectsExplainer[] cardsEffectsExplainers;

	[Header("Slots")]
	public Animator slotLeftExplosion;
	public Animator slotCenterExplosion;
	public Animator slotRightExplosion;

	[Header("Player")]
	public Text playerLifeTextUI; // vida do jogador na tela
	/*public Text playerPoisonTextUI; // poison no jogador na tela
	  public Text playerDefenseTextUI; // defesa do jogador na tela*/
	public TextMeshProUGUI playerActionPointsText; // texto que mostra os pontos de acao do jogador

	public Animator actionPointsAnimator; // animator da caixa que contem pontos de acao
	public Animator healthBarAnimator; // animator da barra de vida do player
	public Animator bloodBorderAnimator; // animator da borda de sangue quando player toma dano

	/*public GameObject bonusDamageUI; // simbolo de bonus de dano
	  public Text bonusDamageText; // texto do bonus de dano
	  public GameObject damageMultiplierUI; // simbolo de multiplicador de dano
	  public Text damageMultiplierText; // texto do multiplicador de dano*/

	/*public GameObject[] playerStatusEffects;
	  public Text[] playerStatusTexts;*/
	public GameObject[] slotBlockOverlay;

	public GameObject playerStatusPanel;
	public PlayerStatusIcon[] playerIcons;

	public bool isSettingIcon = false;

	[Header("Player Status Images")]
	public Sprite playerPoison;
	public Sprite playerClumsiness;
	public Sprite playerTiredness;
	public Sprite playerInflation;
	public Sprite playerCurse;
	public Sprite playerDestruction;
	public Sprite playerProtection;
	public Sprite playerBonusDamage;
	public Sprite playerDamageMultiplier;

	[Header("Enemy")]
	public Text enemyLifeTextUI; // vida do inimigo na tela
	public Text enemyPoisonTextUI; // poison que o inimigo está
	public HealthBar enemyHealthBar;
	public PlayerHealthBar playerHealthBar;
	public GameObject dmgTakenText; // texto do dano sofrido do inimigo
	public GameObject[] EnemyDebuffSlots; // Efeitos como veneno causado pelo player
	public GameObject explosion; // explosao dos sparks
	public GameObject explosion2; // itens da explosao que rodam e sao mais longos

	public PlayerStatusIcon[] enemyIcons;
	public ParticleSystem shieldEnemyParticles;
	public ParticleSystem poisonEnemyParticles;

	[Header("Saving Sequence")]
	public Image relicUsedImage; // reliquia sendo usada para salvar o inimigo
	public GameObject savingDialogueUI; // Tela com dialogo quando vc salva o inimigo
	public Text savingDialogueText; // texto de agradecimento
	public Image relicDroppedImage; // imagem da reliquia droppada

	/*[Header("Stats")]
	  public Image statsBackground;
	  public Text statsTextSlot;*/
	[HideInInspector] public GameObject cardOn; // a carta da qual vai ser pega a descricao pra mostrar no statsTextSlot

	[Header("Colors")]
	public Color color_PiercingSlot;
	public Color color_PoisonSlot;
	public Color color_StrengthSlot;
	public Color color_ProtectionSlot;
	public Color color_DefaultSlot;

	[Header("ActionSlotSprites")]
	public Sprite genericSprite;
	public Sprite actionSlotAttack;
	public Sprite actionSlotProtect;
	public Sprite actionSlotClumsiness; // Bagunca
	public Sprite actionSlotSpecialProtection;
	public Sprite actionSlotDestruction;
	public Sprite actionSlotFatigue;
	public Sprite actionSlotNastyPresent;
	public Sprite actionSlotBlocked;
	public Sprite actionSlotGainStrength;
	public Sprite actionSlotCurse;
	public Sprite actionSlotPoison;

	[Header("EnemyDebuffSlotSprites")]
	public Sprite poisonDebuff;

	[Header("EnemyBuffSlotSprites")]
	public Sprite protectionBuff;
	public Sprite swordBuff;

	[Header("Mutexes")]
	private static Mutex enemyMutex = new Mutex();
	private static Mutex playerMutex = new Mutex();

	void Awake()
	{
		GameManager.Instance.battleUIManager = this;
		PlayerBehaviour.Instance.battleUIManagerScript = this;
		PlayerBehaviour.Instance.healthBar = playerHealthBar;
		PlayerBehaviour.Instance.playerHealthBarAnimator = healthBarAnimator;
	}

	void Start()
	{
		ShowLifePoints();
		foreach (GameObject debuff in EnemyDebuffSlots)  debuff.SetActive(false);
	}

	public Color GetColorFromMod(CardModifier mod)
	{
		switch(mod)
		{
			case CardModifier.FISICAL_DMG:
				return color_StrengthSlot;
			case CardModifier.POISON:
				return color_PoisonSlot;
			case CardModifier.PROTECTION:
				return color_ProtectionSlot;
			case CardModifier.PIERCING:
				return color_PiercingSlot;
			default:
				return color_DefaultSlot;
		}
	}

	public void ShowGraveyardNumberCards()
	{
		textGraveyardNumberOfCards.text = DeckBehaviour.Instance.graveyardCards.Count.ToString();
	}

	public void ShowDeckNumberCards()
	{
		textDeckNumberofCards.text = DeckBehaviour.Instance.deckInstantiatedQueue.Count.ToString();
	}

	public void ShowActionPoints()
	{
		playerActionPointsText.text = PlayerBehaviour.Instance.playerBehaviourData.actionPointsCurrent.ToString() + "/" + PlayerBehaviour.Instance.playerBehaviourData.actionPointsInitial.ToString();
	}
	
	public void ShowLifePoints()
	{
		Logging.Log("playerLifeTextUI.text: ");
		Logging.Log(playerLifeTextUI.text);
		Logging.Log("playerLifeTextUI.text: terminei");
		playerLifeTextUI.text = PlayerBehaviour.Instance.playerBehaviourData.lifeCurrent.ToString() + "/" + PlayerBehaviour.Instance.playerBehaviourData.lifeInitial.ToString();
		playerHealthBar.SetHealth(PlayerBehaviour.Instance.playerBehaviourData.lifeCurrent);
	}

	public void ShowEnemyLifePoints()
	{
		int enemyLifePoints = battleManagerScript.enemyUnitScript.enemyUnitData.actualHealth;
		int enemyTotalLifePoints = battleManagerScript.enemyUnitScript.enemyUnitData.originalHealth;
		enemyLifeTextUI.text = enemyLifePoints.ToString() + "/" + enemyTotalLifePoints.ToString();
	}

	public void ShowEnemyPoison()
	{
		int poison = battleManagerScript.enemyBehaviourScript.thisEnemyUnit.enemyUnitData.actualPoison;
		StartCoroutine(SearchAndSetEnemyStatusIcon("enemyPoison", poison, playerPoison));
	}

	public void ShowEnemyProtection()
	{
		int protection = battleManagerScript.enemyBehaviourScript.thisEnemyUnit.enemyUnitData.actualProtection;
		StartCoroutine(SearchAndSetEnemyStatusIcon("enemyProtection", protection, playerProtection));

		if (protection > 0)
		{
			shieldEnemyParticles.Clear();
			shieldEnemyParticles.Play();
		}
	}

	public void ShowEnemySpecialProtection()
	{
		int specialProtection = battleManagerScript.enemyBehaviourScript.thisEnemyUnit.enemyUnitData.actualSpecialProtection;
		StartCoroutine(SearchAndSetEnemyStatusIcon("enemySpecialProtection", specialProtection, playerProtection));
	}

	public void SetGainedStrengthUI()
	{
		int strength = battleManagerScript.enemyUnitScript.enemyUnitData.gainedStrength;
		StartCoroutine(SearchAndSetEnemyStatusIcon("enemyStrength", strength, playerBonusDamage));
	}
	public IEnumerator SearchAndSetEnemyStatusIcon(string statusName, int valueToSet, Sprite statusImage)
	{
		enemyMutex.WaitOne();

		if (valueToSet > 0)
		{
			PlayerStatusIcon statusIconFound = null;
			foreach (PlayerStatusIcon statusIcon in enemyIcons)
			{
				if (statusIcon.isBeingUsed)
				{
					if (statusIcon.effectName == statusName)
					{
						statusIconFound = statusIcon;
						statusIcon.UpdateText(valueToSet.ToString());
						break;
					}
				}
				else
				{
					statusIconFound = statusIcon;
					statusIcon.EnableStatusIcon(statusName, valueToSet.ToString(), statusImage);
					break;
				}
			}

			FindAndDisableDuplicates(statusIconFound, enemyIcons);
		}
		else foreach (PlayerStatusIcon statusIcon in enemyIcons)
				if (statusIcon.effectName == statusName)  statusIcon.DisableStatusIcon();

		enemyMutex.ReleaseMutex();
		yield break;
	}

	public void ShowEnemyStatus()
	{
		ShowEnemyLifePoints();
		ShowEnemyPoison();
		ShowEnemyProtection();
		ShowEnemySpecialProtection();
		SetGainedStrengthUI();
	}

	public void ShowPlayerStatus()
	{
		ShowLifePoints();
		ShowPlayerProtection();
		ShowPlayerTiredness();
		ShowPlayerClumsiness();
		ShowPlayerInflation();
		ShowPlayerCurse();
		ShowDestruction();
		ShowPlayerBonusDamage();
		ShowPlayerDamageMultiplier();
	}

	public void DisableAllPlayerStatusIcons()
	{
		foreach (PlayerStatusIcon statusIcon in playerIcons)
		{
			statusIcon.isBeingUsed = false;
			statusIcon.effectName = "statusIcon";
			statusIcon.gameObject.SetActive(false);
		}
	}

	void FindAndDisableDuplicates(PlayerStatusIcon statusIcon, PlayerStatusIcon[] iconSet)
	{
		foreach (PlayerStatusIcon icon in iconSet)
		{
			if (icon.effectName == statusIcon.effectName && icon != statusIcon)
				icon.DisableStatusIcon();
		}
	}

	public IEnumerator SearchAndSetStatusIcon(string statusName, int valueToSet, Sprite statusImage, int baselineValue=0)
	{
		playerMutex.WaitOne();

		if (valueToSet > baselineValue)
		{
			PlayerStatusIcon statusIconFound = null;
			foreach (PlayerStatusIcon statusIcon in playerIcons)
			{
				if (statusIcon.isBeingUsed)
				{
					if (statusIcon.effectName == statusName)
					{
						statusIconFound = statusIcon;
						statusIcon.UpdateText(valueToSet.ToString());
						break;
					}
				}
				else
				{
					statusIconFound = statusIcon;
					statusIcon.EnableStatusIcon(statusName, valueToSet.ToString(), statusImage);
					break;
				}
			}

			FindAndDisableDuplicates(statusIconFound, playerIcons);
		}
		else foreach (PlayerStatusIcon statusIcon in playerIcons)
				if (statusIcon.effectName == statusName)
					statusIcon.DisableStatusIcon();

		playerMutex.ReleaseMutex();
		yield break;
	}

	public void DisableAllPlayerIcons()
	{
		foreach (PlayerStatusIcon statusIcon in playerIcons)  statusIcon.DisableStatusIcon();
	}

	public void DisableAllEnemyIcons()
	{
		foreach (PlayerStatusIcon statusIcon in enemyIcons)  statusIcon.gameObject.SetActive(false);
	}

	public void ShowPlayerProtection()
	{
		StartCoroutine(SearchAndSetStatusIcon("playerProtection", PlayerBehaviour.Instance.playerBehaviourData.defenseCurrent, playerProtection));

		if (PlayerBehaviour.Instance.playerBehaviourData.defenseCurrent > 0)
		{
			battleUIParticles.playerShieldParticles.Clear();
			battleUIParticles.playerShieldParticles.Play();
		}
	}

	public void ShowPlayerClumsiness()
	{
		StartCoroutine(SearchAndSetStatusIcon("playerClumsiness", PlayerBehaviour.Instance.playerBehaviourData.clumsinessCurrent, playerClumsiness));
	}

	public void ShowPlayerTiredness()
	{
		StartCoroutine(SearchAndSetStatusIcon("playerTiredness", PlayerBehaviour.Instance.playerBehaviourData.tirednessTurns, playerTiredness));
	}

	public void ShowPlayerInflation()
	{
		int inflationCurrent = PlayerBehaviour.Instance.playerBehaviourData.inflationCurrent;
		StartCoroutine(SearchAndSetStatusIcon("playerInflation", inflationCurrent, playerInflation));
	}

	public void ShowPlayerCurse()
	{
		StartCoroutine(SearchAndSetStatusIcon("playerCurse", PlayerBehaviour.Instance.playerBehaviourData.curseCurrent, playerCurse));
	}

	public void ShowDestruction()
	{
		StartCoroutine(SearchAndSetStatusIcon("playerDestruction", battleManagerScript.destructionLasts, playerDestruction));
	}

	public void ShowPlayerBonusDamage()
	{
		StartCoroutine(SearchAndSetStatusIcon("playerBonusDamage", battleManagerScript.bonusDamage, playerBonusDamage));
	}

	public void ShowPlayerDamageMultiplier()
	{
		StartCoroutine(SearchAndSetStatusIcon("playerDamageMultiplier", battleManagerScript.damageMultiplierNextAttack, playerDamageMultiplier, 1));
	}

	public void DestroyCard(int index)
	{
		switch (index)
		{
			case 0:
				slotLeftExplosion.SetTrigger("Explode");
				break;
			case 1:
				slotCenterExplosion.SetTrigger("Explode");
				break;
			case 2:
				slotRightExplosion.SetTrigger("Explode");
				break;
		}
	}
	public void DestroyCards()
	{
		slotLeftExplosion.SetTrigger("Explode");
		slotCenterExplosion.SetTrigger("Explode");
		slotRightExplosion.SetTrigger("Explode");
	}

	public IEnumerator DrainActionPoints()
	{
		actionPointsAnimator.SetTrigger("DrainAction");
		yield return new WaitForSeconds(0.3f);
	}

	public void DecreaseSlotTransparency()
	{
		battleManagerScript.objectReferences.slots[0].GetComponent<CanvasGroup>().alpha = 0.6f;
		battleManagerScript.objectReferences.slots[1].GetComponent<CanvasGroup>().alpha = 0.6f;
		battleManagerScript.objectReferences.slots[2].GetComponent<CanvasGroup>().alpha = 0.6f;
	}

	public void IncreaseSlotTransparency()
	{
		battleManagerScript.objectReferences.slots[0].GetComponent<CanvasGroup>().alpha = 1f;
		battleManagerScript.objectReferences.slots[1].GetComponent<CanvasGroup>().alpha = 1f;
		battleManagerScript.objectReferences.slots[2].GetComponent<CanvasGroup>().alpha = 1f;
	}

	public void PlayerWon()
	{
        TakeCardsOffTheScreen();
		blackImage.SetActive(true);
		blackImage.GetComponent<Image>().DOFade(0.68f, 3f).SetEase(Ease.OutQuad);

        // mostra a tela de vitoria na tela
        winScreen.GetComponent<VictoryVisualEvents>().StartAnimation();
        // winScreen.transform.SetAsLastSibling();
    }

	public void GameWon()
	{
		battleUI.SetActive(false); // desativa a tela de batalha		
		endScreen.SetActive(true); // mostra a tela de fim de jogo
	}

	public void PlayerLose()
	{
		loseScreen.GetComponent<DefeatVisualEvents>().StartAnimation(); // mostra a tela de derrota
		// loseScreen.transform.SetAsLastSibling();
	}
	
	// Used after a victory
	void TakeCardsOffTheScreen()
	{
		DeckBehaviour.Instance.onTable[0].GetComponent<RectTransform>().DOMoveY(-500f, 1f).SetEase(Ease.OutCirc);
        DeckBehaviour.Instance.onTable[1].GetComponent<RectTransform>().DOMoveY(-500f, 1f).SetEase(Ease.OutCirc);
        DeckBehaviour.Instance.onTable[2].GetComponent<RectTransform>().DOMoveY(-500f, 1f).SetEase(Ease.OutCirc);
    }

	int ContinueButtonAfterTransition()
	{
		// Limpa tudo da batalha
		GameManager.Instance.state = GameState.REWARD_SCREEN;
		battleManagerScript.FinishBattle();

		// Prepara o mapa  
		MapSceneTransition.mapSceneTransitionData.needToEnableNextPlace = true;
		MapSceneTransition.mapSceneTransitionData.actualPlace = GameManager.Instance.actualPlace;

		RewardData.rewardType = 1;
		SceneManager.LoadScene("RewardScene");
		return 0;
	}

	int RestartRunButtonAfterTransition()
	{
		// Limpa tudo da batalha
		GameManager.Instance.state = GameState.SHOP_SCREEN;
		battleManagerScript.FinishBattle();

		SceneManager.LoadScene("ShopScene");
		return 0;
	}

	public void RestartRunButton()
	{
		StartCoroutine(GameManager.Instance.waitForTransition(GameManager.Instance.transitionObjectAnimator, RestartRunButtonAfterTransition));
	}

	public void ContinueButton()
	{
		StartCoroutine(GameManager.Instance.waitForTransition(GameManager.Instance.transitionObjectAnimator, ContinueButtonAfterTransition));
	}

	public void BattleWonButton()
	{
		if(!GameManager.Instance.boss)
		{
			winScreen.SetActive(false);
			statRewardsScreen.SetActive(true);
			statRewardsScreen.transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
			statRewardsScreen.transform.DOScale(new Vector3(1.21f, 1.21f, 1.21f), 0.5f).SetEase(Ease.OutBounce);
			gainedShrooms.DOText(thisBattleShrooms.ToString(), 1.25f, true, ScrambleMode.Numerals);
			gainedXP.DOText(thisBattleXP.ToString(), 1.25f, true, ScrambleMode.Numerals);
		}
		else
		{
			winScreen.SetActive(false);
			endScreen.SetActive(true);
			int xpGanho = PlayerBehaviour.Instance.playerBehaviourData.xp - PlayerBehaviour.Instance.playerBehaviourData.xpRunStart + thisBattleShrooms;
			int cogumelosGanhos = PlayerBehaviour.Instance.playerBehaviourData.coins - PlayerBehaviour.Instance.playerBehaviourData.coinsRunStart + thisBattleXP;
			statRewardsScreen.transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
			statRewardsScreen.transform.DOScale(new Vector3(1.21f, 1.21f, 1.21f), 0.5f).SetEase(Ease.OutBounce);
			gainedShroomsRun.DOText(xpGanho.ToString(), 1.25f, true, ScrambleMode.Numerals);
			gainedXPRun.DOText(cogumelosGanhos.ToString(), 1.25f, true, ScrambleMode.Numerals);
		}
	}

	public IEnumerator ShowSavingSequence(Sprite relicUsed, Sprite relicDropped, float relicInterval, float transformationInterval)
	{
		EnemyDisplay enemyDisplayScript = battleManagerScript.enemyDisplayScript;

		relicUsedImage.sprite = relicUsed;
		relicUsedImage.transform.SetAsLastSibling();
		relicUsedImage.gameObject.SetActive(true);
		yield return new WaitForSeconds(relicInterval);
		relicUsedImage.gameObject.SetActive(false);

		foreach (GameObject obj in enemyDisplayScript.objectsToDeactivate)
		{
			obj.SetActive(false);
			yield return new WaitForSeconds(transformationInterval);
		}

		foreach (GameObject obj in enemyDisplayScript.objectsToActivate)
		{
			obj.SetActive(true);
			yield return new WaitForSeconds(transformationInterval);
		}

		savingDialogueText.text = battleManagerScript.enemyUnitScript.enemyUnitData.savingDialogue;
		relicDroppedImage.sprite = relicDropped;
		savingDialogueUI.transform.SetAsLastSibling();
		savingDialogueUI.SetActive(true);
	}

	public void SaveBossButton()
	{
		// Limpa tudo da batalha
		battleManagerScript.FinishBattle();
		savingDialogueUI.SetActive(false);
		battleUI.gameObject.SetActive(false);
		initialGameUI.gameObject.SetActive(true);
		
		// Gera novo mapa 
		GameManager.Instance.state = GameState.INITIAL_SCREEN;
		mapBehaviourScript.InitializeMap(true);
		GameManager.Instance.SaveGame();
	}

	public void RestartButton()
	{
		GameObject.Destroy(DeckBehaviour.Instance.gameObject);
		GameObject.Destroy(PlayerBehaviour.Instance.gameObject);
		GameObject.Destroy(battleManagerScript.gameObject);
		MapSceneTransition.ResetClass();
		GameManager.Instance.ResetSaveFile();
		SceneManager.LoadScene("MenuScene");
	}

	public void RestartButtonNoResetSave()
	{
		GameObject.Destroy(DeckBehaviour.Instance.gameObject);
		GameObject.Destroy(PlayerBehaviour.Instance.gameObject);
		GameObject.Destroy(battleManagerScript.gameObject);
		MapSceneTransition.ResetClass();
	}

	public void BackToMenuScene()
	{
		GameManager.Instance.SaveGame();
		GameManager.Instance.SaveSettingsData();
		GameManager.Instance.state = GameState.INITIAL_SCREEN;
		RestartButtonNoResetSave();
		SceneManager.LoadScene("MenuScene");
	}

	public void StartTurnTransitionAnimation(string entry)
	{
		localizedTurnTransitionString.gameObject.SetActive(true);
		localizedTurnTransitionString.StringReference.SetReference("UI", entry);
		turnTransitionObject.SetActive(true);
	}

	public void DisableTurnTransitionAnimation()
	{
		localizedTurnTransitionString.gameObject.SetActive(false);
		turnTransitionObject.SetActive(false);
	}

    private static void FadePlayerCards(float endValue, float duration, bool animatorBool)
    {
        foreach (GameObject o in DeckBehaviour.Instance.onTable)
        {
            o.GetComponent<Animator>().enabled = animatorBool;
            IDragger idragger = o.GetComponent<IDragger>();
            foreach (SpriteRenderer s in idragger.cardSpriteRenderers)
            {
                s.DOFade(endValue, duration);
            }

            foreach (TextMeshPro t in idragger.cardTexts)
            {
                t.DOFade(endValue, duration);
            }

			TextMeshPro[] descriptionTexts = idragger.cardDescriptions.GetComponentsInChildren<TextMeshPro>();
			foreach (TextMeshPro description in descriptionTexts)
			{
				description.DOFade(endValue, duration);
			}
        }
    }

	public void OnOvershadowPlayerTable()
    {
        foreach (Image img in playerCardSlotsImages)
        {
            img.DOFade(0.3f, 0.3f);
        }

		FadePlayerCards(0.3f, 0.3f, false);

        playerMagicCircle.DOFade(0.0f, 0.3f);
    }

    public void OffOvershadowPlayerTable()
	{
		foreach (Image img in playerCardSlotsImages)
		{
			img.DOFade(1.0f, 0.3f);
		}

		FadePlayerCards(1.0f, 0.3f, true);

		playerMagicCircle.DOFade(1.0f, 0.3f);
	}

	public void ResetDebuffSlots()
	{
		foreach (GameObject debuffSlot in EnemyDebuffSlots)	debuffSlot.SetActive(false);
	}

	public void ShowSlotDescription(int index, bool toShow)
	{
		SlotBehaviour slotBehaviour = battleManagerScript.objectReferences.slots[index].GetComponent<SlotBehaviour>();

		switch(slotBehaviour.slot) 
		{
			case SlotPosition.LEFT:
				battleManagerScript.objectReferences.slotLeftDescriptionImage.SetActive(toShow);
				break;

			case SlotPosition.CENTER:
				battleManagerScript.objectReferences.slotCenterDescriptionImage.SetActive(toShow);
				break;
			case SlotPosition.RIGHT:
				battleManagerScript.objectReferences.slotRightDescriptionImage.SetActive(toShow);
				break;
		}
	}

	public void ResetSlotDescriptions()
	{
		battleManagerScript.objectReferences.slotLeftDescription.text = "Sem Modificador";
		battleManagerScript.objectReferences.slotCenterDescription.text = "Sem Modificador";
		battleManagerScript.objectReferences.slotRightDescription.text = "Sem Modificador";
	}

	public void DisableExplosions()
	{
		explosion.SetActive(false);
		explosion2.SetActive(false);
		dmgTakenText.SetActive(false);
	}

	public void TakeDamage(float damageTaken)
	{
		battleCamera.DOShakePosition(0.4f, damageTaken * 1.3f, 15, 70, true, ShakeRandomnessMode.Harmonic);
		PlayerBehaviour.Instance.playerHealthBarAnimator.SetTrigger("TakeDamage");
	}

	public void ActivateEffectLayout(string displayText)
	{
		eventEffectLayout.SetActive(true);
		eventEffectLayoutText.text = displayText;
	}

	public void DeactivateEffectLayout()
	{
		eventEffectLayout.SetActive(false);
	}
}
