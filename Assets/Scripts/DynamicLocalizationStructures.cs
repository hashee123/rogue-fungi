using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization;

[System.Serializable]
public class DynamicLocaleText
{
	public string name;
	public Locale localeName;
	[TextArea(3,3)]
	public string text;
}

[System.Serializable]
public class DynamicLocaleDialogue
{
	public string name;
	public Locale localeName;
	[TextArea(3,3)]
	public string[] text;
}