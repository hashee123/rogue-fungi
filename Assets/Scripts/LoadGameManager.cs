﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.SceneManagement;

public class LoadGameManager : MonoBehaviour
{
	SaveData dataToLoad = null;

	public void SetupLoadGame()
	{
		if (!File.Exists(Application.persistentDataPath + "/SaveData.dat"))
		{
			Logging.LogError("There is no save data!");
			return;
		}

		BinaryFormatter binaryFormatter = new BinaryFormatter();
		FileStream saveFile = File.Open(Application.persistentDataPath + "/SaveData.dat", FileMode.Open);
		dataToLoad = (SaveData)binaryFormatter.Deserialize(saveFile);
		saveFile.Close();
	}

	public SaveData GetDataToLoad()
	{
		return dataToLoad;
	}

	public int LoadGame()
	{
		if (dataToLoad == null)
		{
			Logging.LogError("Didn't load save file");
			return -1;
		}

		LoadGeneralData();

		switch (dataToLoad.gameState)
		{
			case GameState.MAP_SCREEN:
				LoadMapData();
				SceneManager.LoadScene("MapScene");
				break;

			case GameState.BATTLE_SCREEN:
				LoadBattleData();
				SceneManager.LoadScene("BattleScene");
				break;

			case GameState.CHEST_SCREEN:
				MapSceneTransition.mapSceneTransitionData.needToEnableNextPlace = true;
				RewardData.rewardType = 0;
				LoadRewardData();
				SceneManager.LoadScene("RewardScene");
				break;

			case GameState.REWARD_SCREEN:
				RewardData.rewardType = 1;
				LoadRewardData();
				SceneManager.LoadScene("RewardScene");
				break;

			case GameState.SHOP_SCREEN:
				LoadShopData();
				SceneManager.LoadScene("ShopScene");
				break;

			case GameState.UPGRADE_SCREEN:
				LoadUpgradeData();
				SceneManager.LoadScene("UpgradeScene");
				break;

			case GameState.REST_SCREEN:
				SceneManager.LoadScene("RestScene");
				break;
		}

		MenuData.justCameFromMenu = true;
		Logging.Log("Game data loaded!");
		return 0;
	}

	public void LoadRewardData()
	{
		GameManager.Instance.battleRewardCards = GameManager.Instance.
			actualMap.placeUnits[GameManager.Instance.actualPlace].rewardSetup.cards;
		GameManager.Instance.selectedCards = dataToLoad.rewardCardNames;
	}

	public void LoadShopData()
	{
		ShopData.shopCards = dataToLoad.shopCards;
		CauldronData.cauldronBuffs = dataToLoad.cauldronBuffs;
	}

	public void LoadUpgradeData()
	{
		GameManager.Instance.selectedCardNames = dataToLoad.upgradeCardNames;
		GameManager.Instance.selectedCardIndex = dataToLoad.upgradeCardIndex;
		GameManager.Instance.loadedCardsUpgrade = 1;
	}

	public void LoadMapData()
	{
		GameManager.Instance.actualMap = GameManager.Instance.mapSetupPool.maps[dataToLoad.mapNumber];
		GameManager.Instance.mapNumber = dataToLoad.mapNumber;
		GameManager.Instance.actualPlace = dataToLoad.placeIndexCurrent;
		GameManager.Instance.battleSetupPool.difficulties = dataToLoad.battleSetupPoolDifficulties;
		GameManager.Instance.battleSetupPool.indexes = dataToLoad.battleSetupPoolIndexes;

		// Classe estatica MapSceneTransition
		MapSceneTransition.mapSceneTransitionData = dataToLoad.mapSceneTransitionData;
	}

	private void LoadGeneralData()
	{
		GameManager.Instance.currentData = dataToLoad; // informacoes atuais
		GameManager.Instance.state = dataToLoad.gameState; // Estado do jogo
		GameManager.Instance.mapNumber = dataToLoad.mapNumber;
		if(dataToLoad.mapNumber >= 0)	GameManager.Instance.actualMap = GameManager.Instance.mapSetupPool.maps[dataToLoad.mapNumber];
		GameManager.Instance.actualPlace = dataToLoad.placeIndexCurrent;

		// Estado do player
		LoadPlayerData();

		// Estado do deck
		LoadDeckData();

		// Classe estatica MapSceneTransition
		MapSceneTransition.mapSceneTransitionData = dataToLoad.mapSceneTransitionData;
	}

	private void LoadPlayerData()
	{
		PlayerBehaviour.Instance.playerBehaviourData = dataToLoad.playerBehaviourData;
		if (dataToLoad.playerRelicHolding != null)
		{
			Relic holdingRelic = GameManager.GetRelicFromBundle(dataToLoad.playerRelicHolding);
			PlayerBehaviour.Instance.SetRelic(holdingRelic);
		}
	}

	private void LoadDeckData()
	{
		DeckBehaviour.Instance.levelOfEachCard = dataToLoad.levelOfEachCard;
		DeckBehaviour.Instance.deckList = new List<GameObject>();
		for (int i = 0; i < dataToLoad.deckList.Count; i++)	
			DeckBehaviour.Instance.deckList.Add(GameManager.GetCardFromBundle(dataToLoad.deckList[i]));
	}

	private void LoadBattleData()
	{
		// Entrando na batalha
		int tmpDiff = GameManager.Instance.battleSetupPool.difficulties[GameManager.Instance.actualPlace];
		int tmpIndex = GameManager.Instance.battleSetupPool.indexes[GameManager.Instance.actualPlace];
		GameManager.Instance.currentBattleSetup = GameManager.Instance.getBattleSetup(tmpDiff, tmpIndex);
		GameManager.Instance.enemyMax = dataToLoad.totalEnemyStages;
	}
}
