﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

public class RewardManager : MonoBehaviour
{
	[Header("Essential scripts")]
	public SceneTransitionManager sceneTransitionManager;
	[Header("Map")]
	public MapBehaviour mapBehaviourScript;
	private MapSetup mapSetup;
	[Header("Battle Rewards")]
	public RewardSelector battleRewardSelector;
	public RewardObject[] battleRewardObjects;
	[Header("Chest Reward")]
	public RewardObject chestRewardObject;
	public RewardSelector chestRewardSelector;
	public Button chestButton;
	public GameObject chestText;
	[Header("Player Deck")]
	public DeckBehaviour playerDeck;
	[Header("Reward")]
	public RewardType currentType; // tipo de recompensa ativa no momento
	public GameObject selectedRewardPrefab;
	public string[] selectedCards = null;
	public float baseChanceFor2Stars;
	public float baseChanceFor3Stars;

	[Header("To choose the rewards")]
	public GameObject chestRewardUI;
	public GameObject battleRewardUI;

	[HideInInspector] public string[] cardsSelected; // cartas selecionadas para recompensa
	[HideInInspector] public bool gotReward = false; // o player ja recebeu a recompensa?

	public void Awake()
	{
		GameManager.Instance.rewardManager = this;
		if(GameManager.Instance.selectedCards != null)
		{
			selectedCards = GameManager.Instance.selectedCards;
			GameManager.Instance.selectedCards = null;
		}
		
		playerDeck = GameObject.Find("PlayerDeck").GetComponent<DeckBehaviour>();
		sceneTransitionManager = GameObject.Find("LevelLoader").GetComponent<SceneTransitionManager>();

		if (RewardData.rewardType == 1)
		{
			battleRewardUI.SetActive(true);
			ChooseBattleRewards();
		}
		else if (RewardData.rewardType == 0)
		{
			chestRewardUI.SetActive(true);
			battleRewardUI.SetActive(false);
			chestRewardSelector.SetupChestReward();
		}
		GameManager.Instance.SaveGame();
	}

	public void LoadBattleRewards()
	{
		cardsSelected = GameManager.Instance.currentData.rewardCardNames;
		currentType = RewardType.BATTLE;
		gotReward = false; // o player ainda nao recebeu a recompensa

			// configura a carta. Funcao de LoadGame ja atribuiu o cardsSelected
		for (int i = 0; i < battleRewardObjects.Length; i++)	
			battleRewardObjects[i].SetupCardEssence(GameManager.GetCardFromBundle(cardsSelected[i]));

		battleRewardSelector.SetupBattleReward();
	}
	
	public void ChooseBattleRewards()
	{
		currentType = RewardType.BATTLE;
		gotReward = false; 

		int rewardObjectIndex = 0;
		if(GameManager.Instance.battleRewardCards.Length == 0)
			GameManager.Instance.battleRewardCards = GameManager.Instance.
					actualMap.placeUnits[GameManager.Instance.actualPlace].rewardSetup.cards;

		selectedCards = new string[3];

		GameObject[] possibleCards = GameManager.Instance.battleRewardCards;

		foreach(GameObject card in possibleCards)
		{
			CardStatus status = card.GetComponent<CardStatus>();

			int chance = Random.Range(0, 100);
			if (chance <= ((baseChanceFor3Stars 
				+ PlayerBehaviour.Instance.playerBuffs[CauldronBuffTypes.REWARD_RATE_3_STARS][0])/100))
					UpgradeRewardCard(status, 2);
			else if (chance <= (((baseChanceFor3Stars + baseChanceFor2Stars)
				+(PlayerBehaviour.Instance.playerBuffs[CauldronBuffTypes.REWARD_RATE_2_STARS][0]))/100)) 
					UpgradeRewardCard(status, 1);
		}

		
		/* Embaralha o vetor de cartas para escolher aleatoriamente certo numero */
		int size = possibleCards.Length;
		if (size == 0) // Nao ha recompensas
		{
			Logging.Log("Saiu fora da recompensa");
			//GameUIManager.Instance.FromRewardScreenToMapScreen();
			return;
		}
		for (int i = 0; i < size; i++)
		{
			// get a random number from ramaining positions
			var randomNumber = i + Random.Range(0, (size - i));

			//switch positions
			GameObject temp = possibleCards[randomNumber];
			possibleCards[randomNumber] = possibleCards[i];
			possibleCards[i] = temp;
		}

		Logging.Log("Size: " + size);
		for (int i = 0; i < 3; i++)	selectedCards[i] = possibleCards[size - i - 1].name;

		for (int i = 0; i < GameManager.Instance.battleRewardCards.Length; i++)
		{
			for (int j = 0; j < selectedCards.Length; j++)
			{
				if (selectedCards[j] == GameManager.Instance.battleRewardCards[i].name)
				{
					battleRewardObjects[rewardObjectIndex].SetupCardEssence(GameManager.Instance.battleRewardCards[i]);
					rewardObjectIndex++;
					break;
				}
			}
		}

		battleRewardSelector.SetupBattleReward();
	}

	public void OpenChest()
	{
		gotReward = false; // o player ainda nao pegou a recompensa
		StartCoroutine(WaitToSpawnChestReward());

		GameObject reward = GameManager.Instance.battleRewardCards[Random.Range(0, GameManager.Instance.battleRewardCards.Length)];

		CardStatus status = reward.GetComponent<CardStatus>();

		int chance = Random.Range(0, 100);
		if (chance <= ((baseChanceFor3Stars 
			+ PlayerBehaviour.Instance.playerBuffs[CauldronBuffTypes.CHEST_RATE_3_STARS][0])/100))
				UpgradeRewardCard(status, 2);
		else if (chance <= (((baseChanceFor3Stars + baseChanceFor2Stars)
			+	(PlayerBehaviour.Instance.playerBuffs[CauldronBuffTypes.CHEST_RATE_2_STARS][0]))/100)) 
				UpgradeRewardCard(status, 1);

		chestRewardObject.SetupCardEssence(reward);

		chestButton.gameObject.GetComponent<Animator>().SetBool("ToOpenChest", true);

		chestText.gameObject.SetActive(false);

		StartCoroutine(OpenChest2());
	}

	public static void UpgradeRewardCard(CardStatus cardStatus, int times)
	{
		for(int i = 0; i < times; i++)
		{
			// Usado para fazer o upgrade real das cartas
			Logging.Log("Upgrade realizado");
			cardStatus.level++;
			DeckBehaviour.Instance.levelOfEachCard[cardStatus.indexOnDeck]++;
			cardStatus.stars.sprite = GameManager.GetSpriteFromBundle((cardStatus.level + 1) + "_Stars");
			foreach (CardsEffects effect in cardStatus.cardEffects)
				effect.Upgrade(cardStatus.level, false);
		}
	}

	// Espera um pouco para aparecer o livro na tela depois de abrir o baú
	public IEnumerator OpenChest2()
	{
		yield return new WaitForSeconds(0.7f);
        chestRewardSelector.gameObject.SetActive(true);
    }

	public IEnumerator WaitToSpawnChestReward()
	{
        yield return new WaitForSeconds(0.2f);
        chestRewardObject.gameObject.SetActive(true);
        chestRewardObject.transform.DOLocalMoveY(155f, 0.4f);
    }

	public void ResetChestPlace()
	{
		chestRewardSelector.CleanBook();

		chestRewardObject.gameObject.SetActive(false);
		chestRewardSelector.gameObject.SetActive(false);
		chestButton.gameObject.SetActive(true);
		chestText.gameObject.SetActive(true);

		chestRewardSelector.SnapBack(chestRewardSelector.selectedReward);
		chestRewardSelector.selectedReward = null;
	}

	public void ResetRewardPlace()
	{
		battleRewardSelector.CleanBook();
		battleRewardSelector.SnapBack(battleRewardSelector.selectedReward);
		battleRewardSelector.selectedReward = null;
	}

	public void GetReward(GameObject cardPrefab)
	{
		int index = playerDeck.deckList.Count;
		cardPrefab.GetComponent<CardStatus>().indexOnDeck = index;
		playerDeck.AddCardToDeckList(cardPrefab);
		gotReward = true;
	}

	public void GetBackToMap()
	{
		DOTween.KillAll();
		sceneTransitionManager.ChangeBackToMapScene();
	}
}
