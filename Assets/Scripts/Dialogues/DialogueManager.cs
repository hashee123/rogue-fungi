﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;

public class DialogueManager : MonoBehaviour
{
    public Queue<string> lines;
    public Text nameText;
    public Text lineText;
    public GameObject dialoguePanel;
    public GameObject[] backPanels;
    private int lineIndex = 0;

    // Start is called before the first frame update
    void Awake()
    {
        lines = new Queue<string>();
        Debug.Log(lines);
    }

    public void StartDialogue(DialogueObject dialogue)
    {
        nameText.text = dialogue.name;
        lines.Clear();
        
        dialogue.lines.FirstOrDefault(lineLocale => LocalizationSettings.SelectedLocale == lineLocale.localeName)?
            .text.ToList().ForEach(line => lines.Enqueue(line));

        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if(lines.Count == 0)
        {
            EndDialogue();
            return;
        }

        string line = lines.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeLine(line));

    }
    
    public void StartLoop(DialogueObject dialogue)
    {
        nameText.text = dialogue.name;
        StartCoroutine(DisplayNextInLoop(dialogue));
    }

    IEnumerator DisplayNextInLoop(DialogueObject dialogue)
    {
        Coroutine typeLoop;
        List<string> loopingLines = new List<string>();
        dialogue.lines.FirstOrDefault(lineLocale => LocalizationSettings.SelectedLocale == lineLocale.localeName)?
            .text.ToList().ForEach(line => loopingLines.Add(line));


        while(true)
        {
            typeLoop = StartCoroutine(TypeLine(loopingLines[lineIndex]));
            yield return new WaitForSeconds(10);
            lineIndex = (lineIndex + 1)%loopingLines.Count;
            StopCoroutine(typeLoop);
        }
    }

    IEnumerator TypeLine(string line)
    {
        lineText.text = "";
        foreach(char letter in line.ToCharArray())
        {
            lineText.text += letter;
            yield return null;
        }
    }

    public void EndDialogue()
    {
        dialoguePanel.SetActive(false);
        foreach(GameObject panel in backPanels) panel.SetActive(true);

    }
}
