﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopingDialogueManager : MonoBehaviour
{
    public DialogueManager dialogueManagerScript;
    public DialogueObject dialogue;
    void OnEnable()
    {
        dialogueManagerScript.StartLoop(dialogue);
    }
}
