﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

// Script from Unity Manual https://docs.unity3d.com/540/Documentation/Manual/keepingtrackofloadedassetbundles.html
// Modified
static public class AssetBundleManager
{
	// A dictionary to hold the AssetBundle references
	static private Dictionary<string, AssetBundleRef> dictAssetBundleRefs;
	static AssetBundleManager()
	{
		dictAssetBundleRefs = new Dictionary<string, AssetBundleRef>();
	}
	// Class with the AssetBundle reference, file name
	private class AssetBundleRef
	{
		public AssetBundle assetBundle = null;
		public string fileName;
		public AssetBundleRef(string newFileName)
		{
			fileName = newFileName;
			assetBundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, newFileName));
		}
	};
	// Get an AssetBundle
	public static AssetBundle getAssetBundle(string fileName)
	{
		AssetBundleRef abRef;
		if (dictAssetBundleRefs.TryGetValue(fileName, out abRef))
			return abRef.assetBundle;
		else
			return null;
	}
	// Download an AssetBundle
	/*public static IEnumerator downloadAssetBundle(string url, int version)
	  {
	  string keyName = url + version.ToString();
	  if (dictAssetBundleRefs.ContainsKey(keyName))
	  yield return null;
	  else
	  {
	  while (!Caching.ready)
	  yield return null;

	  using (WWW www = WWW.LoadFromCacheOrDownload(url, version))
	  {
	  yield return www;
	  if (www.error != null)
	  throw new Exception("WWW download:" + www.error);
	  AssetBundleRef abRef = new AssetBundleRef(url, version);
	  abRef.assetBundle = www.assetBundle;
	  dictAssetBundleRefs.Add(keyName, abRef);
	  }
	  }
	  }*/

	// Set an AssetBundle
	public static AssetBundle setAssetBundle(string fileName)
	{
		AssetBundleRef abRef = new AssetBundleRef(fileName);
		if (dictAssetBundleRefs.ContainsKey(fileName))
		{
			// Por padrao, da' overwrite no par que ja' contem
			dictAssetBundleRefs[fileName] = abRef;
		}
		else
		{
			dictAssetBundleRefs.Add(fileName, abRef);
		}

		return abRef.assetBundle;
	}

	// Unload an AssetBundle
	public static void Unload(string fileName, bool allObjects)
	{
		AssetBundleRef abRef;
		if (dictAssetBundleRefs.TryGetValue(fileName, out abRef))
		{
			abRef.assetBundle.Unload(allObjects);
			abRef.assetBundle = null;
			dictAssetBundleRefs.Remove(fileName);
		}
	}
}
