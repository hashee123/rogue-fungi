﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CardShopItem : ShopItem
{
	
	public void OnEnable()
	{
		gameObject.GetComponent<RectTransform>().DOScale(new Vector3(1.0f, 1.0f, 1.0f), 0.5f).SetEase(Ease.OutBounce);
	}

	public override void SetupShopItem(GameObject prefabToAdd)
	{
		base.SetupShopItem(prefabToAdd);
		IDragger cardIDragger = instantiatedObject.GetComponent<IDragger>();
		cardIDragger.selectionAreaImage.raycastTarget = false;
		cardIDragger.enabled = false;
		// instantiatedObject.GetComponent<CanvasGroup>().blocksRaycasts = false;
		// priceText.text = instantiatedObject.GetComponent<CardStatus>().shopPrice.ToString();
	}

	public override void AddToPlayer()
	{
		DeckBehaviour.Instance.AddCardToDeckList(objectToBuyPrefab);
		//PlayerBehaviour.Instance.GameManager.Instance.battleManager.DeckBehaviour.Instance.deckInstantiatedList.Add(objectToBuyPrefab);
		AfterPurchase();
	}

	public override void OnEndAnim()
	{
		gameObject.SetActive(false);
	}
}
