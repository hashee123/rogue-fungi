﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.UI;
using FMODUnity;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.Localization;
using UnityEngine.Localization.Components;
using UnityEngine.Localization.Settings;
using DG.Tweening;

public enum GameState { INITIAL_SCREEN, BATTLE_SCREEN, REWARD_SCREEN, CHEST_SCREEN, SHOP_SCREEN, MAP_SCREEN, UPGRADE_SCREEN, REST_SCREEN }

public class GameManager : MonoBehaviour
{
	public static GameManager Instance { get; private set; }

	private bool isDoingNewGame = true;
	private bool isDoingNewBattle = false;
	[Header("Essential Scripts")]
	public BattleManager battleManager;
	public BattleUIManager battleUIManager;
	public MapBehaviour mapBehaviourScript;
	public UpgradeManager upgradeManager;
	public RewardManager rewardManager;
	public ShopManager shopManager;
	public CauldronManager cauldronManager;
	public AudioSettings audioSettings;
	public LoadGameManager loadGameManager;

	// slots dos inimigos
	[Header("Battle Setup")]
	public BattleSetupPool battleSetupPool;
	public BattleSetup currentBattleSetup;
	public BattleState loadBattleState;
	public int currentEnemiesKilled;
	public bool boss;
	public int enemyMax;

	[Header("Map Setup")]
	public int actualPlace;
	public int mapNumber;
	public MapSetup actualMap;
	public MapSetupPool mapSetupPool;

	[Header("Changing UI")]
	public Camera mainCamera;
	public Camera menuCamera;
	public Canvas mainCanvas;
	public Canvas menuCanvas;

	[Header("Text files")]
	public TextAsset cardEffectDescriptions;
	public TextAsset statusEffectExplanation;
	public TextAsset enemyActionExplanation;

	[Header("Current State")]
	public GameState state; // estado do jogo, a partir disso eu posso impossibilitar o jogador de usar drag na carta durante a tela de Chest por exemplo
	public SaveData currentData;

	[Header("Asset Bundles")]
	public string cardBundleName; // nome do asset bundle que contem as cartas. O normal e' 'cardbundle'
	public string enemyBundleName; // nome do asset bundle que contem os inimigos. O normal e 'enemybundle'
	public string relicBundleName; // nome do asset bundle que contem as reliquias. O normal e' 'relicbundle'
	public string fontBundleName;
	public string spriteBundleName;
	public static AssetBundle cardBundle; // o asset bundle em si
	public static AssetBundle enemyBundle; // o asset bundle em si
	public static AssetBundle relicBundle; // o asset bundle em si
	public static AssetBundle fontBundle;
	public static AssetBundle spriteBundle;

	[Header("Other")]
	public GameObject transitionImage;

	[Header("Transition Object")]
	public GameObject transitionObject;
	public Animator transitionObjectAnimator;

	public GameObject[] battleRewardCards;
	public string[] selectedCardNames;
	public string[] selectedCards = null;
	public int[] selectedCardIndex;
	public int loadedCardsUpgrade;

	public string[] effectsExplainerTableEntries;
	public string[] enemyActionExplainerTableEntries;

	public SettingsMenu settingsMenu;

	public void Awake()
	{
		if (Instance != null && Instance != this)	Destroy(gameObject);
		else	Instance = this;

		SettingsSaveData.Instance = new SettingsSaveData();

		DontDestroyOnLoad(gameObject);
		state = GameState.MAP_SCREEN;

		if (File.Exists(Application.persistentDataPath + "/SettingsSaveData.dat"))	LoadSettingsData();
		else	SaveSettingsData();

		ApplySettingsData();

		if (AssetBundleManager.getAssetBundle(cardBundleName) == null)
			cardBundle = AssetBundleManager.setAssetBundle(cardBundleName);
		else	cardBundle = AssetBundleManager.getAssetBundle(cardBundleName);

		if (AssetBundleManager.getAssetBundle(enemyBundleName) == null)
			enemyBundle = AssetBundleManager.setAssetBundle(enemyBundleName);
		else	enemyBundle = AssetBundleManager.getAssetBundle(enemyBundleName);

		if (AssetBundleManager.getAssetBundle(relicBundleName) == null)
			relicBundle = AssetBundleManager.setAssetBundle(relicBundleName);
		else	relicBundle = AssetBundleManager.getAssetBundle(relicBundleName);

		if (AssetBundleManager.getAssetBundle(fontBundleName) == null)
			fontBundle = AssetBundleManager.setAssetBundle(fontBundleName);
		else	fontBundle = AssetBundleManager.getAssetBundle(fontBundleName);

		if (AssetBundleManager.getAssetBundle(spriteBundleName) == null)
			spriteBundle = AssetBundleManager.setAssetBundle(spriteBundleName);
		else	spriteBundle = AssetBundleManager.getAssetBundle(spriteBundleName);

		if (cardBundle == null)	Logging.LogError("Falha ao carregar o Card Bundle!");
		if (enemyBundle == null)	Logging.LogError("Falha ao carregar o Enemy Bundle!");
		if (relicBundle == null)	Logging.LogError("Falha ao carregar o Relic Bundle!");
		if (fontBundle == null)	Logging.LogError("Falha ao carregar o Font Bundle!");
		if (spriteBundle == null)	Logging.LogError("Falha ao carregar o Sprite Bundle!");

		EffectsExplainerTable.tableEntries = effectsExplainerTableEntries;
		EnemyActionExplainerTable.tableEntries = enemyActionExplainerTableEntries;

		﻿﻿﻿﻿﻿DOTween.Init();
	}

	private void Start()
	{
		transitionObject = GameObject.FindGameObjectWithTag("LevelLoader");
		transitionObjectAnimator = transitionObject.GetComponentInChildren<Animator>();
	}

	public static void UnloadAssetBundles()
	{
		cardBundle.Unload(true);
		enemyBundle.Unload(true);
		relicBundle.Unload(true);
		fontBundle.Unload(true);
	}

	public static GameObject GetCardFromBundle(string cardName)
	{
		return cardBundle.LoadAsset<GameObject>(cardName);
	}

	public static GameObject GetEnemyFromBundle(string enemyName)
	{
		return enemyBundle.LoadAsset<GameObject>(enemyName);
	}

	public static Relic GetRelicFromBundle(string relicName)
	{
		return relicBundle.LoadAsset<Relic>(relicName);
	}

	public static Font GetFontFromBundle(string fontName)
	{
		return fontBundle.LoadAsset<Font>(fontName);
	}

	public static Sprite GetSpriteFromBundle(string spriteName)
	{
		return spriteBundle.LoadAsset<Sprite>(spriteName);
	}

	// Ainda n e' chamada, e' feita para fazer uma transicao entre a tela de mapa e de batalha
	public IEnumerator ActiveTransition()
	{
		transitionImage.SetActive(true);
		yield return new WaitForSeconds(0.3f);
		transitionImage.SetActive(false);
	}

	public int NewGame()
	{
		Logging.Log("Chamei a função de load map GameManager");
		battleManager.mapBehaviourScript.InitializeMap(true);
		state = GameState.INITIAL_SCREEN;
		MenuData.justCameFromMenu = true;
		isDoingNewGame = true;
		return 0;
	}

	public void SetupLoadGame()
	{
		loadGameManager.SetupLoadGame();
	}

	public void LoadMap()
	{
		loadGameManager.LoadMapData();
	}
	public void ResetMapData()
	{
		MenuData.doNewGame = true;
		isDoingNewGame = true;
		actualPlace = 0;
		MapSceneTransition.mapSceneTransitionData.alreadyGotInformation = false;
	}

	public void LoadGame()
	{
		isDoingNewGame = false;
		loadGameManager.LoadGame();
	}

	public bool IsNewGame()
	{
		return isDoingNewGame;
	}

	public void DoNewBattle()
	{
		isDoingNewBattle = true;
	}

	public bool IsDoingNewBattle()
	{
		return isDoingNewBattle;
	}

	public void LoadSettingsData()
	{
		// SÓ CHAMAR DEPOIS DE VERIFICAR SE O ARQUIVO EXISTE!
		BinaryFormatter bf = new BinaryFormatter();
		FileStream settingsSaveFile = File.Open(Application.persistentDataPath + "/SettingsSaveData.dat", FileMode.Open);
		SettingsSaveData.Instance = (SettingsSaveData)bf.Deserialize(settingsSaveFile);
		settingsSaveFile.Close();
	}

	public void ApplySettingsData()
	{
		settingsMenu.SetQuality(SettingsSaveData.Instance.qualitySettings);
		settingsMenu.SetLanguage(SettingsSaveData.Instance.languageSettings);
		settingsMenu.SetMasterVolumeSlider(SettingsSaveData.Instance.volumeGeneralSettings);
		settingsMenu.SetMusicVolumeSlider(SettingsSaveData.Instance.volumeMusicSettings);
		settingsMenu.SetSFXVolumeSlider(SettingsSaveData.Instance.volumeSoundEffectsSettings);
	}

	public void SaveSettingsData()
	{
		BinaryFormatter bf = new BinaryFormatter();
		FileStream saveFile = new FileStream(Application.persistentDataPath + "/SettingsSaveData.dat", FileMode.Create);
		bf.Serialize(saveFile, SettingsSaveData.Instance);
		saveFile.Close();
	}

	public void DeleteSettingsData()
	{
		if (File.Exists(Application.persistentDataPath + "/SettingsSaveData.dat"))
			File.Delete(Application.persistentDataPath + "/SettingsSaveData.dat");
	}

	public void SaveGame()
	{
		BinaryFormatter bf = new BinaryFormatter();
		FileStream saveFile = new FileStream(Application.persistentDataPath + "/SaveData.dat", FileMode.Create);
		SaveData dataToSave = new SaveData();
		dataToSave.SaveDataFunction();
		bf.Serialize(saveFile, dataToSave);
		saveFile.Close();
	}

	public void ResetSaveFile()
	{
		if (File.Exists(Application.persistentDataPath + "/SaveData.dat"))
			File.Delete(Application.persistentDataPath + "/SaveData.dat");
	}

	public void SwitchToMenuUI()
	{
		mainCamera.gameObject.SetActive(false);
		menuCamera.gameObject.SetActive(true);
	}

	public void SwitchToMainUI()
	{
		mainCamera.gameObject.SetActive(true);
		menuCamera.gameObject.SetActive(false);
	}


	public IEnumerator waitForTransition(Animator transitionAnimator, GameObject objectToDeact, GameObject objectToAct)
	{
		transitionAnimator.SetTrigger("Start");
		yield return new WaitForSeconds(0.5f);
		objectToDeact.SetActive(false);
		objectToAct.SetActive(true);
	}

	public IEnumerator waitForTransition(Animator transitionAnimator, Func<int> afterTransitionFunction, string animArg, bool fullAnim)
	{
		transitionAnimator.SetBool("FullAnimation", fullAnim);
		transitionAnimator.SetTrigger(animArg);
		yield return new WaitForSeconds(0.01f);
		afterTransitionFunction();
	}

	public IEnumerator waitForTransition(Animator transitionAnimator, Func<int> afterTransitionFunction)
	{
		transitionAnimator.SetBool("FullAnimation", true);
		transitionAnimator.SetTrigger("Start");
		yield return new WaitForSeconds(0.5f);
		afterTransitionFunction();
		transitionAnimator.SetBool("FullAnimation", false);
	}

	public BattleSetup getBattleSetup(int diff, int index)
	{
		if (diff <= 0)	return GameManager.Instance.battleSetupPool.battleSetupPoolLow[index];
		else if (diff == 1)	return GameManager.Instance.battleSetupPool.battleSetupPoolMed[index];
		else if (diff == 2)	return GameManager.Instance.battleSetupPool.battleSetupPoolHigh[index];
		else	return GameManager.Instance.battleSetupPool.battleSetupPoolBoss[index];
	}

	public BattleSetup randomizeBattleSetup(int count, int diff, Place p)
	{
		if (diff <= 0)
		{
			int random = UnityEngine.Random.Range(0, GameManager.Instance.battleSetupPool.battleSetupPoolLow.Length);
			p.battleSetupIndex = random;
			GameManager.Instance.battleSetupPool.difficulties[count] = p.diff;
			GameManager.Instance.battleSetupPool.indexes[count] = random;
			return GameManager.Instance.battleSetupPool.battleSetupPoolLow[random];
		}
		else if (diff == 1)
		{
			int random = UnityEngine.Random.Range(0, GameManager.Instance.battleSetupPool.battleSetupPoolMed.Length);
			p.battleSetupIndex = random;
			GameManager.Instance.battleSetupPool.difficulties[count] = p.diff;
			GameManager.Instance.battleSetupPool.indexes[count] = random;
			return GameManager.Instance.battleSetupPool.battleSetupPoolMed[random];
		}
		else if (diff == 2)
		{
			int random = UnityEngine.Random.Range(0, GameManager.Instance.battleSetupPool.battleSetupPoolHigh.Length);
			p.battleSetupIndex = random;
			GameManager.Instance.battleSetupPool.difficulties[count] = p.diff;
			GameManager.Instance.battleSetupPool.indexes[count] = random;
			return GameManager.Instance.battleSetupPool.battleSetupPoolHigh[random];
		}
		else
		{
			int random = UnityEngine.Random.Range(0, GameManager.Instance.battleSetupPool.battleSetupPoolBoss.Length);
			p.battleSetupIndex = random;
			GameManager.Instance.battleSetupPool.difficulties[count] = p.diff;
			GameManager.Instance.battleSetupPool.indexes[count] = random;
			return GameManager.Instance.battleSetupPool.battleSetupPoolBoss[random];
		}
	}
}
