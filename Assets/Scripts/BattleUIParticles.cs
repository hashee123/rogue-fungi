﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleUIParticles : MonoBehaviour
{
	public Transform particleSpawnPosition;
	public Transform enemyAttackPosition;
	public Transform enemyFeetPos;
	public ParticleSystem playerShieldParticles;
	public ParticleSystem sleepParticles;
	public ParticleSystem gainStrenghtParticles;
	public ParticleSystem fleeParticles;
	public ParticleSystem clumsinessParticles;
	public GameObject enemyBite;
	public GameObject enemyCurse;
}
