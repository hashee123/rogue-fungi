﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FMODUnity;
using System.IO;
using System;
using UnityEngine.SceneManagement;
using TMPro;

public class MainMenuButtonScript : MonoBehaviour
{
	public Animator transitionObjectAnimator;
	public AnimationClip transitionStartAnimation;

	public Button LoadButton;
	public GameObject NewGameButton;
	public GameObject ConfirmationButton;
	public TMP_Text LoadButtonText;
	public StudioEventEmitter fmodEmitter;

	public void Start()
	{
		if (!File.Exists(Application.persistentDataPath + "/SaveData.dat"))
		{
			LoadButton.interactable = false;
			LoadButtonText.color = new Color32(94, 94, 94, 255);
			fmodEmitter.enabled = false;
			NewGameButton.SetActive(true);
		}
		else
		{
			LoadButton.interactable = true;
			fmodEmitter.enabled = true;
			ConfirmationButton.SetActive(true);
		}
	}

	int afterTransitionFunctionNewGame()
	{
		MenuData.doNewGame = true;
		MenuData.justCameFromMenu = true;
		SceneManager.LoadScene(1);
		return 0;
	}

	public void ChangeToSceneNewGame()
	{
		StartCoroutine(waitForTransitionMainMenu(transitionObjectAnimator, afterTransitionFunctionNewGame));
	}


	int afterTransitionFunctionLoadGame()
	{
		MenuData.doNewGame = false;
		MenuData.justCameFromMenu = true;
		SceneManager.LoadScene(1);
		return 0;
	}

	public void ChangeToSceneLoadGame()
	{
		StartCoroutine(waitForTransitionMainMenu(transitionObjectAnimator, afterTransitionFunctionLoadGame));
	}

	public IEnumerator waitForTransitionMainMenu(Animator transitionAnimator, Func<int> afterTransitionFunction)
	{
		transitionAnimator.SetBool("FullAnimation", false);
		transitionAnimator.SetTrigger("Start");

		yield return new WaitForSeconds(transitionStartAnimation.length);

		afterTransitionFunction();
	}
}
