using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;
using DG.Tweening;
using TMPro;

public class CauldronItem : MonoBehaviour
{
	public Button confirmButton;
	public CauldronBuffClass cauldronBuff;
	public Button backButton;
	public int price;
	public Text priceText;
	public Text buffDescriptionText;
	public CauldronManager cauldronManagerScript;
	public GameObject confirmationPanel;
	public Text confirmationText;

	public virtual void TryAndBuy()
	{

		if (PlayerBehaviour.Instance.playerBehaviourData.coins >= price)
		{
			confirmationPanel.SetActive(true);
			confirmationText.text = buffDescriptionText.text;
			confirmButton.onClick.AddListener(AddToPlayer);
			backButton.onClick.AddListener(BackButton);
		}
		else	FailPurchase();
	}    

	public virtual void AddToPlayer()
	{
		confirmButton.onClick.RemoveListener(AddToPlayer);
		PlayerBehaviour.Instance.playerBehaviourData.coins -= price;
		foreach(MoneyDisplay moneyDisplay in cauldronManagerScript.moneyDisplays) moneyDisplay.UpdateDisplay();
		PlayerBehaviour.Instance.playerBuffs[cauldronBuff.type][0] += 
			cauldronBuff.buffValues[PlayerBehaviour.Instance.playerBuffs[cauldronBuff.type][1]];
		AfterPurchase();
	}

	public virtual void AfterPurchase()
	{
		PlayerBehaviour.Instance.playerBuffs[cauldronBuff.type][1]++;
		CauldronBuffClass previousBuff = cauldronBuff;

		if(cauldronManagerScript.availableBuffs.Count > 0)
		{
			int randomValue = Random.Range(0, cauldronManagerScript.availableBuffs.Count);
			cauldronBuff = cauldronManagerScript.cauldronBuffsPack.cauldronBuffs[cauldronManagerScript.availableBuffs[randomValue]];

			cauldronManagerScript.cauldronBuffs.Add(cauldronManagerScript.availableBuffs[randomValue]);
			cauldronManagerScript.availableBuffs.RemoveAt(randomValue);

			foreach(DynamicLocaleText localizedTexts in cauldronBuff.localizedText)
			{
			 	if(LocalizationSettings.SelectedLocale == localizedTexts.localeName)
				{
					buffDescriptionText.text = localizedTexts.text;
					buffDescriptionText.text = 
						buffDescriptionText.text.Replace("&&", 
							cauldronBuff.buffValues[PlayerBehaviour.Instance.playerBuffs[cauldronBuff.type][1]].ToString());
					break;
				}
			}
			priceText.text = cauldronBuff.prices[PlayerBehaviour.Instance.playerBuffs[cauldronBuff.type][1]].ToString();
			price = cauldronBuff.prices[PlayerBehaviour.Instance.playerBuffs[cauldronBuff.type][1]];
		}

		for (int i = 0; i< cauldronManagerScript.cauldronBuffsPack.cauldronBuffs.Length; i++)
		{
			if(cauldronManagerScript.cauldronBuffsPack.cauldronBuffs[i] == previousBuff)
			{
				if(PlayerBehaviour.Instance.playerBuffs[previousBuff.type][1] < 10)	cauldronManagerScript.availableBuffs.Add(i);
				cauldronManagerScript.cauldronBuffs.Remove(i);
				break;
			}

		}
		
		if(cauldronManagerScript.availableBuffs.Count <= 0) gameObject.SetActive(false);
	}

	public virtual void BackButton()
	{
		confirmationText.text = "";
		confirmButton.onClick.RemoveListener(AddToPlayer);
		backButton.onClick.RemoveListener(BackButton);
	}

	public virtual void FailPurchase()
	{
		gameObject.transform.DOShakeRotation(0.4f, 5.0f, 15, 70, true, ShakeRandomnessMode.Harmonic);
	}

}