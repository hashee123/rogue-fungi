﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GraveyardCardIcon : MonoBehaviour
{
	private BattleUIManager battleUIManager;
	[SerializeField] private SpriteRenderer spriteRenderer;
	[SerializeField] private SpriteRenderer glowSpriteRenderer;

	public void SetBattleUIManager(BattleUIManager battleUIManager)
	{
		this.battleUIManager = battleUIManager;
	}

	public void MoveTowardsDeck()
	{
		Vector3 targetPosition = battleUIManager.graveyardIconEndPosition.position;
		transform.DOMove(targetPosition, 0.5f).SetEase(Ease.InCirc).OnComplete(OnCompleteMoveToDeck);
		transform.DOScale(Vector3.zero, 0.5f).SetEase(Ease.InCirc);
		glowSpriteRenderer.DOFade(1.0f, 0.5f);
	}

	void OnCompleteMoveToDeck()
	{
		battleUIManager.deckButtonJuice.Juicy(1.4f);
		Destroy(gameObject);
	}
}
