﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;
using DG.Tweening;

public class ShopManager : MonoBehaviour
{
	[Header("Shop Settings")]
	public ShopPack shopPack;
	public GameObject shopPanel;
	public GameObject shopItemsPanel;
	public RectTransform shopUIPanelTransform;
	public MoneyDisplay[] moneyDisplays;
	public Button goBackButton;
	public AnimationClip shopItemStartAnimationClip;

	public GameObject noInteractionPanel;
	public int[] shopCards = null;

	[HideInInspector] public bool hasRelic = false;

	public GameObject dialoguePanel;
	public GameObject cauldronDialoguePanel;
	public bool isInteracting = false;
	public DialogueObject spiderDialogue;
	public DialogueManager entryDialogueManagerScript;

	public void Start()
	{
		GameManager.Instance.shopManager = this;
		EnterShop();
		foreach(MoneyDisplay moneyDisplay in moneyDisplays) moneyDisplay.UpdateDisplay();
	}

	public void EnterShop()
	{
		GameManager.Instance.state = GameState.SHOP_SCREEN;
		if (ShopData.shopCards == null)	entryDialogueManagerScript.StartDialogue(spiderDialogue);
		else
		{
			dialoguePanel.SetActive(false);
			shopPanel.SetActive(true);
		}
		RandomizeShop();
	}

	public void RandomizeShop()
	{
		bool doNewRandom = false;
		int index = 0;
		if (ShopData.shopCards == null)
		{
			int numberOfCards = shopItemsPanel.transform.childCount;
			shopCards = new int[numberOfCards];
			doNewRandom = true;
		}
		foreach (Transform child in shopItemsPanel.transform)
		{
			string cardName;
			int cardPrice;
			if (doNewRandom)
			{
				int randomValue = Random.Range(0, shopPack.cards.Length);
				cardName = shopPack.cards[randomValue];
				cardPrice = shopPack.price[randomValue];
				shopCards[index] = randomValue;
			}
			else
			{
				Debug.Log(shopCards);
				cardName = shopPack.cards[ShopData.shopCards[index]];
				cardPrice = shopPack.price[ShopData.shopCards[index]];
			}

			GameObject card = GameManager.GetCardFromBundle(cardName);
			// androidPanelText.text = "Peguei uma carta do card Bundle";
			ShopItem shopItemScript = child.GetComponent<ShopItem>();
			shopItemScript.price = cardPrice;
			shopItemScript.priceText.text = cardPrice.ToString();
			shopItemScript.SetupShopItem(card);
			shopItemScript.shopManager = this;
			index++;
		}

		GameManager.Instance.SaveGame();
	}

	public void BackToMap()
	{
		PlayerBehaviour.Instance.playerBehaviourData.xpRunStart = PlayerBehaviour.Instance.playerBehaviourData.xp;
		PlayerBehaviour.Instance.playerBehaviourData.coinsRunStart = PlayerBehaviour.Instance.playerBehaviourData.coins;
		GameManager.Instance.ResetMapData();
		GameUIManager.Instance.FromShopScreenToMapScreen();
	}
}
