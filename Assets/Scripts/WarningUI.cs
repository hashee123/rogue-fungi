﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WarningUI : MonoBehaviour
{
	public Text warningText;
	public Animator animator;

	public void NotEnoughActionPoints()
	{
		warningText.text = GameUIManager.Instance.notEnoughActionPoints;
		animator.SetTrigger("FadeInAndOut");
	}

	public void NotEnoughCoins()
	{
		warningText.text = GameUIManager.Instance.notEnoughCoins;
		animator.SetTrigger("FadeInAndOut");
	}

	public void NoCardToUpgrade()
	{
		warningText.text = GameUIManager.Instance.noCardsToUpgrade;
		animator.SetTrigger("TurnOn");
	}

	public void Reset()
	{
		animator.SetTrigger("Reset");
	}
}
