﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EnemyBiteAnimation : MonoBehaviour
{
	[SerializeField] private SpriteRenderer spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
		FadeIn();
    }

	public void FadeIn()
	{
		spriteRenderer.DOFade(1.0f, 0.4f);
	}

	public void FadeOut()
	{
		spriteRenderer.DOFade(0.0f, 0.4f).OnComplete(OnEndFadeOut);
	}

	void OnEndFadeOut()
	{
		Destroy(gameObject);
	}
}
