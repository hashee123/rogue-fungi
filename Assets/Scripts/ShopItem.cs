﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class ShopItem : MonoBehaviour
{
	public GameObject objectHolder;
	public GameObject confirmObjectHolder;
	public GameObject confirmationPanel;
	public Button confirmButton;
	public Button backButton;
	public int price;
	public TextMeshPro priceText;
	public GameObject objectToBuyPrefab;
	public Animator animator;
	public GameObject instantiatedObject;
	public ShopManager shopManager;
	private GameObject confirmCard;

	public virtual void SetupShopItem(GameObject prefabToAdd)
	{
		objectToBuyPrefab = prefabToAdd;
		instantiatedObject = Instantiate(objectToBuyPrefab, objectHolder.transform.position, objectHolder.transform.rotation, objectHolder.transform);
	}

	public virtual void TryAndBuy()
	{

		if (PlayerBehaviour.Instance.playerBehaviourData.coins >= price)
		{
			confirmationPanel.SetActive(true);
			confirmCard = Instantiate(objectToBuyPrefab, confirmObjectHolder.transform.position, confirmObjectHolder.transform.rotation, confirmObjectHolder.transform);
			confirmButton.onClick.AddListener(AddToPlayer);
			backButton.onClick.AddListener(BackButton);
		}
		else
		{
			// animator.SetTrigger("FailPurchase");
			FailPurchase();
		}
	}    

	public virtual void AddToPlayer()
	{
		// Adicionar pro player
		PlayerBehaviour.Instance.playerBehaviourData.coins -= price;
		foreach(MoneyDisplay moneyDisplay in shopManager.moneyDisplays) moneyDisplay.UpdateDisplay();
		confirmButton.onClick.RemoveListener(AddToPlayer);
		AfterPurchase();
	}

	public virtual void BackButton()
	{
		confirmButton.onClick.RemoveListener(AddToPlayer);
		backButton.onClick.RemoveListener(BackButton);
		Destroy(confirmCard);
	}

	public virtual void AfterPurchase()
	{
		PlayerBehaviour.Instance.playerBehaviourData.coins -= price;
		foreach(MoneyDisplay moneyDisplay in shopManager.moneyDisplays) moneyDisplay.UpdateDisplay();
		confirmButton.onClick.RemoveListener(AddToPlayer);
		StartCoroutine(SuccessPurchase());
	}

	public virtual void OnEndAnim()
	{
		Destroy(gameObject);
	}

	public virtual void FailPurchase()
	{
		gameObject.transform.DOShakeRotation(0.4f, 5.0f, 15, 70, true, ShakeRandomnessMode.Harmonic);
	}

	public virtual IEnumerator SuccessPurchase()
	{
		float sequenceDuration = 0.4f;
		Sequence s = DOTween.Sequence();
		s.Append(gameObject.transform.DORotate(new Vector3(0.0f, 0.0f, -720.0f), sequenceDuration + 1.0f, RotateMode.FastBeyond360));
		s.Join(gameObject.transform.DOScale(new Vector3(0.01f, 0.01f, 0.01f), sequenceDuration));

		yield return new WaitForSeconds(sequenceDuration);
		OnEndAnim();
		yield return null;
	}
}
