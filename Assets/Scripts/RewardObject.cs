﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using static UnityEngine.ParticleSystem;
using TMPro;

public class RewardObject : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler
{
	[Header("Setup")]
	public RewardSelector rewardSelector;
	//public Canvas mainCanvas;
	public RectTransform rectTransform;
	public SpriteRenderer essenceImage;
	public SpriteRenderer overlayImage; // needs the same image of essenceImage
	public Animator animator;
	public ParticleSystem particles;
	[Header("Current Reward")]
	public GameObject cardPrefab;
	[HideInInspector] public bool isSelected;
	[HideInInspector] public string cardDescription;

	public bool clickDown = false;

	public Vector3 originalPosition;

	public void Awake()
	{
		originalPosition = rectTransform.localPosition;
	}

	public void SetupCardEssence(GameObject newCardPrefab)
	{
		cardDescription = "";
		cardPrefab = newCardPrefab;

		GameObject card = Instantiate(newCardPrefab, transform);
		essenceImage.sprite = card.GetComponent<CardStatus>().cardCenterSprite; // imagem que esta no prefab
		overlayImage.sprite = essenceImage.sprite;

		Transform descriptions = card.transform.Find("Description");
		foreach (Transform description in descriptions)	cardDescription += description.GetComponent<TextMeshPro>().text + '\n';
		Destroy(card);
	}

	public void OnPointerDown(PointerEventData pointerEventData)
	{
		clickDown = true;
	}

	public void OnPointerUp(PointerEventData pointerEventData)
	{
		if (clickDown)	rewardSelector.SetRewardOnBook(pointerEventData);
		else	ReturnObject(gameObject);

		clickDown = false;
	}

	public void ReturnObject(GameObject obj)
	{
		rewardSelector.SnapBack(obj);
		if (rewardSelector.selectedReward == null)	rewardSelector.SetSelectionElements(false);
		else if (rewardSelector.selectedReward != obj)	rewardSelector.SetSelectionElements(true);
	}

	public void OnBeginDrag(PointerEventData pointerEventData)
	{
		clickDown = false;
		if(animator.gameObject.activeSelf && animator.runtimeAnimatorController!=null) animator.SetBool("IsDragging", true);
		rewardSelector.SetSelectionElements(false);
		if(rewardSelector.selectedReward == gameObject)	rewardSelector.selectedReward = null;
	}

	public void OnDrag(PointerEventData pointerEventData)
	{
		// Arrasta a carta para a posicao do dedo
		rectTransform.anchoredPosition += pointerEventData.delta / rewardSelector.rewardCanvas.scaleFactor;
	}

	public void OnEndDrag(PointerEventData pointerEventData)
	{
		if(animator.gameObject.activeSelf && animator.runtimeAnimatorController!=null) animator.SetBool("IsDragging", false);
		Logging.Log("EndDrag: " + isSelected);
		if (!isSelected)	ReturnObject(gameObject);
	}

	public void OnDisable()
	{
		isSelected = false;
		cardDescription = null;
	}
}
