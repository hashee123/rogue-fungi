using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class PersistentSaveData
{
	public int currentRun;
	public int coins;
}
