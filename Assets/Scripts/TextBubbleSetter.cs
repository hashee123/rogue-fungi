﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextBubbleSetter : MonoBehaviour
{
	public SpriteRenderer backgroundSprite;
	public RectTransform backgroundRectTransform;
	public TextMeshPro bubbleText;
	public float pivotToSubtract;
	public float lineSize;

	public void Start()
	{
		pivotToSubtract = backgroundRectTransform.pivot.y;
		lineSize = backgroundSprite.size.y;
	}


	public void SetText(string t)
	{
		bubbleText.text = t;
		// Logging.Log("O bubbleText é: " + bubbleText.textInfo);
		// int lineCount = bubbleText.textInfo.lineCount;
		// backgroundSprite.size.Set(backgroundSprite.size.x, lineCount * lineSize);
		// backgroundRectTransform.pivot -= new Vector2(0, pivotToSubtract);
	}
}
