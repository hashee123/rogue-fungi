﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Components;
using UnityEngine.Localization.SmartFormat.PersistentVariables;

public enum KindOfCard { ATTACK, SKILL, MODIFIER, TRAP };

public abstract class CardsEffects : MonoBehaviour
{
	public KindOfCard kind; // tipo da carta
	public bool hasPassiveEffect = false; // essa carta possui um efeito passivo atualizado depois do turno?

	[TextArea(2, 5)]
	public string effectText;

	[HideInInspector] public GameObject battleManager;
	[HideInInspector] public GameObject playerDeck;
	[HideInInspector] public GameObject player;
	[HideInInspector] public CardStatus thisCardStatus;
	[HideInInspector] public BattleUIManager battleUIManagerScript;
	public bool destroyOnUse = false;
	public bool sendToGraveyardOnUse = true;
	public LocalizeStringEvent[] localizedStrings;
	public bool[] dhos = {false, false}; // Do Highlight On String shortened because of use in editor

	public virtual void Setup()
	{
		playerDeck = DeckBehaviour.Instance.gameObject;
		player = PlayerBehaviour.Instance.gameObject;

		thisCardStatus = this.GetComponent<CardStatus>();
		Logging.Log("No setup thisCardStatus: ", thisCardStatus);
		if (GameManager.Instance.state == GameState.BATTLE_SCREEN)
		{
			battleUIManagerScript = GameObject.FindGameObjectWithTag("BattleUIManager").GetComponent<BattleUIManager>();
		}
	}

	public void Awake()
	{
		dhos = new bool[localizedStrings.Length];
		for (int i = 0; i < localizedStrings.Length; i++)
		{
			dhos[i] = false;
		}
		Logging.Log("Chamei o  setup");
		Setup();
	}

	public void RefreshCardStrings()
	{
		for (int i = 0; i < localizedStrings.Length; i++)
		{
			localizedStrings[i].RefreshString();
		}
	}

	public virtual void ChooseEffect(CardStatus card)
	{
		if (card.kind == KindOfCard.ATTACK)
		{
			switch (card.actualModifier)
			{
				case CardModifier.ORIGINAL:
					ApplyEffect();
					break;
				case CardModifier.POISON:
					ApplyPoisonModifierEffect();
					break;
				case CardModifier.PROTECTION:
					ApplyProtectionEffect();
					break;
				case CardModifier.FISICAL_DMG:
					ApplyStrengthEffect();
					break;
				case CardModifier.PIERCING:
					ApplyPiercingEffect();
					break;
			}
		} else {
			ApplyEffect();
		}
	}

	public virtual void Upgrade(int level, bool highlight)
	{

	}

	public virtual void ApplyEffect()
	{

	}

	public virtual void ApplyPassiveEffect()
	{

	}

	public virtual void ApplyPoisonModifierEffect()
	{

	}

	public virtual void ApplyProtectionEffect()
	{

	}

	public virtual void ApplyPiercingEffect()
	{

	}

	public virtual void ApplyStrengthEffect()
	{

	}

	public virtual void OnDrawEffect()
	{

	}

	public virtual void OnDiscardEffect()
	{

	}

	public virtual string CardDescription()
	{
		return "";
	}

}
