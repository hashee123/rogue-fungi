﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Localization.Components;

public class EffectsExplainer : MonoBehaviour
{
	public RectTransform panelTransform;
	public GameObject[] descriptors;
	public LocalizeStringEvent[] localizedStrings;
	public bool allowUpdate = false;

	private RectTransform cardTransform;
	public void SetDescriptions(CardStatus cardStatus)
	{
		cardTransform = cardStatus.cardTransform;
		int i = 0;
		foreach (Transform description in cardStatus.cardDescriptionTransform)
		{
			if (EffectsExplainerTable.IsInTable(description.gameObject.name))
			{
				descriptors[i].SetActive(true);
				localizedStrings[i].StringReference.SetReference("EffectsExplainer", description.gameObject.name);
			}
		}
	}

	public void DisableDescriptors()
	{
		foreach (GameObject descriptor in descriptors)
		{
			descriptor.SetActive(false);
		}
	}

	public void LateUpdate()
	{
		if (allowUpdate)
		{
			float offsetY = cardTransform.rect.height * cardTransform.localScale.y / 2
				+ panelTransform.rect.height * panelTransform.localScale.y / 2;
			transform.position = cardTransform.position + Vector3.up * offsetY;
		}
	}
}
