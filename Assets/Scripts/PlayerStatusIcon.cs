﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Localization.Components;

public class PlayerStatusIcon : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public Animator animator;
	public TextMeshProUGUI textBox;
	public GameObject explainerObject;
	public Image statusImage;
	public BattleUIManager battleUIManager;
	public LocalizeStringEvent localizedString;
	public bool isBeingUsed = false;
	public string effectName = "statusIcon";
	bool isBeingTurnedOff = false;

	public void OnPointerEnter(PointerEventData pointerEventData)
	{
		if (isBeingTurnedOff) return;

		localizedString.StringReference.SetReference("StatusExplainer", effectName);
		explainerObject.SetActive(true);
	}

	public void OnPointerExit(PointerEventData pointerEventData)
	{
		explainerObject.SetActive(false);
	}

	public void EnableStatusIcon(string statusName, string statusValue, Sprite image)
	{
		effectName = statusName;
		isBeingTurnedOff = false;
		isBeingUsed = true;
		gameObject.SetActive(true);
		textBox.text = statusValue;
		statusImage.sprite = image;
		if (animator != null && animator.gameObject.activeSelf)	animator.SetTrigger("Activate");
	}

	public void UpdateText(string statusValue)
	{
		if (statusValue != textBox.text)
		{
			animator.SetTrigger("Activate");
		}
		textBox.text = statusValue;
	}

	public void DisableStatusIcon()
	{
		isBeingTurnedOff = true;
		effectName = "statusIcon";
		explainerObject.SetActive(false);
		if (animator != null && animator.gameObject.activeSelf)	animator.SetTrigger("FadeOut");
	}

	public void OnBeginEndAnim()
	{
		explainerObject.SetActive(false);
	}

	public void OnEndAnim()
	{
		isBeingTurnedOff = false;
		isBeingUsed = false;
		explainerObject.SetActive(false);
		transform.SetAsLastSibling();
		gameObject.SetActive(false);
	}
}
