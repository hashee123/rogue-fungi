﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardSceneScript : MonoBehaviour
{
	public RewardManager rewardManager;
	public GameObject chestCanvas;
	public GameObject battleRewardCanvas;

	void OnEnable()
	{	
		if (RewardData.rewardType == 0)	chestCanvas.SetActive(true);
		else if (RewardData.rewardType == 1)	battleRewardCanvas.SetActive(true);
	}
}
