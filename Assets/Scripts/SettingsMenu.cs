﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Localization.Settings;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
	public Dropdown qualityDropdown;
	public Dropdown languageDropdown;

	public Slider masterVolumeSlider;
	public Slider musicVolumeSlider;
	public Slider sfxVolumeSlider;

	void Awake()
	{
		GameManager.Instance.settingsMenu = this;
	}

	public void SetQuality(int index)
	{
		QualitySettings.SetQualityLevel(2*index);
		SettingsSaveData.Instance.qualitySettings = index;
		qualityDropdown.value = index;
	}

	public void SetLanguage(int index)
	{
		LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[index];
		SettingsSaveData.Instance.languageSettings = index;
		languageDropdown.value = index;
	}

	public void SetMasterVolumeSlider(float v)
	{
		masterVolumeSlider.value = v;
	}

	public void SetMusicVolumeSlider(float v)
	{
		musicVolumeSlider.value = v;
	}

	public void SetSFXVolumeSlider(float v)
	{
		sfxVolumeSlider.value = v;
	}
}
