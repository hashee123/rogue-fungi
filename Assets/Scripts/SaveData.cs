﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class SaveData
{
	[Header("General")]
	public GameState gameState;
	public int currentBossIndex;
	
	[Header("Mapa")]
	public int mapNumber;
	public bool[] isMapPlaceEnabled;
	public int placeIndexCurrent;
	public bool[] visitedPlaces;
	
	[Header("Deck")]
	public List<string> deckList;
	public List<int> levelOfEachCard;

	[Header("ShopSceneData")]
	public int[] shopCards = null;
	public List<int> cauldronBuffs = new List<int>();

	[Header("Static Class MapSceneTranstion")]
	public MapSceneTransitionData mapSceneTransitionData;

	[Header("Battle Setup Pool For Load Game")]
	public int[] battleSetupPoolIndexes;
	public int[] battleSetupPoolDifficulties;

	[Header("BATTLE SCREEN")]
	public BattleState battleState;
	public int enemiesKilled;
	public int totalEnemyStages;
	public int[] slots;
	public int[] cardsOnTable;
	public CardModifier[] cardModifiers;
	public List<string> deckInstantiatedList;
	public Queue<int> deckInstantiatedQueue;
	public Queue<int> graveyardCards;
	public CardModifier[] slotModifiers;
	public int[] slotModifiersPower;

	[Header("Player")]
	public PlayerBehaviourData playerBehaviourData;
	public string playerRelicHolding;

	[Header("Enemy")]
	public string enemyCard;
	public EnemyUnitData enemyUnitData;
	public EnemyActionPatternData enemyActionPatternData;

	[Header("UPGRADE SCREEN")]
	public string[] upgradeCardNames;
	public int[] upgradeCardIndex;

	[Header("REWARD SCREEN")]
	public string[] rewardCardNames;

	public void SaveDataFunction()
	{
		MapBehaviour mapBehaviour = GameManager.Instance.mapBehaviourScript;
		SaveGeneralData(mapBehaviour);

		switch (GameManager.Instance.state)
		{
			case GameState.BATTLE_SCREEN:
				SaveBattleData(GameManager.Instance.battleManager);
				break;

			case GameState.CHEST_SCREEN:
				SaveRewardsData();
				break;

			case GameState.REWARD_SCREEN:
				SaveRewardsData();
				break;

			case GameState.MAP_SCREEN:
				break;

			case GameState.SHOP_SCREEN:
				SaveShopData();
				break;

			case GameState.UPGRADE_SCREEN:
				SaveUpgradesData();
				break;

			case GameState.REST_SCREEN:
				break;
		}
	}

	private void SaveGeneralData(MapBehaviour mapBehaviour)
	{
		gameState = GameManager.Instance.state; // Estado do jogo
		//currentBossIndex = GameManager.Instance.currentBossIndex; // boss atual

		// Salva o estado do mapa se houver
		if (mapBehaviour != null)	SaveMapState(mapBehaviour);
		else	placeIndexCurrent = GameManager.Instance.actualPlace;
		SaveGeneralMapData();

		// Estado do player
		Debug.Log("Instance do player: " + PlayerBehaviour.Instance.playerBehaviourData);
		SavePlayerBehaviourData();

		// Estado do deck
		SaveDeckState();

		// Classe estatica MapSceneTransition
		SaveMapTransitionState();
	}

	private void SaveRewardsData()
	{
		rewardCardNames = GameManager.Instance.rewardManager.selectedCards;
	}

	private void SaveUpgradesData()
	{
		upgradeCardNames = GameManager.Instance.upgradeManager.selectedCardNames;
		upgradeCardIndex = GameManager.Instance.upgradeManager.selectedCardIndex;
	}

	private void SaveMapState(MapBehaviour mapBehaviour)
	{
		placeIndexCurrent = mapBehaviour.actualPlace;
		GameObject[] placesOnMap = mapBehaviour.placesOnMap;
		isMapPlaceEnabled = new bool[placesOnMap.Length];
		for (int i = 0; i < placesOnMap.Length; i++) isMapPlaceEnabled[i] = placesOnMap[i].GetComponent<Button>().interactable;
	}

	private void SaveGeneralMapData()
	{
        mapNumber = GameManager.Instance.mapNumber;
	    visitedPlaces = MapSceneTransition.mapSceneTransitionData.visitedPlaces;
		battleSetupPoolIndexes = GameManager.Instance.battleSetupPool.indexes;
		battleSetupPoolDifficulties = GameManager.Instance.battleSetupPool.difficulties;
	}

	private void SaveDeckState()
	{
		levelOfEachCard = DeckBehaviour.Instance.levelOfEachCard;
		List<GameObject> deckListToSave = DeckBehaviour.Instance.deckList;
		deckList = new List<string>();
		for (int i = 0; i < deckListToSave.Count; i++)	deckList.Add(deckListToSave[i].name);
	}

	private void SaveMapTransitionState()
	{
		mapSceneTransitionData = MapSceneTransition.mapSceneTransitionData;
	}

	private void SaveBattleData(BattleManager battleManager)
	{
		// Estado da batalha
		SaveBattleState(battleManager);

		// Salva todas as cartas instanciadas
		List<GameObject> deckInstantiatedListToSave = SaveInstantiatedCards();

		// Salva o estado da mesa
		SaveTableState(deckInstantiatedListToSave);

		// Salva fila de cartas para sacar
		SaveCardQueue(deckInstantiatedListToSave);

		// Salva cartas no cemiterio
		SaveGraveyardCards(deckInstantiatedListToSave);

		// Salva os modificadores de slot
		SaveModifierSlots(battleManager);

		// Salva o resto
		SavePlayerBehaviourData();
		SaveEnemyStatus(battleManager);
	}

	public void SaveBattleState(BattleManager battleManager)
	{
		battleState = battleManager.state;
		totalEnemyStages = battleManager.totalEnemyStages;
		enemiesKilled = battleManager.enemiesKilled;
	}

	public List<GameObject> SaveInstantiatedCards()
	{
		List<GameObject> deckInstantiatedListToSave = DeckBehaviour.Instance.deckInstantiatedList;
		deckInstantiatedList = new List<string>();
		foreach (GameObject card in deckInstantiatedListToSave)	deckInstantiatedList.Add(card.name.Replace("(Clone)", ""));
		return deckInstantiatedListToSave;
	}

	public void SaveTableState(List<GameObject> deckInstantiatedListToSave)
	{
		GameObject[] cardsOnTableToSave = DeckBehaviour.Instance.onTable;
		cardsOnTable = new int[cardsOnTableToSave.Length];
		slots = new int[3];
		cardModifiers = new CardModifier[3];
		for (int i = 0; i < cardsOnTable.Length; i++)
		{
			cardsOnTable[i] = deckInstantiatedListToSave.IndexOf(cardsOnTableToSave[i]);
			CardStatus cardStatus = cardsOnTableToSave[i].GetComponent<CardStatus>();
			slots[i] = cardStatus.slotIsOn;
			cardModifiers[i] = cardStatus.actualModifier;
		}
	}

	public void SaveCardQueue(List<GameObject> deckInstantiatedListToSave)
	{
		Queue<GameObject> deckInstantiatedQueueToSave = DeckBehaviour.Instance.deckInstantiatedQueue;
		deckInstantiatedQueue = new Queue<int>();
		foreach (GameObject card in deckInstantiatedQueueToSave)	deckInstantiatedQueue.Enqueue(deckInstantiatedListToSave.IndexOf(card));
	}

	public void SaveGraveyardCards(List<GameObject> deckInstantiatedListToSave)
	{
		Queue<GameObject> graveyardQueueToSave = DeckBehaviour.Instance.graveyardCards;
		graveyardCards = new Queue<int>();
		foreach (GameObject card in graveyardQueueToSave)	graveyardCards.Enqueue(deckInstantiatedListToSave.IndexOf(card));
	}

	public void SaveModifierSlots(BattleManager battleManager)
	{
		slotModifiers = new CardModifier[3];
		slotModifiersPower = new int[3];
		for (int i = 0; i < 3; i++)
		{
			SlotBehaviour slotB = battleManager.objectReferences.slots[i].GetComponent<SlotBehaviour>();
			slotModifiers[i] = slotB.actualModifier;
			slotModifiersPower[i] = slotB.powerSlot;
		}
	}

	private void SaveShopData()
	{
		shopCards = GameManager.Instance.shopManager.shopCards;
		cauldronBuffs = GameManager.Instance.cauldronManager.cauldronBuffs;
	}

	private void SavePlayerBehaviourData()
	{
		playerBehaviourData = PlayerBehaviour.Instance.playerBehaviourData;
		if (PlayerBehaviour.Instance.relicHolding != null)	playerRelicHolding = PlayerBehaviour.Instance.relicHolding.name;
		else playerRelicHolding = null;
	}

	private void SaveEnemyStatus(BattleManager battleManager)
	{
		enemyCard = battleManager.enemyCard[0].name.Replace("(Clone)", ""); // Nome do inimigo
		enemyActionPatternData = battleManager.objectReferences.enemyActionPatternScript.enemyActionPatternData;
		enemyUnitData = battleManager.enemyUnitScript.enemyUnitData;
	}

/*
	public void MatchSave<T>(Dictionary<string, T> storage, object data)
	{
		foreach(KeyValuePair<string, T> valorStorage in storage) 
			foreach(KeyValuePair<string, T> valorData in data)
				if(valorStorage.Key == valorData.Key)	storage[valorStorage.Key] = valorData.Value;
	}

	public void InitializeDict<T>(Dictionary<string, T> dictionary, object data)
	{
		var properties = data.GetType().GetProperties();
		foreach(PropertyInfo property in properties)
			if(property.PropertyType == typeof(T))	dictionary.Add(property.Name, Activator.CreateInstance<T>());
	}
*/
}
