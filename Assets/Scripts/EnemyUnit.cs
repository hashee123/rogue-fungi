﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemyUnitData
{
	[Header("Saving Settings")]
	public string savingDialogue;

	[Header("Original Settings")]
	public int originalHealth;

	//seria bom fazer um HideInSpector() depois de acabar o prototipo
	[Header("Actual Settings")]
	public int actualHealth;
	public int actualProtection;
	public int actualPoison; // debuff no inimigo
	public int actualSpecialProtection;
	public int gainedStrength = 0;
	//public int count = 1; // Indica o ataque do enemy action pattern, talvez precise definir aqui, para poder pegar esse valor na hora de salvar o jogo. Ai atualiza aqui sempre que o count do enemyActionPattern atualizar

	[Header("Skills & Powers")]
	public int actualAttack1;
	public int actualAttack2;
	public int actualAttack3;
	public int actualPoisonPower;
	public int actualProtectionPower;
	
	[Header("Actions")]
	public int currentStage = 0;
	//public EnemyStage stage;

	[Header("Others")]
	public float scale;
	//public EnemyActionsOrder[] actionsOrder;

	//public int actionsVariations; // valido para o inimigo, define quantas acoes diferentes aparecem no Random dele TEMPORARIO

	[Header("Enemy Stage Based")]
	public int[] attack1PerStage;
	public int[] attack2PerStage;
	public int[] attack3PerStage;
	public int[] protectionPerStage;
	public int[] healthPerStage;
	public int[] specialProtectionPerStage;	
}

public class EnemyUnit : MonoBehaviour
{
	public EnemyUnitData enemyUnitData;

	[Header("Saving Settings")]
	public Relic relicDropped;

	[Header("Actions")]
	public EnemyActionPattern actionPattern;

	[Header("Color")]
	public Color normalColor;
	public Color damageColor;
	public Color poisonColor;
}
