﻿/* Autor: Klinsmann Silva Hengles Cordeiro
   Agosto de 2020
   ScriptableObject contendo os dados de uma determinada carta
   OBS: E' usado para armazenar variaveis que nao serao modificadas, e' como um template para criar algo novo*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Card", menuName = "Card")]
public class CardUnit : ScriptableObject
{
	//public string cardName;
	[TextArea(2, 5)]
	public string description;

	public GameObject cardPrefab;

	//public Sprite cardArt;

	/*[Header("Initial Settings")]
	  public int cardInitialAttack;
	  public int cardInitialHealth;*/

	//seria bom fazer um HideInSpector() depois de acabar o prototipo
	/*[Header("Actual Settings")]
	  public int cardActualAttack;
	  public int cardActualHealth;*/

	/*[Header("Color")]
	  public Color normalColor;
	  public Color damageColor;
	  public Color poisonColor;*/

	//public int actionsVariations; // valido para o inimigo, define quantas acoes diferentes aparecem no Random dele

}
