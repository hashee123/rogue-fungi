﻿/* Autor: Klinsmann Silva Hengles Cordeiro
   Agosto de 2020
   Programa que cuida do display do inimigo na tela*/

using CardCopy;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.UI;
using DG.Tweening;

public class EnemyDisplay : MonoBehaviour
{
	public EnemyUnit thisEnemyUnit;
	public Animator animator; // this enemy animator
	public SpriteRenderer enemySpriteRenderer; // imagem desse inimigo
	public BattleUIManager battleUIManager;

	[Header("Saving Sequence")]
	public GameObject[] objectsToDeactivate;
	public GameObject[] objectsToActivate;

    [Header("Shaking")]
    [SerializeField] private float shakeDuration;
    [SerializeField] private float shakeStrength;

    private void Start()
    {
        battleUIManager = GameObject.FindGameObjectWithTag("BattleUIManager").GetComponent<BattleUIManager>();
    }

	public IEnumerator VisualFleeFromBattle()
	{
		battleUIManager.battleManagerScript.SetBattleState(BattleState.WON);
		battleUIManager.battleManagerScript.DeactivateCardsExcept(-1);
		transform.DOShakePosition(1.0f, 40.0f, 10, randomnessMode: ShakeRandomnessMode.Harmonic);

		yield return new WaitForSeconds(0.9f);

		Vector3[] waypoints = {
  			transform.position - new Vector3(20.0f, 0.0f, 0.0f),
			new Vector3(battleUIManager.rightEdgeOfScreen.position.x + 150.0f,
			            transform.position.y,
						transform.position.z)
		};

		transform.DOMove(waypoints[0], 0.3f).SetEase(Ease.OutCirc);

		yield return new WaitForSeconds(0.23f);

		Instantiate(battleUIManager.battleUIParticles.fleeParticles,
					battleUIManager.battleUIParticles.fleeParticles.transform.position,
					battleUIManager.battleUIParticles.fleeParticles.transform.rotation,
					battleUIManager.battleUI.transform);
		transform.DOMove(waypoints[1], 1.0f).SetEase(Ease.OutExpo);
		enemySpriteRenderer.DOFade(0.0f, 0.7f).SetEase(Ease.OutExpo);

		yield return new WaitForSeconds(0.65f);

		GameManager.Instance.battleManager.enemiesKilled += 1; // adiciona um inimigo morto na contagem de mortes
		GameManager.Instance.battleManager.CheckEnemiesKilled(); // checa se o jogador ja ganhou a batalha
	}

    public IEnumerator VisualTakingDamage(int dmgToTake)
    {
        //Debug.Log("IS BEING CALLED");

        if (dmgToTake > 0)
        {
            battleUIManager.dmgTakenText.GetComponent<Text>().text = "-" + dmgToTake.ToString();
            battleUIManager.dmgTakenText.SetActive(true);
            ShakeEnemy();
            ChangeToBeingDamagedColor();
        }
        
        battleUIManager.explosion.SetActive(true);
        battleUIManager.explosion2.SetActive(true);

		yield return new WaitForSeconds(0.5f); // 0.333 é o tempo da animacao de shake

        battleUIManager.dmgTakenText.SetActive(false);
        battleUIManager.explosion.SetActive(false);

		yield return new WaitForSeconds(0.4f); // pra terminar a explosao que rotaciona

		battleUIManager.explosion2.SetActive(false);
	}

    public IEnumerator VisualTakingPoisonDamage(int dmgToTake)
    {
        battleUIManager.dmgTakenText.GetComponent<Text>().text = "-" + dmgToTake.ToString();
        battleUIManager.dmgTakenText.SetActive(true);

        ShakeEnemy();
        ChangeColorToPoison();

        yield return new WaitForSeconds(0.5f);

        battleUIManager.dmgTakenText.SetActive(false);
    }

    public IEnumerator VisualDefeatedAnimation(int dmgToTake)
    {
        animator.SetBool("IsDefeated", true);
        yield return new WaitForSeconds(1f);
        animator.SetBool("IsDefeated", false);
    }

    public void ChangeColorToPoison()
    {
        enemySpriteRenderer.DOColor(thisEnemyUnit.poisonColor, 0.3f).SetEase(Ease.InQuad).onComplete = BackToNormalColor;
    }

    public void ShakeEnemy()
    {
        transform.DOShakePosition(shakeDuration, shakeStrength);
    }

    // Make the enemy color to being damaged color (red)
    public void ChangeToBeingDamagedColor()
    {
        enemySpriteRenderer.DOColor(Color.red, 0.3f).SetEase(Ease.InQuad).onComplete = BackToNormalColor;
    }

    // Make the enemy color back to its normal color (white)
    public void BackToNormalColor()
    {
        enemySpriteRenderer.DOColor(thisEnemyUnit.normalColor, 0.3f).SetEase(Ease.OutQuad);
    }

}
