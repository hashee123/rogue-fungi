using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New BattleSetupPool", menuName = "BattleSetupPool")]
public class BattleSetupPool : ScriptableObject
{
	public BattleSetup[] battleSetupPoolLow;
	public BattleSetup[] battleSetupPoolMed;
	public BattleSetup[] battleSetupPoolHigh;
	public BattleSetup[] battleSetupPoolBoss;

	// Para guardar os indices dos mapas que foram randomizados
	// Cada mapa possui uma dificuldade, que seleciona o array de qual ele origina
	// O indice se refere ao array especifico do diff
	public int[] difficulties;
	public int[] indexes;
}
