﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RelicShopItem : ShopItem
{
	public override void SetupShopItem(GameObject prefabToAdd)
	{
		base.SetupShopItem(prefabToAdd);
		priceText.text = instantiatedObject.GetComponent<Relic>().shopPrice.ToString();
	}

	public override void AddToPlayer()
	{
		// Não sei como adiciona relíquia ainda
	}

	public override void AfterPurchase()
	{

	}

	public override void OnEndAnim()
	{

	}
}
