using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EAP_ResearcherFrog : EnemyActionPattern
{
	private IEnumerator nextMove; // proxima acao que o inimigo vai executar

	public override IEnumerator ApplyAction()
	{
		return nextMove;
	}

	public override void ActionPatternStage1()
	{
		switch (enemyActionPatternData.count)
		{
			case 1:
				enemyAction.PrepareToTiredness(1);
				nextMove = enemyAction.Tiredness(1, 2);
				enemyActionPatternData.count++;
				break;
			case 2:
				enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				enemyActionPatternData.count++;
				break;
			case 3:
				enemyAction.PrepareToProtect(enemyAction.enemyUnit.enemyUnitData.actualProtectionPower);
				nextMove = enemyAction.Protect(enemyAction.enemyUnit.enemyUnitData.actualProtectionPower);
				enemyActionPatternData.count++;
				break;
			case 4:
				enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack2);
				nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack2);
				enemyActionPatternData.count = 1;
				break;
		}
	}

	public override void ActionPatternStage2()
	{
		switch (enemyActionPatternData.count)
		{
			case 1:
				enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				enemyActionPatternData.count++;
				break;
			case 2:
				enemyAction.PrepareToTiredness(1);
				nextMove = enemyAction.Tiredness(1, 2);
				enemyActionPatternData.count++;
				break;
			case 3:
				// 50% Ataca vs 50% Cansaco
				float randomNumber = Random.value;
				if (randomNumber <= 0.5)
				{
					enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack2);
					nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack2);
				}
				else
				{
					enemyAction.PrepareToProtect(enemyAction.enemyUnit.enemyUnitData.actualProtectionPower);
					nextMove = enemyAction.Protect(enemyAction.enemyUnit.enemyUnitData.actualProtectionPower);
				}
				enemyActionPatternData.count = 1;
				break;
		}
	}

	public override void ActionPatternStage3()
	{
		switch(enemyActionPatternData.count)
		{
			case 1:
				enemyAction.PrepareToProtect(enemyAction.enemyUnit.enemyUnitData.actualProtectionPower);
				nextMove = enemyAction.Protect(enemyAction.enemyUnit.enemyUnitData.actualProtectionPower);
				enemyActionPatternData.count++;
				break;
			case 2:
				enemyAction.PrepareToTiredness(2);
				nextMove = enemyAction.Tiredness(2, 2);
				enemyActionPatternData.count++;
				break;
			case 3:
				enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				enemyActionPatternData.count++;
				break;
			case 4:
				enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack2);
				nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack2);
				enemyActionPatternData.count = 2;
				break;
		}
	}
}
