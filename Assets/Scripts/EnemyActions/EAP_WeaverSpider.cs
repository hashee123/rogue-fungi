﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EAP_WeaverSpider : EnemyActionPattern
{
	//private int count = 1; // usado para definir as acoes do lobo
	private IEnumerator nextMove; // proxima acao que o inimigo vai executar

	public override IEnumerator ApplyAction()
	{
		return nextMove;
	}

	public override void ActionPatternStage1()
	{
		switch(enemyActionPatternData.count)
		{
			case 1:
				break;
			case 2:
				break;
		}
	}

	public override void ActionPatternStage2()
	{
		switch (enemyActionPatternData.count)
		{
			case 1:
				break;
			case 2:
				break;
			case 3:
				break;
		}
	}

	public override void ActionPatternStage3()
	{
		switch (enemyActionPatternData.count)
		{
			case 1:
				break;
			case 2:
				break;
			case 3:
				break;
		}
	}

}
