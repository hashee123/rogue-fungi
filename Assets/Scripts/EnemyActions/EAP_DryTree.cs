﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EAP_DryTree : EnemyActionPattern
{
	//private int count = 1; // usado para definir as acoes do lobo
	private IEnumerator nextMove; // proxima acao que o inimigo vai executar

	public override IEnumerator ApplyAction()
	{
		return nextMove;
	}

	public override void ActionPatternStage1()
	{
		switch (enemyActionPatternData.count)
		{
			case 1: // Protege
				enemyAction.PrepareToProtect(enemyAction.enemyUnit.enemyUnitData.actualProtectionPower);
				nextMove = enemyAction.Protect(enemyAction.enemyUnit.enemyUnitData.actualProtectionPower);
				enemyActionPatternData.count++;
				break;
			case 2: // Maldicao
				enemyAction.PrepareToCurse(3);
				nextMove = enemyAction.Curse(3);     
				enemyActionPatternData.count++;
				break;
			case 3: // Ataque 1
				enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				enemyActionPatternData.count++;
				break;
			case 4: // Protege
				enemyAction.PrepareToProtect(enemyAction.enemyUnit.enemyUnitData.actualProtectionPower);
				nextMove = enemyAction.Protect(enemyAction.enemyUnit.enemyUnitData.actualProtectionPower);
				enemyActionPatternData.count++;
				break;
			case 5: // Ataque 2 (Mais forte)
				enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack2);
				nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack2);
				enemyActionPatternData.count = 1;
				break;
		}
	}

	public override void ActionPatternStage2()
	{
		switch (enemyActionPatternData.count)
		{
			case 1:
				enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				enemyActionPatternData.count++;
				break;
			case 2:
				enemyAction.PrepareToProtect(enemyAction.enemyUnit.enemyUnitData.actualProtectionPower);
				nextMove = enemyAction.Protect(enemyAction.enemyUnit.enemyUnitData.actualProtectionPower);
				enemyActionPatternData.count++;
				break;
			case 3:
				enemyAction.PrepareToCurse(3);
				nextMove = enemyAction.Curse(3);
				enemyActionPatternData.count++;
				break;
			case 4:
				enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack1 + 9);
				nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				enemyActionPatternData.count++;
				break;
			case 5:
				enemyAction.PrepareToGainStrength(2);
				nextMove = enemyAction.GainStrength(2);
				enemyActionPatternData.count = 1;
				break;
		}
	}

	public override void ActionPatternStage3()
	{
		switch (enemyActionPatternData.count)
		{
			case 1:
				enemyAction.PrepareToSpecialProtect(enemyAction.enemyUnit.enemyUnitData.specialProtectionPerStage[enemyActionPatternData.enemyStage]);
				nextMove = enemyAction.SpecialProtect(enemyAction.enemyUnit.enemyUnitData.specialProtectionPerStage[enemyActionPatternData.enemyStage], 2);
				enemyActionPatternData.count++;
				break;
			case 2:
				enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				enemyActionPatternData.count++;
				break;
			case 3:
				enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				enemyActionPatternData.count = 1;
				break;
		}
	}

}
