﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EAP_GhostGardener : EnemyActionPattern
{
	public int[] specialProtectionTurns;
	//private int count = 1; // usado para definir as acoes do lobo
	private IEnumerator nextMove; // proxima acao que o inimigo vai executar

	public override IEnumerator ApplyAction()
	{
		return nextMove;
	}

	public override void ActionPatternStage1()
	{
		switch(enemyActionPatternData.count)
		{
			case 1:
				enemyAction.PrepareToSpecialProtect(enemyAction.enemyUnit.enemyUnitData.specialProtectionPerStage[0]);
				nextMove = enemyAction.SpecialProtect(enemyAction.enemyUnit.enemyUnitData.specialProtectionPerStage[0], specialProtectionTurns[0]);
				enemyActionPatternData.count++;
				break;
			case 2:
				enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				enemyActionPatternData.count++;
				break;
			case 3:
				enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack2);
				nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack2);
				enemyActionPatternData.count = 1;
				break;
		}
	}

	public override void ActionPatternStage2()
	{
		switch (enemyActionPatternData.count)
		{
			case 1:
				int index = Random.Range(0, 3);
				enemyAction.PrepareToSlotBlock(index, 1);
				nextMove = enemyAction.SlotBlock(index, 1);
				enemyActionPatternData.count++;
				break;
			case 2:
				enemyAction.PrepareToSpecialProtect(enemyAction.enemyUnit.enemyUnitData.specialProtectionPerStage[1]);
				nextMove = enemyAction.SpecialProtect(enemyAction.enemyUnit.enemyUnitData.specialProtectionPerStage[1], specialProtectionTurns[1]);
				enemyActionPatternData.count++;
				break;
			case 3:
				enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				enemyActionPatternData.count = 1;
				break;
		}
	}

	public override void ActionPatternStage3()
	{
		switch (enemyActionPatternData.count)
		{
			case 1:
				enemyAction.PrepareToSpecialProtect(enemyAction.enemyUnit.enemyUnitData.specialProtectionPerStage[2]);
				nextMove = enemyAction.SpecialProtect(enemyAction.enemyUnit.enemyUnitData.specialProtectionPerStage[2], specialProtectionTurns[2]);
				enemyActionPatternData.count++;
				break;
			case 2:
				enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				enemyActionPatternData.count++;
				break;
			case 3:
				enemyAction.PrepareToDestruction(3);
				nextMove = enemyAction.Destruction(3);
				enemyActionPatternData.count = 1;
				break;
		}
	}

}
