﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAction: MonoBehaviour
{
	public EnemyUnit enemyUnit;
	public BattleManager battleManagerScript;

	private void Awake()
	{
		enemyUnit = GameObject.FindGameObjectWithTag("Enemy").GetComponent<EnemyUnit>();
	}

	public void PrepareAttack(int damage)
	{
		battleManagerScript.objectReferences.action.SetActive(true);
		battleManagerScript.objectReferences.actionBehaviour.ChangeToAttack(damage);
	}

	public IEnumerator Attack(int damage) // Ataque fisico basico
	{
		BattleUIParticles battleUIParticles = battleManagerScript.battleUIManager.battleUIParticles;

		Instantiate(battleUIParticles.enemyBite,
					battleUIParticles.enemyAttackPosition.position,
					battleUIParticles.enemyAttackPosition.rotation,
					battleManagerScript.battleUIManager.battleUI.transform);

		yield return new WaitForSeconds(0.3f);

		PlayerBehaviour.Instance.TakeDamage(damage);

		battleManagerScript.objectReferences.actionBehaviour.Activate();

		yield return new WaitForSeconds(0.2f);

		// battleManagerScript.action.SetActive(false);
	}
	public void PrepareToProtect(int protectionPower)
	{
		battleManagerScript.objectReferences.action.SetActive(true);
		battleManagerScript.objectReferences.actionBehaviour.ChangeToProtect(protectionPower);
	}

	public IEnumerator Protect(int protectionPower) // Ganha protecao basica
	{
		yield return StartCoroutine(battleManagerScript.enemyCard[0].GetComponent<EnemyBehaviour>().EnemyToProtection(protectionPower));
		battleManagerScript.objectReferences.actionBehaviour.Activate();

		// battleManagerScript.action.SetActive(false);
	}

	public void PrepareToClumsiness(int turns)
	{
		battleManagerScript.objectReferences.action.SetActive(true);
		battleManagerScript.objectReferences.actionBehaviour.ChangeToClumsiness(turns);
	}

	public IEnumerator Clumsiness(int turns) // Troca cartas de posicao na mesa por tantos turno
	{
		yield return new WaitForSeconds(0.2f);

		PlayerBehaviour.Instance.playerBehaviourData.clumsinessCurrent = turns;
		PlayerBehaviour.Instance.battleUIManagerScript.ShowPlayerClumsiness();
		battleManagerScript.objectReferences.actionBehaviour.Activate();

		yield return new WaitForSeconds(0.2f);
	}

	public void ToHeal(int actualHealth, int toCure) // Cura o player
	{
		actualHealth += toCure;
		battleManagerScript.objectReferences.action.SetActive(false);
	}

	public void PrepareToHeal(ActionSlotBehaviour actionBehaviour, GameObject action, int healPower)
	{
		action.SetActive(true);
	}

	public IEnumerator NastyPresent(GameObject card, int number) // Coloca um numero <number> de cartas armadilhas no deck
	{
		yield return new WaitForSeconds(0.2f);

		for (int i = 1; i <= number; i++)
		{
			GameObject cardInstance = Instantiate(card, battleManagerScript.objectReferences.battleUIManagerScript.battleUI.transform, false);
			//battleManagerScript.DeckBehaviour.Instance.deckInstantiatedList.Add(cardInstance);
			DeckBehaviour.Instance.deckInstantiatedQueue.Enqueue(cardInstance);
			PlayerBehaviour.Instance.battleUIManagerScript.ShowDeckNumberCards();
			PlayerBehaviour.Instance.battleUIManagerScript.ShowGraveyardNumberCards();
		}

		battleManagerScript.objectReferences.actionBehaviour.Activate();

		yield return new WaitForSeconds(0.2f);
	}

	public void PrepareToNastyPresent()
	{
		battleManagerScript.objectReferences.action.SetActive(true);
		battleManagerScript.objectReferences.actionBehaviour.ChangeToNastyPresent();
	}

	public IEnumerator GainStrength(int strength) // Inimigo ganha mais dano por certo tempo. Repensando...
	{
		BattleUIParticles battleUIParticles = battleManagerScript.battleUIManager.battleUIParticles;

		Instantiate(battleUIParticles.gainStrenghtParticles,
					battleUIParticles.particleSpawnPosition.position,
					battleUIParticles.particleSpawnPosition.rotation,
					battleManagerScript.battleUIManager.battleUI.transform);

		yield return new WaitForSeconds(0.2f);

		enemyUnit.enemyUnitData.gainedStrength = strength;
		enemyUnit.enemyUnitData.actualAttack1 += strength;
		PlayerBehaviour.Instance.battleUIManagerScript.SetGainedStrengthUI();
		battleManagerScript.objectReferences.actionBehaviour.Activate();

		yield return new WaitForSeconds(0.2f);
	}

	public void RemoveGainedStrength()
	{
		enemyUnit.enemyUnitData.actualAttack1 = enemyUnit.enemyUnitData.attack1PerStage[enemyUnit.enemyUnitData.currentStage];
		enemyUnit.enemyUnitData.actualAttack2 = enemyUnit.enemyUnitData.attack2PerStage[enemyUnit.enemyUnitData.currentStage];
		PlayerBehaviour.Instance.battleUIManagerScript.SetGainedStrengthUI();
	}

	public void PrepareToGainStrength(int strength)
	{
		battleManagerScript.objectReferences.action.SetActive(true);
		battleManagerScript.objectReferences.actionBehaviour.ChangeToGainStrength(strength);
	}

	public IEnumerator Tiredness(int ammount, int turns) // Player tem menos pontos de acao por tantos turnos
	{
		BattleUIParticles battleUIParticles = battleManagerScript.battleUIManager.battleUIParticles;
		Instantiate(battleUIParticles.sleepParticles,
					battleUIParticles.particleSpawnPosition.position,
					battleUIParticles.particleSpawnPosition.rotation,
					battleManagerScript.battleUIManager.battleUI.transform);

		yield return new WaitForSeconds(0.4f);

		PlayerBehaviour.Instance.playerBehaviourData.tirednessCurrent = ammount;
		PlayerBehaviour.Instance.playerBehaviourData.tirednessTurns = turns;
		PlayerBehaviour.Instance.battleUIManagerScript.ShowPlayerTiredness();
		battleManagerScript.objectReferences.actionBehaviour.Activate();


		yield return new WaitForSeconds(0.2f);
	}

	public void PrepareToTiredness(int ammount)
	{
		battleManagerScript.objectReferences.action.SetActive(true);
		battleManagerScript.objectReferences.actionBehaviour.ChangeToTiredness(ammount);
	}

	// Essa ideia seria o mesmo resultado do cansaço se todas as cartas tiverem o mesmo custo
	// public IEnumerator Inflation(int ammount)
	// {
	//     yield return new WaitForSeconds(0.2f);



	// 	yield return new WaitForSeconds(0.2f);
	// }

	// public void PrepareToInflation(int ammount)
	// {

	// }

	public IEnumerator SlotBlock(int index, int turns) // Bloqueia a carta que estiver naquele slot por tantos turnos
	{
		yield return new WaitForSeconds(0.2f);

		battleManagerScript.SlotBlock(index);

		PlayerBehaviour.Instance.playerBehaviourData.slotBlockCurrent[index] = turns;

		// PlayerBehaviour.Instance.battleUIManagerScript.slotBlockOverlayText[index].text = turns.ToString();

		battleManagerScript.objectReferences.actionBehaviour.Activate();

		yield return new WaitForSeconds(0.2f);
	}

	public void SlotClear(int index)
	{
		battleManagerScript.SlotClear(index);
	}

	public void PrepareToSlotBlock(int index, int turns)
	{
		battleManagerScript.objectReferences.action.SetActive(true);
		string slot = "";
		switch (index)
		{
			case 0:
				slot = "◄";
				break;
			case 1:
				slot = "▼";
				break;
			case 2:
				slot = "►";
				break;
		}

		slot = slot + turns;
		battleManagerScript.objectReferences.actionBehaviour.ChangeToSlotBlock(slot);
	}

	public void PrepareToSpecialProtect(int specialProtection)
	{
		battleManagerScript.objectReferences.action.SetActive(true);
		battleManagerScript.objectReferences.actionBehaviour.ChangeToSpecialProtect(specialProtection);
		Logging.Log("Special protection: " + specialProtection);
	}

	public IEnumerator SpecialProtect(int specialProtection, int turns) // Inimigo so pode tomar uma quantidade maxima de dano por tantos turnos
	{
		yield return StartCoroutine(battleManagerScript.enemyCard[0].GetComponent<EnemyBehaviour>().EnemyToSpecialProtection(specialProtection, turns));
		battleManagerScript.objectReferences.actionBehaviour.Activate();
		// battleManagerScript.action.SetActive(false);
	}

	public void RemoveSpecialProtection()
	{
		EnemyBehaviour enemyBehaviour = battleManagerScript.enemyCard[0].GetComponent<EnemyBehaviour>();
		enemyBehaviour.thisEnemyUnit.enemyUnitData.actualSpecialProtection = 0;
		PlayerBehaviour.Instance.battleUIManagerScript.ShowEnemySpecialProtection();
	}

	public void PrepareToDestruction(int turns)
	{
		battleManagerScript.objectReferences.action.SetActive(true);
		battleManagerScript.objectReferences.actionBehaviour.ChangeToDestruction(turns);
	}

	public IEnumerator Destruction(int turns) // Todas as cartas da mesa sao substituidas no final do turno por tantos turnos
	{
		yield return new WaitForSeconds(0.2f);

		battleManagerScript.destructionLasts = turns;
		PlayerBehaviour.Instance.battleUIManagerScript.ShowDestruction();
		battleManagerScript.objectReferences.actionBehaviour.Activate();

		yield return new WaitForSeconds(0.2f);
	}

	public IEnumerator Curse(int turns)
	{
		BattleUIParticles battleUIParticles = battleManagerScript.battleUIManager.battleUIParticles;

		Instantiate(battleUIParticles.enemyCurse,
					battleUIParticles.enemyAttackPosition.position,
					battleUIParticles.enemyAttackPosition.rotation,
					battleManagerScript.battleUIManager.battleUI.transform);

		yield return new WaitForSeconds(0.4f);

		PlayerBehaviour.Instance.playerBehaviourData.curseCurrent = turns;
		PlayerBehaviour.Instance.battleUIManagerScript.ShowPlayerCurse();
		battleManagerScript.objectReferences.actionBehaviour.Activate();

		yield return new WaitForSeconds(0.2f);
	}

	public void PrepareToCurse(int turns)
	{
		battleManagerScript.objectReferences.action.SetActive(true);
		battleManagerScript.objectReferences.actionBehaviour.ChangeToCurse(turns);
	}

}
