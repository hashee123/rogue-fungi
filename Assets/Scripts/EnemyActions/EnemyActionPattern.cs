﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemyActionPatternData
{
	public int enemyStage = 0;
	public int enemyMaxStage = 0;
	public int count = 1; // contador interno do inimigo para determinar a sequencia de acoes
}

public class EnemyActionPattern : MonoBehaviour
{
	public static EnemyAction enemyAction;

	public EnemyActionPatternData enemyActionPatternData;

	void Awake()
	{
		enemyAction = GameObject.FindGameObjectWithTag("EnemyActionManager").GetComponent<EnemyAction>();      
	}

	public virtual void DecideAction()
	{
		switch (enemyActionPatternData.enemyStage)
		{
			case 0:
				ActionPatternStage1();
				break;
			case 1:
				ActionPatternStage2();
				break;
			case 2:
				ActionPatternStage3();
				break;
		}
	}

	public virtual IEnumerator ApplyAction()
	{
		yield return new WaitForSeconds(0.1f);
	}

	public virtual void ActionPatternStage1() { }

	public virtual void ActionPatternStage2() { }

	public virtual void ActionPatternStage3() { }
}
