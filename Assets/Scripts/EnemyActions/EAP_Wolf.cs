﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EAP_Wolf : EnemyActionPattern
{
	public GameObject nastyPresentCard;
	//private int enemyActionPatternData.count = 1; // usado para definir as acoes do lobo
	private IEnumerator nextMove; // proxima acao que o inimigo vai executar

	public override IEnumerator ApplyAction()
	{
		return nextMove;
	}

	public override void ActionPatternStage1()
	{
		switch(enemyActionPatternData.count)
		{
			case 1:
				// Presente Desagradavel
				enemyAction.PrepareToNastyPresent();
				nextMove = enemyAction.NastyPresent(nastyPresentCard, 2);
				enemyActionPatternData.count++;
				break;

			case 2:
				// 70% Protege vs 30% Ataca
				float randomNumber = Random.value;
				if(randomNumber <= 0.3)
				{
					enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack2);
					nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack2);
				}
				else
				{
					enemyAction.PrepareToProtect(enemyAction.enemyUnit.enemyUnitData.actualProtectionPower);
					nextMove = enemyAction.Protect(enemyAction.enemyUnit.enemyUnitData.actualProtectionPower);
				}

				enemyActionPatternData.count++;
				break;
			case 3:
				// Ataca
				enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				enemyActionPatternData.count = 1;
				break;
		}
	}

	public override void ActionPatternStage2()
	{
		switch (enemyActionPatternData.count)
		{
			case 1:
				// Ataca
				enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				enemyActionPatternData.count++;
				break;
			case 2:
				// 50% Protege vs 50% Ataca
				float randomNumber = Random.value;
				if (randomNumber <= 0.5)
				{
					enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack2);
					nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack2);
				}
				else
				{
					enemyAction.PrepareToProtect(enemyAction.enemyUnit.enemyUnitData.actualProtectionPower);
					nextMove = enemyAction.Protect(enemyAction.enemyUnit.enemyUnitData.actualProtectionPower);
				}

				enemyActionPatternData.count++;
				break;
			case 3:
				// Presente Desagradavel
				enemyAction.PrepareToNastyPresent();
				nextMove = enemyAction.NastyPresent(nastyPresentCard, 3);
				enemyActionPatternData.count = 1;
				break;
		}
	}

	public override void ActionPatternStage3()
	{
		switch(enemyActionPatternData.count)
		{
			case 1:
				enemyAction.PrepareAttack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				nextMove = enemyAction.Attack(enemyAction.enemyUnit.enemyUnitData.actualAttack1);
				enemyActionPatternData.count++;
				break;
			case 2:
				enemyAction.PrepareToClumsiness(1);
				nextMove = enemyAction.Clumsiness(1);
				enemyActionPatternData.count++;
				break;
			case 3:
				// Presente Desagradavel
				enemyAction.PrepareToNastyPresent();
				nextMove = enemyAction.NastyPresent(nastyPresentCard, 3);
				enemyActionPatternData.count = 1;
				break;
		}
	}

}
