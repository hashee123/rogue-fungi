﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleAnimation : MonoBehaviour
{
	public float timer = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
		IEnumerator coroutine = SelfDestruct();
        StartCoroutine(coroutine);
    }

	IEnumerator SelfDestruct()
	{
		yield return new WaitForSeconds(timer);
		Destroy(gameObject);
	}
}
