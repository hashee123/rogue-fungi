﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Relic", menuName = "Relic")]
public class Relic : ScriptableObject
{
	public Sprite sprite;
	public BattleSetup bossThatIsSaved;
	public int shopPrice;
}
