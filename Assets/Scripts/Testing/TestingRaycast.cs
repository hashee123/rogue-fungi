﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestingRaycast : MonoBehaviour
{
	Ray ray;
	RaycastHit x;

	void Update()
	{
		ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (Physics.Raycast(ray, out x))
		{
			print(x.collider.name);
		}
	}
}
