﻿/* Autor: Klinsmann Silva Hengles Cordeiro
   Agosto de 2020
   ScriptableObject com os dados de como deve ser uma determinada batalha*/

using UnityEngine;
using UnityEngine.UI;

public enum BattleConfig { ONE_ENEMY, TWO_ENEMIES, THREE_ENEMIES, ONE_BOSS, ONE_BOSS_TWO_HENCHMAN}

[CreateAssetMenu(fileName = "New BattleSetup", menuName = "BattleSetup")]
public class BattleSetup : ScriptableObject
{
	public BattleConfig config;

	public GameObject[] enemies; // inimigos que aparecerão na batalha

	public Image background; // o background pode mudar de acordo com a batalha

	public int coinsReward; // recompensa depois de vencer a batalha
	public int xpReward; // recompensa depoinse de vencer a batalha

}
