﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardCopy
{
	public class CardCopied
	{
		public string name;
		public string description;

		public Sprite art;

		public GameObject slotIsOn;

		public int actualHealth;
		public int actualAttack;
		public int actualPoison;
		public int actualProtection;

		public int actionsVariation;

	}
}
