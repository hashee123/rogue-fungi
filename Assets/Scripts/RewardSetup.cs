﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New RewardSetup", menuName = "Pack")]
public class RewardSetup : ScriptableObject
{
	public GameObject[] cards;
}
