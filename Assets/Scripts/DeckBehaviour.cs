﻿/* Autor: Klinsmann Silva Hengles Cordeiro
   Agosto de 2020
   Programa com o comportamento do deck do jogador*/

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

public class DeckBehaviour : MonoBehaviour
{
	public static DeckBehaviour Instance { get; private set; }

	public GameObject[] initialDeckCards;
	public List<GameObject> deckList; // ta com prefabs
	public List<int> levelOfEachCard; // o nivel de cada carta para atualiza-las corretamente
	public List<GameObject> deckInstantiatedList; // ta com as cartas instanciadas
	public Queue<GameObject> graveyardCards;
	public Queue<GameObject> deckQueue;
	public Queue<GameObject> deckInstantiatedQueue;
	public List<GameObject> destroyAtEndOfBattle;
	/*[HideInInspector]*/ public GameObject[] onTable; // cards that are on the table

	public DeckVisualizer visualizer;
	public int levelArrayToDelete = 0;

	private void Awake()
	{
		// If there is an instance, and it's not me, delete myself.

		if (Instance != null && Instance != this)
		{
			Destroy(gameObject);
		}
		else
		{
			Instance = this;
		}

		DontDestroyOnLoad(gameObject);

		deckQueue = new Queue<GameObject>();
		deckInstantiatedQueue = new Queue<GameObject>();
		graveyardCards = new Queue<GameObject>();
		deckList = initialDeckCards.ToList();
		levelOfEachCard = new List<int>();
		for (int i = 0; i < deckList.Count; i++)
		{
			levelOfEachCard.Add(deckList[i].GetComponent<CardStatus>().level);
		}
		destroyAtEndOfBattle = new List<GameObject>();

		// visualizer.Setup();
		//onTable = new GameObject[3]; // OnTable guarda as 3 posicoes do tabuleiro, da esquerda para a direita: [0], [1], [2]
		//ShuffleCardsList(deckList, deckQueue);
		//ShuffleCardsArray(initialDeckCards, deckQueue);
	}

	private void Start()
	{
		// visualizer.Setup();
	}

	public void DestroyCardsToDestroyAtEnd()
	{
		foreach (GameObject obj in destroyAtEndOfBattle)
		{
			if (obj != null)
			{
				Destroy(obj);
			}
		}

		destroyAtEndOfBattle = new List<GameObject>();
	}

	// embaralha as cartas
	public void ShuffleCardsArray(GameObject[] cards, Queue<GameObject> deck)
	{
		int size = cards.Length; // size of the deck
		int randomNumber;

		for (int i = 0; i < size; i++)
		{
			// get a random number from ramaining positions
			randomNumber = i + Random.Range(0, (size - i));

			//switch positions
			GameObject temp = cards[randomNumber];
			cards[randomNumber] = cards[i];
			cards[i] = temp;
		}

		// put the elements on a Queue
		for (int i = 0; i < size; i++)
		{
			deck.Enqueue(cards[i]);
		}

	}

	// embaralha a lista de cartas
	public void ShuffleCardsList(List<GameObject> cards, Queue<GameObject> deck)
	{
		int size = cards.Count;
		int randomNumber;

		for (int i = 0; i < size; i++)
		{
			randomNumber = i + Random.Range(0, (size - i));

			GameObject temp = cards[randomNumber];
			cards[randomNumber] = cards[i];
			cards[i] = temp;
		}

		foreach (GameObject obj in cards)
		{
			deck.Enqueue(obj);
		}

	}

	public void CleanTable()
	{
		onTable[0] = null;
		onTable[1] = null;
		onTable[2] = null;
	}

	// pega as cartas no deck e embaralha no cemiterio
	public void ShuffleGraveyardToDeck()
	{
		/*deckCardsDuringBattle = graveyardCards.ToArray(); // coloca as cartas do cemiterio em um array temporario
		  ShuffleCardsArray(deckCardsDuringBattle, deckQueue); // embaralha e coloca na queue deck
		  graveyardCards = new Queue<GameObject>(); // o cemiterio vira uma nova queue ATENCAO MUDAR ISSO DEPOIS PORQUE A OUTRA QUEUE VIRA LIXO, TALVEZ EU SO LIMPE ELA QUE E' MELHOR*/

		GameObject[] deckCardsDuringBattle = graveyardCards.ToArray(); // coloca as cartas do cemiterio em um array temporario
		ShuffleCardsArray(deckCardsDuringBattle, deckInstantiatedQueue); // embaralha e coloca na queue deck
		graveyardCards = new Queue<GameObject>(); // o cemiterio vira uma nova queue ATENCAO MUDAR ISSO DEPOIS PORQUE A OUTRA QUEUE VIRA LIXO, TALVEZ EU SO LIMPE ELA QUE E' MELHOR

		/*int i = 0;
		  foreach (GameObject obj in deckCardsDuringBattle)
		  {
		  deckCardsDuringBattle[i] = null;
		  }*/
	}

	// reseta o estado do deck para como era antes da batalha
	public void ResetDeck()
	{
		deckInstantiatedQueue.Clear();
		graveyardCards.Clear();

		foreach (GameObject obj in deckInstantiatedList)
		{
			Destroy(obj);
		}

		for (int i = 0; i < levelArrayToDelete; i++)
		{
			levelOfEachCard.RemoveAt(levelOfEachCard.Count - 1);
		}
		levelArrayToDelete = 0;
		deckInstantiatedList.Clear();

		// PRECISO LIMPAR O ARRAY ONTABLE TAMBÉM
		//ShuffleCardsArray(initialDeckCards, deckInstantiatedQueue);
	}

	public void AddCardToDeckList(GameObject card)
	{
		deckList.Add(card);
		levelOfEachCard.Add(card.GetComponent<CardStatus>().level);
		if (visualizer != null)
		{
			visualizer.AddCard(card);
		}
	}

	public void AddTemporaryCardToDeckList(GameObject card)
	{
		deckInstantiatedList.Add(card);
		graveyardCards.Enqueue(card);
		levelOfEachCard.Add(card.GetComponent<CardStatus>().level);
		// card.SetActive(false);
		levelArrayToDelete++;
	}

	public IEnumerator moveCardTo(RectTransform cardTransform, Vector3 toPosition, float duration)
	{ // https://stackoverflow.com/questions/36850253/move-gameobject-over-time
		//Make sure there is only one instance of this function running
		if (GameManager.Instance.battleManager.applyingPlayerStatusEffects)
		{
			yield break; // exit if this is still running
		}
		GameManager.Instance.battleManager.applyingPlayerStatusEffects = true;

		float counter = 0;

		//Get the current position of the object to be moved
		Vector3 startPos = cardTransform.position;

		while (counter < duration)
		{
			counter += Time.deltaTime;
			cardTransform.position = Vector3.Lerp(startPos, toPosition, counter / duration);
			yield return null;
		}

		GameManager.Instance.battleManager.applyingPlayerStatusEffects = false;
	}

	public void SwitchCards(int card1Index, int card2Index) // Troca as cartas mas nao as posicoes
	{
		GameObject card1 = onTable[card1Index];
		GameObject card2 = onTable[card2Index];

		CardStatus card1Status = card1.GetComponent<CardStatus>();
		CardStatus card2Status = card2.GetComponent<CardStatus>();

		// altera os slotIsOn das duas cartas entre si
		int card1Slot = card1Status.slotIsOn;
		card1Status.slotIsOn = card2Status.slotIsOn;
		card2Status.slotIsOn = card1Slot;

		// altera os modificadores
		CardModifier card1Modifier = card1Status.actualModifier;
		card1Status.actualModifier = card2Status.actualModifier;
		card2Status.actualModifier = card1Modifier;

		// troca carta em onTable no deck
		onTable[card1Index] = card2;
		onTable[card2Index] = card1;
	}

	public IEnumerator SwitchUpCards()
	{
		// Sorteia duas trocas binarias: 1 com 2 e 2 com 3
		// Isso desloca as cartas para as posicoes: 1 -> 3, 2 -> 1, 3 -> 2.
		List<int> choices = new List<int>();
		choices.Add(0);
		choices.Add(1);
		choices.Add(2);
		int card1Index = Random.Range(0, 3);
		choices.RemoveAt(card1Index);
		int randomChoice = Random.Range(0, choices.Count);
		int card2Index = choices[randomChoice];
		choices.RemoveAt(randomChoice);
		int card3Index = choices[0];

		// Transforms das cartas
		RectTransform card1Transform = onTable[card1Index].GetComponent<RectTransform>();
		RectTransform card2Transform = onTable[card2Index].GetComponent<RectTransform>();
		RectTransform card3Transform = onTable[card3Index].GetComponent<RectTransform>();

		// Posicoes originais das cartas
		Vector3 card1Position = card1Transform.position;
		Vector3 card2Position = card2Transform.position;
		Vector3 card3Position = card3Transform.position;

		float durationOfMovement = 0.15f; // tempo que as cartas demoram se movendo

		BattleUIManager battleUIManager = GameObject.FindGameObjectWithTag("BattleUIManager").GetComponent<BattleUIManager>();

		// Move as cartas para o centro
		Instantiate(battleUIManager.battleUIParticles.clumsinessParticles,
					Vector3.zero,
					battleUIManager.battleUIParticles.particleSpawnPosition.rotation,
					battleUIManager.battleUI.transform);

		yield return StartCoroutine(moveCardTo(card1Transform, Vector3.zero, durationOfMovement));
		yield return StartCoroutine(moveCardTo(card2Transform, Vector3.zero, durationOfMovement));
		yield return StartCoroutine(moveCardTo(card3Transform, Vector3.zero, durationOfMovement));

		// Move as cartas para as posicoes baguncadas
		yield return StartCoroutine(moveCardTo(card1Transform, card3Position, durationOfMovement));
		yield return StartCoroutine(moveCardTo(card2Transform, card1Position, durationOfMovement));
		yield return StartCoroutine(moveCardTo(card3Transform, card2Position, durationOfMovement));

		// Troca de verdade as cartas
		SwitchCards(card1Index, card2Index);
		SwitchCards(card2Index, card3Index);
	}
}
