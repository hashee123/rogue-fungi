﻿/* Autor: Klinsmann Silva Hengles Cordeiro
   Agosto de 2020
   Contem o comportamento do Slot das cartas do jogador*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.Localization.Components;
using DG.Tweening;

public enum SlotPosition { LEFT, CENTER, RIGHT };

public class SlotBehaviour : MonoBehaviour, IDropHandler
{

	[Header("System Components")]
	public BattleUIManager battleUIManagerScript;

	[Header("Others")]
	public SlotPosition slot;
	public CardModifier actualModifier = CardModifier.ORIGINAL;
	public int powerSlot;
	public LocalizeStringEvent localizedString;
	public Image slotImage;

	public void OnDrop(PointerEventData pointerEventData)
	{
		if (!(GameManager.Instance.state == GameState.BATTLE_SCREEN
			  && GameManager.Instance.battleManager.state == BattleState.PLAYERTURN))
		{
			return;
		}

		GameObject cardBeingDropped = pointerEventData.pointerDrag.gameObject;

		if (cardBeingDropped.tag == "Card")
		{
			// Pega os status desta carta e da carta que esta sendo dropada
			CardStatus cardBeingDroppedStatus = cardBeingDropped.GetComponent<CardStatus>();
			CardStatus thisCardStatus = null;

			switch (slot)
			{
				case SlotPosition.LEFT:
					thisCardStatus = DeckBehaviour.Instance.onTable[0].GetComponent<CardStatus>();
					break;
				case SlotPosition.CENTER:
					thisCardStatus = DeckBehaviour.Instance.onTable[1].GetComponent<CardStatus>();
					break;
				case SlotPosition.RIGHT:
					thisCardStatus = DeckBehaviour.Instance.onTable[2].GetComponent<CardStatus>();
					break;
			}

			// Pega a posicao de ambas as cartas
			Vector3 cardBeingDroppedPosition = GameManager.Instance.battleManager.objectReferences.slots[cardBeingDroppedStatus.slotIsOn].transform.position;
			Vector3 thisCardPosition = GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].transform.position;

			// altera a posicao entre as duas cartas
			thisCardStatus.gameObject.transform.DOMove(cardBeingDroppedPosition, 0.2f, true);
			cardBeingDropped.transform.DOMove(thisCardPosition, 0.2f, true);
			// thisCardStatus.gameObject.GetComponent<Transform>().position = cardBeingDroppedPosition;
			// cardBeingDropped.GetComponent<Transform>().position = thisCardPosition;

			// altera os slotIsOn das duas cartas entre si
			int thisCardSlot = thisCardStatus.slotIsOn;
			thisCardStatus.slotIsOn = cardBeingDroppedStatus.slotIsOn;
			cardBeingDroppedStatus.slotIsOn = thisCardSlot;

			// altera os modificadores
			CardModifier thisCardModifier = thisCardStatus.actualModifier;
			thisCardStatus.actualModifier = cardBeingDroppedStatus.actualModifier;
			cardBeingDroppedStatus.actualModifier = thisCardModifier;

			// Altera a posicao das cartas no OnTable do DeckBehaviour
			int count = 0;
			int onTableThisPosition = 0;
			int onTableCardBeingDroppedPosition = 0;
			foreach (GameObject obj in DeckBehaviour.Instance.onTable)
			{
				if (obj == thisCardStatus.gameObject)
				{
					onTableThisPosition = count;
				}
				else if (obj == cardBeingDropped)
				{
					onTableCardBeingDroppedPosition = count;
				}
				count++;
			}
			DeckBehaviour.Instance.onTable[onTableThisPosition] = cardBeingDropped;
			DeckBehaviour.Instance.onTable[onTableCardBeingDroppedPosition] = thisCardStatus.gameObject;

			if (PlayerBehaviour.Instance.playerBehaviourData.actionPointsCurrent <= 1)
				cardBeingDropped.GetComponent<IDragger>().OnEndDrag(null);

			// Tirar um ponto de acao do Player
			GameManager.Instance.battleManager.TakePlayerActionPoints(1 - PlayerBehaviour.Instance.playerBuffs[CauldronBuffTypes.SLOT_CHANGE_COST][0]); // PRECISO ARRUMOR O BUG NO QUAL SE ESTA FOR A ULTIMA ACAO A CARTA BUGA
			battleUIManagerScript.ShowActionPoints();
		}
	}
}
