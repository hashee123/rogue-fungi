﻿/* Autor: Klinsmann Silva Hengles Cordeiro
   Agosto de 2020
   Programa que cuida da utilizacao das cartas do jogador durante a batalha
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using System.IO;
using DG.Tweening;

public class IDragger : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler, IBeginDragHandler, IDragHandler, IEndDragHandler, IDropHandler, IPointerDownHandler
{
	[Header("System Components")]
	public BattleUIManager battleUIManagerScript;
	public AudioManager audioManagerScript;
	public RewardManager rewardManagerScript;
	public RewardPosition rewardPosition;
	public UpgradeManager upgradeManagerScript;

	[Header("This Card")]
	public CardStatus thisCardStatus;
	public CardUnit thisCardUnit;
	public Image selectionAreaImage;
	public RectTransform rectTransform;
	public SpriteRenderer[] cardSpriteRenderers;
	public TextMeshPro[] cardTexts;
	public Animator cardAnimator;
	public GameObject effectExplainerPanel;
	public GameObject cardDescriptions;
	public Image cardDetectionImage;

	public int effectExplainerIndex;

	private bool beingDragged = false; // pra fazer a transicao entre BeginDrag e ExitEnter
	private bool highlighted;

	[Header("Essential")]
	public Canvas mainCanvas;
	public string languageOfTextFiles = "Portuguese";
	private Camera cam;

	//temporario, vou implementar na UI
	public Material shaderHighlight; // shader do slot

	public int velocidade;

	void Start()
	{
		audioManagerScript = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();

		if (GameManager.Instance.state == GameState.BATTLE_SCREEN)
			battleUIManagerScript = GameObject.FindGameObjectWithTag("BattleUIManager").GetComponent<BattleUIManager>();
		else if (GameManager.Instance.state == GameState.REWARD_SCREEN)
			rewardManagerScript = GameObject.FindGameObjectWithTag("RewardManager").GetComponent<RewardManager>();

		mainCanvas = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Canvas>();

		cam = Camera.main;
	}

	void Awake()
	{
		Font explainerFont = GameManager.GetFontFromBundle("LoveYaLikeASister");
		TMP_FontAsset explainerTMPFont = TMP_FontAsset.CreateFontAsset(explainerFont);

		foreach (TextMeshPro text in cardTexts)
		{
			Logging.Log("Corrigindo fonte em " + text.ToString() + " " + explainerFont.name);
			text.font = explainerTMPFont;
		}
		foreach (TextMeshPro text in cardDescriptions.GetComponentsInChildren<TextMeshPro>())
		{
			Logging.Log("Corrigindo fonte em " + text.ToString() + " " + explainerFont.name);
			text.font = explainerTMPFont;
		}
	}

	public void SetCardSpritesOnLayer(int layerID)
	{
		foreach (SpriteRenderer spriteRenderer in cardSpriteRenderers)
		{
			spriteRenderer.sortingLayerID = layerID;
		}

		foreach (TextMeshPro text in cardTexts)
		{
			text.sortingLayerID = layerID;
		}

		foreach (TextMeshPro desc in cardDescriptions.GetComponentsInChildren<TextMeshPro>())
		{
			desc.sortingLayerID = layerID;
		}
	}

	public void OnPointerEnter(PointerEventData pointerEventData)
	{
		if (GameManager.Instance.state != GameState.BATTLE_SCREEN ||
			GameManager.Instance.battleManager.state != BattleState.PLAYERTURN)
			return;
	}

	public void OnPointerDown(PointerEventData pointerEventData)
	{
		if (GameManager.Instance.state != GameState.BATTLE_SCREEN ||
			GameManager.Instance.battleManager.state != BattleState.PLAYERTURN)     
			return;

		int index = GameManager.Instance.battleManager.FindIndexOnTable(this.gameObject);
		if (index == -1)
		{
			Logging.LogError("O objeto enviado nao esta no OnTable");
			return;
		}

		GameManager.Instance.battleManager.DeactivateCardsExcept(index);
		GameManager.Instance.battleManager.DeactivateSlot(index);
		
		GameObject card = pointerEventData.pointerEnter.transform.parent.gameObject;
		battleUIManagerScript.ShowSlotDescription(card.GetComponent<CardStatus>().slotIsOn, true);

		cardAnimator.SetBool("Larger", true);
	}

	public void OnPointerExit(PointerEventData pointerEventData)
	{
		if (GameManager.Instance.state != GameState.BATTLE_SCREEN ||
		    GameManager.Instance.battleManager.state != BattleState.PLAYERTURN
			|| beingDragged)
			return;

		GameManager.Instance.battleManager.ActivateCards();
		GameManager.Instance.battleManager.ActivateSlots();
		cardAnimator.SetBool("Larger", false);
		cardAnimator.SetBool("Translucent", false);

		GameObject card = pointerEventData.pointerEnter.transform.parent.gameObject;
		battleUIManagerScript.ShowSlotDescription(card.GetComponent<CardStatus>().slotIsOn, false);

	}

	public void OnPointerClick(PointerEventData pointerEventData)
	{
		if(GameManager.Instance.state == GameState.UPGRADE_SCREEN)
				upgradeManagerScript.ShowBeforeAndAfterUpgrade(thisCardStatus);
	}

	public void SetEffectExplainer(bool toSet)
	{
		if (toSet)
		{
			battleUIManagerScript.cardsEffectsExplainers[effectExplainerIndex].allowUpdate = true;
			battleUIManagerScript.cardsEffectsExplainers[effectExplainerIndex].SetDescriptions(thisCardStatus);
		}
		else
		{
			battleUIManagerScript.cardsEffectsExplainers[effectExplainerIndex].allowUpdate = false;
			battleUIManagerScript.cardsEffectsExplainers[effectExplainerIndex].DisableDescriptors();
		}
	}

	public void OnBeginDrag(PointerEventData pointerEventData)
	{
		if (GameManager.Instance.state != GameState.BATTLE_SCREEN ||
			GameManager.Instance.battleManager.state != BattleState.PLAYERTURN)
			return;

		int index = GameManager.Instance.battleManager.FindIndexOnTable(this.gameObject);
		beingDragged = true;
		SetCardSpritesOnLayer(SortingLayer.NameToID("CardBeingDragged"));
		if (index == -1)
		{
			Logging.LogError("O objeto enviado nao esta no OnTable");
			return;
		}

		GameManager.Instance.battleManager.DeactivateSlot(index);
		GameManager.Instance.battleManager.DeactivateCardsExcept(index);

		SetEffectExplainer(true);
		if(!cardAnimator.GetBool(("Larger")))	cardAnimator.SetBool("Larger", true);
		if(!cardAnimator.GetBool(("Translucent")))	cardAnimator.SetBool("Translucent", true);

		rectTransform.SetAsLastSibling();
		selectionAreaImage.raycastTarget = false;
		audioManagerScript.Play("SelectCard");
	}

	public void OnDrag(PointerEventData pointerEventData)
	{
		if (GameManager.Instance.state != GameState.BATTLE_SCREEN ||
			GameManager.Instance.battleManager.state != BattleState.PLAYERTURN)
			return;


		Vector2 screenPos = battleUIManagerScript.battleCamera.ScreenToWorldPoint(pointerEventData.position);
		transform.position = screenPos;

		if (transform.position.y > GameManager.Instance.battleManager.cardActivationY)	
			cardAnimator.SetBool("Glowing", true);
		else if (transform.position.y <= GameManager.Instance.battleManager.cardActivationY)	
			cardAnimator.SetBool("Glowing", false);
	
	}

	public void OnEndDrag(PointerEventData pointerEventData)
	{
		if ((GameManager.Instance.state != GameState.BATTLE_SCREEN ||
			 GameManager.Instance.battleManager.state != BattleState.PLAYERTURN))
			return;
		
		if (transform.position.y > GameManager.Instance.battleManager.cardActivationY) 
			if(!cardAnimator.GetBool(("Glowing")))	cardAnimator.SetBool("Glowing", true);

		GameManager.Instance.battleManager.ActivateCards();
		GameManager.Instance.battleManager.ActivateSlots();
		SetCardSpritesOnLayer(SortingLayer.NameToID("Cards"));

		// Usar a carta se ela foi arrastada acima da linha determinada
		if (!GameManager.Instance.battleManager.cardSelectEvent && 
			 transform.position.y > GameManager.Instance.battleManager.cardActivationY)
		{
			if (thisCardStatus.actionPoints > PlayerBehaviour.Instance.playerBehaviourData.actionPointsCurrent)
			{
				battleUIManagerScript.warningUI.NotEnoughActionPoints();
				ResetCard();
			} 
			else
			{
				cardDetectionImage.raycastTarget = false;
				cardAnimator.SetTrigger("Fade");
			}

			cardAnimator.SetBool("Glowing", false);
		}
		else if(transform.position.y <= GameManager.Instance.battleManager.cardActivationY)
		{
			ResetCard();
		}

		SetEffectExplainer(false);
		beingDragged = false;
	}

	public void OnDisable()
	{
		if(GameManager.Instance.state == GameState.BATTLE_SCREEN)
		{
			GameManager.Instance.battleManager.ActivateCards();
			GameManager.Instance.battleManager.ActivateSlots();
		}
	}

	public void OnDrop(PointerEventData pointerEventData)
	{
		if ((GameManager.Instance.state != GameState.BATTLE_SCREEN ||
			 GameManager.Instance.battleManager.state != BattleState.PLAYERTURN))
			return;

		GameObject cardBeingDropped = pointerEventData.pointerDrag.gameObject;
		if (cardBeingDropped == this.gameObject) return;

		if (cardBeingDropped.tag == "Card")
		{
			// Pega os status desta carta e da carta que esta sendo dropada
			CardStatus cardBeingDroppedStatus = cardBeingDropped.GetComponent<CardStatus>();

			// Pega a posicao de ambas as cartas
			Vector3 cardBeingDroppedPosition = GameManager.Instance.battleManager.objectReferences.slots[cardBeingDroppedStatus.slotIsOn].transform.position;
			Vector3 thisCardPosition = GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].transform.position;

			// altera a posicao entre as duas cartas
			rectTransform.position = cardBeingDroppedPosition;
			cardBeingDropped.GetComponent<RectTransform>().position = thisCardPosition;

			// altera os slotIsOn das duas cartas entre si
			int thisCardSlot = thisCardStatus.slotIsOn;
			thisCardStatus.slotIsOn = cardBeingDroppedStatus.slotIsOn;
			cardBeingDroppedStatus.slotIsOn = thisCardSlot;

			// altera os modificadores
			CardModifier thisCardModifier = thisCardStatus.actualModifier;
			thisCardStatus.actualModifier = cardBeingDroppedStatus.actualModifier;
			cardBeingDroppedStatus.actualModifier = thisCardModifier;

			// Altera a posicao das cartas no OnTable do DeckBehaviour
			int onTableThisPosition = GameManager.Instance.battleManager.FindIndexOnTable(this.gameObject);
            int onTableCardBeingDroppedPosition = GameManager.Instance.battleManager.FindIndexOnTable(cardBeingDropped);

			DeckBehaviour.Instance.onTable[onTableThisPosition] = cardBeingDropped;
			DeckBehaviour.Instance.onTable[onTableCardBeingDroppedPosition] = this.gameObject;

			// Altera as descrições dos efeitos das cartas
			Vector3 swapPosition = battleUIManagerScript.cardsEffectsExplainers[onTableThisPosition].transform.position;
			battleUIManagerScript.cardsEffectsExplainers[onTableThisPosition].transform.position = battleUIManagerScript.cardsEffectsExplainers[onTableCardBeingDroppedPosition].transform.position;
			battleUIManagerScript.cardsEffectsExplainers[onTableCardBeingDroppedPosition].transform.position = swapPosition;
			cardBeingDropped.GetComponent<IDragger>().effectExplainerIndex = effectExplainerIndex;

			GameManager.Instance.battleManager.TakePlayerActionPoints(1);
			battleUIManagerScript.ShowActionPoints();
		}
	}

	public void UseCard()
	{
		// Se nao é o turno do player, nao fazer nada
		if (GameManager.Instance.battleManager.state != BattleState.PLAYERTURN) return;

		ResetCard(false); // Reseta a carta

		int currentProtection = PlayerBehaviour.Instance.playerBehaviourData.defenseCurrent;

		// Usar e Desativar a carta usada
		thisCardStatus.cardEffects[0].ChooseEffect(thisCardStatus);
		int index = GameManager.Instance.battleManager.FindIndexOnTable(gameObject);

		// Atualiza os pontos de vida do inimigo na tela
		battleUIManagerScript.ShowEnemyLifePoints();
		battleUIManagerScript.ShowEnemyPoison();


		if (currentProtection != PlayerBehaviour.Instance.playerBehaviourData.defenseCurrent)
		{
			// Atualiza o escudo do jogador na tela
			battleUIManagerScript.ShowPlayerProtection();
		}

		KindOfCard kindOfCard = gameObject.GetComponent<CardsEffects>().kind;
		CardsEffects cardEffectScript = gameObject.GetComponent<CardsEffects>();
		bool destroyOnUse = cardEffectScript.destroyOnUse;
		bool sendToGraveyardOnUse = cardEffectScript.sendToGraveyardOnUse;

		GameManager.Instance.battleManager.DeactivateCardAt(index);

		// Cartas de modificadores nao voltam ao cemiterio, pois so podem ser usadas uma vez por partida
		if (destroyOnUse)	Destroy(gameObject); // OBS: Se der problema destruir só no final

		if (!destroyOnUse && sendToGraveyardOnUse)
		{
			DeckBehaviour.Instance.graveyardCards.Enqueue(DeckBehaviour.Instance.onTable[index]);
			battleUIManagerScript.ShowGraveyardNumberCards();
		}

		foreach (CardsEffects eff in gameObject.GetComponent<CardStatus>().cardEffects)	
			eff.OnDiscardEffect();

		SendCardOffScreen();

		GameObject cardCreated = GameManager.Instance.battleManager.AddCardToSlot(GameManager.Instance.battleManager.objectReferences.slots[index]);
		cardAnimator.Rebind();

		cardDetectionImage.raycastTarget = true;


		// Gerencia pontos de ação do jogador
		int actionPoints = thisCardStatus.actionPoints;
		GameManager.Instance.battleManager.TakePlayerActionPoints(actionPoints); 
		battleUIManagerScript.ShowActionPoints(); 
	}

	void CreateCardIconAnimation()
	{
		CardsEffects cardEffectScript = gameObject.GetComponent<CardsEffects>();
		bool destroyOnUse = cardEffectScript.destroyOnUse;
		bool sendToGraveyardOnUse = cardEffectScript.sendToGraveyardOnUse;

		GameObject cardIcon = Instantiate(battleUIManagerScript.cardIcon,
										  transform.position,
										  transform.rotation,
										  battleUIManagerScript.battleUI.transform);


		CardIcon cardIconScript = cardIcon.GetComponent<CardIcon>();
		cardIconScript.SetBattleUIManager(battleUIManagerScript);
		if (destroyOnUse) cardIconScript.CardIconDestroy();

		if (!destroyOnUse && sendToGraveyardOnUse)
			cardIconScript.MoveTowardsGraveyard();
		else if (!destroyOnUse && !sendToGraveyardOnUse)
		{
			int index = GameManager.Instance.battleManager.FindIndexOnTable(gameObject);
			SlotBehaviour slot = GameManager.Instance.battleManager.objectReferences.slots[index].GetComponent<SlotBehaviour>();
			cardIconScript.MoveTowardsSlot(slot);
		}
	}

	public void SendCardOffScreen()
	{
		gameObject.transform.position = new Vector3(-100, +100, -100);
	}

	public void ResetCard(bool doTweening = true)
	{
		// Resetar as outras cartas e slots
		GameManager.Instance.battleManager.ActivateCards();
		GameManager.Instance.battleManager.ActivateSlots();
		
		// Reseta a posicao
		if (doTweening)
			gameObject.transform.DOMove(GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].transform.position, 0.2f, true);
		
		battleUIManagerScript.ShowSlotDescription(thisCardStatus.slotIsOn, false);
		
		// Animação de voltar
		if(cardAnimator.GetBool(("Translucent")))	cardAnimator.SetBool("Translucent", false);
		if(cardAnimator.GetBool(("Larger")))	cardAnimator.SetBool("Larger", false);
		cardAnimator.Update(0f);
	}
}

