﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CardIcon : MonoBehaviour
{
	private BattleUIManager battleUIManager;
	[SerializeField] private SpriteRenderer spriteRenderer;
	[SerializeField] private SpriteRenderer glowSpriteRenderer;

	public void SetBattleUIManager(BattleUIManager battleUIManager)
	{
		this.battleUIManager = battleUIManager;
	}

	void DOTweenMove(Vector3 targetPosition, float time, Ease easeType, TweenCallback afterTweenFunc)
	{
		Vector3[] waypoints = {
			transform.position + new Vector3(90.0f, 0.0f, 0.0f),
			targetPosition + new Vector3(0.0f, 60.0f, 0.0f),
			targetPosition
		};

		if (waypoints[0].x > targetPosition.x) waypoints[0].x = targetPosition.x;

		transform.DOPath(waypoints, time, PathType.CatmullRom).SetEase(easeType).OnComplete(afterTweenFunc);
		transform.DOScale(new Vector3(0.0f, 0.0f, 0.0f), time).SetEase(easeType);
		transform.DORotate(new Vector3(0.0f, 0.0f, -800.0f), time, RotateMode.FastBeyond360).SetEase(easeType);
	}

	public void MoveTowardsGraveyard()
	{
		Vector3 targetPosition = battleUIManager.textGraveyardNumberOfCards.transform.position;
		DOTweenMove(targetPosition, 0.5f, Ease.InCubic, AfterTweenGraveyard);
		glowSpriteRenderer.DOFade(1.0f, 0.5f).SetEase(Ease.InCubic);
	}

	public void MoveTowardsSlot(SlotBehaviour slot)
	{
		// Vector3 targetPosition = slot.transform.position;
		// DOTweenMove(targetPosition, 0.3f, Ease.InCirc, AfterTweenSlot);

		// glowSpriteRenderer.DOFade(1.0f, 0.01f).SetEase(Ease.InCubic);
		// if (slot.actualModifier == CardModifier.POISON)
		// 	glowSpriteRenderer.DOColor(Color.green, 0.1f);
		// else if (slot.actualModifier == CardModifier.PROTECTION)
		// 	glowSpriteRenderer.DOColor(Color.blue, 0.1f);
		// else if (slot.actualModifier == CardModifier.PIERCING)
		// 	glowSpriteRenderer.DOColor(Color.magenta, 0.1f);
		Destroy(gameObject);
	}

	public void CardIconDestroy()
	{
		Destroy(gameObject);
	}

	void AfterTweenGraveyard()
	{
		battleUIManager.graveyardIconButtonJuice.Juicy(1.4f);
		battleUIManager.ShowGraveyardNumberCards();
		Destroy(gameObject);
	}

	void AfterTweenSlot()
	{

	}
}
