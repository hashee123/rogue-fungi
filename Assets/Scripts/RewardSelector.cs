﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

public class RewardSelector : MonoBehaviour, IDropHandler
{
	[Header("Setup")]
	public RewardManager rewardManager;
	public Canvas rewardCanvas;
	public GameObject[] buttons;
	public Image bookBackground;
	public RectTransform rewardPosition;
	public RectTransform cardInstantiatedPosition;
	public RectTransform cardInstantiatedScale;
	public Text descriptionText;
	public GameObject magicCircle;
	public Image bookDetectionArea;
	public AnimationClip openingAnimation;
	public Animator bookAnimator;
	public GameObject creationParticles;
	public GameObject chooseYourRewardTextUI;

	[Header("Current Reward")]
	public GameObject selectedReward;
	private GameObject cardInstantiated;

	private Vector2 endPosition = new Vector2(0.0f, 150.0f);
	private Vector3 endScale = new Vector3(1.8f, 1.8f, 1.0f);
	private bool gotReward = false;

	public void SetupBattleReward()
	{
		Logging.Log("SETUP");

		foreach (RewardObject rewardObject in rewardManager.battleRewardObjects)	rewardObject.gameObject.SetActive(true);

		descriptionText.enabled = false;
		bookBackground.raycastTarget = true;
	}

	public void SetupChestReward()
	{
		rewardManager.chestRewardObject.gameObject.SetActive(false); // desliga o foguinho
		rewardManager.chestRewardSelector.gameObject.SetActive(false); // desliga o livro

		descriptionText.enabled = false;
		bookBackground.raycastTarget = true;
	}

	public void SetRewardOnBook(PointerEventData pointerEventData)
	{
		if (pointerEventData == null)	return;	

		if (pointerEventData.lastPress != null)
		{
			// Ja tem uma recompensa no livro e nao é a que acabamos de soltar em cima
			if (selectedReward != null && pointerEventData.lastPress.gameObject != selectedReward)	SnapBack(selectedReward);
			
			Logging.Log("Pointer nao nulo");
			selectedReward = pointerEventData.lastPress.gameObject;
			selectedReward.transform.DOMove(rewardPosition.position, 0.2f, true);
			SetSelectionElements(true);
		}
		else if (pointerEventData.pointerDrag != null)
		{
			if (selectedReward != null && pointerEventData.pointerDrag.gameObject != selectedReward)	SnapBack(selectedReward);

			Logging.Log("Pointer nao nulo");
			selectedReward = pointerEventData.pointerDrag.gameObject;
			selectedReward.transform.DOMove(rewardPosition.position, 0.2f, true);
			SetSelectionElements(true);
		}
	}

	public void OnDrop(PointerEventData pointerEventData)
	{
		if (gotReward) return;

		Logging.Log("OnDrop");
		SetRewardOnBook(pointerEventData);
	}

	public void SetSelectionElements(bool option)
	{
		foreach (GameObject button in buttons)	button.SetActive(option);

		descriptionText.enabled = option;
		if (selectedReward != null)
		{
			descriptionText.text = selectedReward.GetComponent<RewardObject>().cardDescription;
			selectedReward.GetComponent<RewardObject>().isSelected = option;
		}

		bookBackground.raycastTarget = (!option);
	}

	// snaps back to original position
	public void SnapBack(GameObject objectToSnapBack)
	{
		if (objectToSnapBack != null)	
			objectToSnapBack.transform.DOMove(objectToSnapBack.GetComponent<RewardObject>().originalPosition, 0.2f, true);
	}

	// funcao do botao de Voltar
	public void ReturnSelectedReward()
	{
		SnapBack(selectedReward);
		SetSelectionElements(false);
		selectedReward = null;
	}

	// funcao do botao de Aceitar
	public void GetSelectedReward()
	{
		rewardManager.GetReward(selectedReward.GetComponent<RewardObject>().cardPrefab);
		SnapBack(selectedReward);
		foreach (RewardObject rewardObject in rewardManager.battleRewardObjects)	rewardObject.gameObject.SetActive(false);	

		rewardManager.chestRewardObject.gameObject.SetActive(false);

		cardInstantiated = Instantiate(selectedReward.GetComponent<RewardObject>().cardPrefab, rewardPosition.position,
									   rewardPosition.rotation, cardInstantiatedPosition);

		cardInstantiated.GetComponent<Animator>().enabled = false;
		cardInstantiated.GetComponent<IDragger>().enabled = false;
		cardInstantiated.GetComponent<RectTransform>().localScale = new Vector3(0.11f, 0.11f, 0);
		if(cardInstantiated.GetComponent<RectTransform>() != null)
			cardInstantiatedPosition.DOAnchorPos(endPosition, 1.2f, true).SetEase(Ease.OutCubic).onComplete = CardRewardedFloatingUP;
		cardInstantiatedScale.DOScale(endScale, 1.2f).SetEase(Ease.OutCubic);
		creationParticles.SetActive(true);
		gotReward = true;

		// Clear the UI
        bookAnimator.SetBool("BookTransition", true);
		magicCircle.SetActive(false);
		descriptionText.gameObject.SetActive(false);
		chooseYourRewardTextUI.SetActive(false);
    }

	// Faz a carta flutuar para cima na tela depois de ter sido escolhida
	public void CardRewardedFloatingUP()
	{
		if(cardInstantiated != null && cardInstantiated.GetComponent<RectTransform>() != null)
			cardInstantiatedPosition.DOAnchorPosY(endPosition.y + 20f, 3f).SetEase(Ease.InOutSine).onComplete = CardRewardedFloatingDOWN;
	}

	// Faz a carta flutuar para baixo
    public void CardRewardedFloatingDOWN()
    {
		if(cardInstantiated != null && cardInstantiated.GetComponent<RectTransform>() != null)
        	cardInstantiatedPosition.DOAnchorPosY(endPosition.y, 3f).SetEase(Ease.InOutSine).onComplete = CardRewardedFloatingUP;
    }

    public void OnDisable()
	{
		bookDetectionArea.enabled = false;
	}

	public void CleanBook()
	{
		if (cardInstantiated != null)	Destroy(cardInstantiated);
		foreach (GameObject button in buttons)	button.SetActive(false);

		magicCircle.SetActive(false);
		creationParticles.SetActive(false);
	}

	public void OnEnable()
	{
		StartCoroutine(ShowMagicCircle());
	}
	private IEnumerator ShowMagicCircle()
	{
		yield return new WaitForSeconds(openingAnimation.length);
		magicCircle.SetActive(true);
		bookDetectionArea.enabled = true;
	}
}
