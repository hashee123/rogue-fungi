﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct MapPath
{
	[SerializeField] public string destination;
	[SerializeField] public Vector2[] intermediatePoints;
	}

[System.Serializable]
public class Place
{
	public string name;

	public bool startEnabled;

	public GameObject placePrefab;

	public RewardSetup rewardSetup; // lista de possiveis recompensas do local

	public int diff = 0;

	public int battleSetupIndex;

	public BattleSetup thisBattleSetup;

	public Vector3 position;

	public MapPath[] paths;

	// public string[] toEnable;

	public int enemyMaxStage;

	public float percentToHeal;

	public int chestCoins;

	// public bool isEnabled = false;
}
