using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New MapSetupPool", menuName = "MapSetupPool")]
public class MapSetupPool : ScriptableObject
{
	public MapSetup[] maps;

	// Para guardar os indices dos mapas que foram randomizados
	// Cada mapa possui uma dificuldade, que seleciona o array de qual ele origina
	// O indice se refere ao array especifico do diff
	public int[] indexes;
}