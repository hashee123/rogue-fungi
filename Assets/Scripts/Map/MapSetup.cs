﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New MapSetup", menuName = "MapSetup")]
public class MapSetup : ScriptableObject
{
	public Vector2 startingPoint;

	public MapPath initialPath;

	public Place[] placeUnits;
}
