﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum KindOfPlace { BATTLE, SHOP, REWARD, UPGRADE, REST };
public class PlaceBehaviour : MonoBehaviour//, IPointerClickHandler
{
	public Image visitedMarkSprite;
	public KindOfPlace place; // tipo de lugar que o icone vai levar
	public float percentToHeal;
	public Button thisButton;
	public Color afterVisitedColor;

	public BattleSetup battleSetup; // vai ser usado se for uma batalha

	public int indexOnPlaceUnits; // index of this GameObject in the Map.Setup place units variable

	//[HideInInspector] public Image transition;
	[HideInInspector] public GameObject mapManager;
	[HideInInspector] public MapBehaviour mapBehaviourScript;
	[HideInInspector] public RewardManager rewardManagerScript;

	private void Awake()
	{
		mapManager = GameObject.FindGameObjectWithTag("MapManager");
		mapBehaviourScript = mapManager.GetComponent<MapBehaviour>();
	}

	public void MarkThisPlaceAsVisited()
	{
		MapSceneTransition.mapSceneTransitionData.visitedPlaces[indexOnPlaceUnits] = true;
	}

	public void EnableVisitedSprite()
	{
		var newColor = thisButton.colors;
		newColor.disabledColor = afterVisitedColor;
		thisButton.colors = newColor;

		visitedMarkSprite.enabled = true;
	}

	int BattlePlaceAfterTransitionFunction()
	{
		mapBehaviourScript.actualPlace = indexOnPlaceUnits;

		if(gameObject.name == "Place_Battle_Boss(Clone)") GameManager.Instance.boss = true;
		else GameManager.Instance.boss = false;

		GameManager.Instance.state = GameState.BATTLE_SCREEN;
		GameManager.Instance.currentBattleSetup = battleSetup;
		GameManager.Instance.battleRewardCards = mapBehaviourScript.actualMap.placeUnits[mapBehaviourScript.actualPlace].rewardSetup.cards;
		GameManager.Instance.enemyMax = mapBehaviourScript.actualMap.placeUnits[mapBehaviourScript.actualPlace].enemyMaxStage;
		GameManager.Instance.actualPlace = mapBehaviourScript.actualPlace;
		GameManager.Instance.DoNewBattle();

		SceneManager.LoadScene("BattleScene");

		return 0;
	}

	int ShopPlaceAfterTranstionFunction()
	{
		mapBehaviourScript.actualPlace = indexOnPlaceUnits;

		mapBehaviourScript.mapUI.gameObject.SetActive(false);
		mapBehaviourScript.shopUI.gameObject.SetActive(true);
		mapBehaviourScript.shopManager.RandomizeShop();

		GameManager.Instance.state = GameState.SHOP_SCREEN;

		return 0;
	}

	int ChestPlaceAfterTranstionFunction()
	{
		MapSceneTransition.mapSceneTransitionData.actualPlace = indexOnPlaceUnits;    // Agora que o mapa esta sendo instanciado e destruido
		mapBehaviourScript.actualPlace = indexOnPlaceUnits; // cada vez que sai e entra do mapa ele pega da variável do
		GameManager.Instance.actualPlace = mapBehaviourScript.actualPlace;

		GameManager.Instance.state = GameState.CHEST_SCREEN;
		GameManager.Instance.battleRewardCards = mapBehaviourScript.actualMap.placeUnits[mapBehaviourScript.actualPlace].rewardSetup.cards; // Para ter o prefab

		RewardData.rewardType = 0;

		SceneManager.LoadScene("RewardScene");

		return 0;
	}

	int UpgradePlaceAfterTranstionFunction()
	{
		mapBehaviourScript.actualPlace = indexOnPlaceUnits;
		GameManager.Instance.actualPlace = mapBehaviourScript.actualPlace;

		mapBehaviourScript.mapCursorInstance.transform.localPosition = mapBehaviourScript.actualMap.placeUnits[mapBehaviourScript.actualPlace].position + new Vector3(0, 26, 0);
		GameManager.Instance.state = GameState.UPGRADE_SCREEN;

		SceneManager.LoadScene("UpgradeScene");

		return 0;
	}

	int RestPlaceAfterTranstionFunction()
	{
		mapBehaviourScript.actualPlace = indexOnPlaceUnits;
		GameManager.Instance.actualPlace = mapBehaviourScript.actualPlace;

		GameManager.Instance.state = GameState.REST_SCREEN;
		mapBehaviourScript.mapCursorInstance.transform.localPosition = mapBehaviourScript.actualMap.placeUnits[mapBehaviourScript.actualPlace].position + new Vector3(0, 26, 0);

		GameManager.Instance.SaveGame();

		SceneManager.LoadScene("RestScene");

		return 0;
	}

	// funcao chamada ao clicar no botao do mapa que te leva a uma batalha
	public void EnterBattlePlace()
	{
		StartCoroutine(GameManager.Instance.waitForTransition(GameManager.Instance.transitionObjectAnimator, BattlePlaceAfterTransitionFunction));
	}

	// funcao chamada ao clicar no botao que te leva a uma loja
	public void EnterShopPlace()
	{
		StartCoroutine(GameManager.Instance.waitForTransition(GameManager.Instance.transitionObjectAnimator, ShopPlaceAfterTranstionFunction));
	}

	// funcao chamada ao clicar no botao que te leva a um bau secreto
	public void EnterChestPlace()
	{
		StartCoroutine(GameManager.Instance.waitForTransition(GameManager.Instance.transitionObjectAnimator, ChestPlaceAfterTranstionFunction));
	}

	public void EnterUpgradePlace()
	{
		StartCoroutine(GameManager.Instance.waitForTransition(GameManager.Instance.transitionObjectAnimator, UpgradePlaceAfterTranstionFunction));
	}

	public void EnterRestPlace()
	{
		StartCoroutine(GameManager.Instance.waitForTransition(GameManager.Instance.transitionObjectAnimator, RestPlaceAfterTranstionFunction));
	}

}
