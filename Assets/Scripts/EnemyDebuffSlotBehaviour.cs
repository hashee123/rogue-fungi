﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyDebuffSlotBehaviour : MonoBehaviour
{

	public BattleManager battleManager;
	public BattleUIManager battleUIManager;

	public Image thisImage;
	public Text thisText;

	public void HideSlot()
	{
		this.gameObject.SetActive(false);
	}

	public void ShowSlot()
	{
		this.gameObject.SetActive(true);
	}

	public void ChangeToPoisonDebuff()
	{
		thisImage.sprite = battleUIManager.poisonDebuff; // DEPOIS TENHO QUE MUDAR PRA IMAGEM DO DEBUFF DE POISON
		thisText.text = battleManager.enemyBehaviourScript.thisEnemyUnit.enemyUnitData.actualPoison.ToString();
	}
}
