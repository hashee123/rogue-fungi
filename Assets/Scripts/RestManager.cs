﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RestManager : MonoBehaviour
{
	public HealthBar playerHealthBar;
	public Text healthBarText;
	public float percentToHeal;
	public MapBehaviour mapBehaviourScript;
	public Button thisButton;

	public void OnEnable()
	{
		thisButton.interactable = true;
		SetHealthBar();
		percentToHeal = 0.3f;
	}

	public void Rest()
	{
		PlayerBehaviour.Instance.playerBehaviourData.lifeCurrent += (int) 
			((percentToHeal + (PlayerBehaviour.Instance.playerBuffs[CauldronBuffTypes.REST_RESTORE_RATE][0]/100)) 
				* PlayerBehaviour.Instance.playerBehaviourData.lifeInitial);
		if (PlayerBehaviour.Instance.playerBehaviourData.lifeCurrent > PlayerBehaviour.Instance.playerBehaviourData.lifeInitial)
			PlayerBehaviour.Instance.playerBehaviourData.lifeCurrent = PlayerBehaviour.Instance.playerBehaviourData.lifeInitial;
		SetHealthBar();
	}

	void SetHealthBar()
	{
		playerHealthBar.SetMaxHealthValue(PlayerBehaviour.Instance.playerBehaviourData.lifeInitial);
		playerHealthBar.SetHealth(PlayerBehaviour.Instance.playerBehaviourData.lifeCurrent);
		healthBarText.text = PlayerBehaviour.Instance.playerBehaviourData.lifeCurrent.ToString() + "/" + PlayerBehaviour.Instance.playerBehaviourData.lifeInitial.ToString();
	}
	public void BackToMap() {
		GameUIManager.Instance.FromRestScreenToMapScreen();
	}
}
