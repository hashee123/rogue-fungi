﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RewardType { CHEST, BATTLE, BOSS }
public class RewardPosition : MonoBehaviour
{
	public RewardType type; // tipo dessa recompensa
	public GameObject selectionPanel; // painel de selecao
	public bool isSelected = false; // essa recompensa esta' selecionada?
	public int index; // indice na lista de recompensas de batalha
	public IDragger iDraggerScript;

	public void OnEnable()
	{
		iDraggerScript.enabled = true;
	}
}
