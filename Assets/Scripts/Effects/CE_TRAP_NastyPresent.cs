﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CE_TRAP_NastyPresent : CardsEffects
{
	public int autoDamage;

	public override void ApplyEffect()
	{
		player.GetComponent<PlayerBehaviour>().TakeDamage(autoDamage);
	}

	// Cartas de dano modificadas pelo modificador de Poison causam 1 de poison
	public override void ApplyPoisonModifierEffect()
	{
		ApplyEffect();
	}

	public override void ApplyProtectionEffect()
	{
		PlayerBehaviour.Instance.playerBehaviourData.defenseCurrent += 1;
	}

	public override void ApplyPiercingEffect()
	{
		player.GetComponent<PlayerBehaviour>().TakePiercingDamage(autoDamage);
	}

	public override void ApplyStrengthEffect()
	{
		player.GetComponent<PlayerBehaviour>().TakeDamage(autoDamage + 2);
	}

	public override string CardDescription()
	{
		return string.Format(effectText, autoDamage);
	}
}
