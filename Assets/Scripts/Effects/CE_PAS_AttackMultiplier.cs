﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CE_PAS_AttackMultiplier : CardsEffects
{
	public int[] damageMultiplierPerLevel; // bonus de dano enquanto carta esta' na mesa
	public int actualMultiplier;
	public int index = 2;

	public void Start()
	{
		actualMultiplier = damageMultiplierPerLevel[thisCardStatus.level];
		index = thisCardStatus.level;
		RefreshCardStrings();
	}

	public override void ApplyEffect()
	{
		GameManager.Instance.battleManager.damageMultiplierNextAttack = damageMultiplierPerLevel[thisCardStatus.level];
		int damageMultiplier = damageMultiplierPerLevel[thisCardStatus.level];
		GameManager.Instance.battleManager.damageMultiplierNextAttack = damageMultiplier;
		battleUIManagerScript.ShowPlayerDamageMultiplier();
		/*battleUIManagerScript.damageMultiplierUI.SetActive(true);
		  battleUIManagerScript.damageMultiplierText.text = "x" + damageMultiplier;*/
	}

	public override void Upgrade(int level, bool highlight)
	{
		index = level;
		actualMultiplier = damageMultiplierPerLevel[level];
		// string color;
		// if (highlight)
		// {
		//     color = GameUIManager.Instance.colorGreen;
		// }
		// else
		// {
		//     color = GameUIManager.Instance.cardDescriptionColor;
		// }
		// thisCardStatus.ChangeWord("AttackMultiplier", "dobro", "triplo", color); // completamente hard coded :v eu sei
		RefreshCardStrings();
	}

}
