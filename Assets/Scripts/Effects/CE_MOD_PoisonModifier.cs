﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Localization.SmartFormat.PersistentVariables;

public class CE_MOD_PoisonModifier : CardsEffects
{
	public int[] poisonPerLevel;
	public int actualPoison;

	void Start()
	{
		actualPoison = poisonPerLevel[thisCardStatus.level];
		RefreshCardStrings();
	}

	public override void ApplyEffect()
	{
		GameObject slot = GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn];
		SlotBehaviour thisSlotBehaviour = slot.GetComponent<SlotBehaviour>();

		thisSlotBehaviour.actualModifier = CardModifier.POISON;
		thisSlotBehaviour.slotImage.color = battleUIManagerScript.color_PoisonSlot;
		//slot.GetComponent<Image>().color = battleUIManagerScript.color_PoisonSlot;
		thisSlotBehaviour.powerSlot = actualPoison;
		thisSlotBehaviour.localizedString.StringReference.SetReference("SlotDescriptions", "Poison");
		thisSlotBehaviour.localizedString.StringReference.Add("v", new ObjectVariable {Value = thisSlotBehaviour});
	}

	public override void Upgrade(int level, bool highlight)
	{
		actualPoison = poisonPerLevel[level];
		// string color;
		// if (highlight)
		// {
		//     color = GameUIManager.Instance.colorGreen;
		// }
		// else
		// {
		//     color = GameUIManager.Instance.cardDescriptionColor;
		// }
		// thisCardStatus.ChangeNumber("PoisonMod", poisonPerLevel[level], color);
		RefreshCardStrings();
	}

	public override string CardDescription()
	{
		return string.Format(effectText, poisonPerLevel[thisCardStatus.level]);
	}
}
