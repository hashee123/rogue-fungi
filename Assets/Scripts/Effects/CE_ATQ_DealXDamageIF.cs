using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CE_ATQ_DealXDamageIF : CardsEffects
{
	public int[] damagePerLevel;
	public int[] otherDamagePerLevel;

	public int actualDamage;
	public int otherDamage;

	public void Start()
	{
		actualDamage = damagePerLevel[thisCardStatus.level];
		otherDamage = otherDamagePerLevel[thisCardStatus.level];
		RefreshCardStrings();
	}

	// Aplica o efeito original da carta
	public override void ApplyEffect()
	{
		int thisCardIndex = GameManager.Instance.battleManager.FindIndexOnTable(gameObject);
		List<int> choices = new List<int>();
		choices.Add(0);
		choices.Add(1);
		choices.Add(2);
		choices.Remove(thisCardIndex);
		KindOfCard type1 = DeckBehaviour.Instance.onTable[choices[0]].GetComponent<CardStatus>().kind;
		KindOfCard type2 = DeckBehaviour.Instance.onTable[choices[1]].GetComponent<CardStatus>().kind;
		if(type1 == KindOfCard.ATTACK && type2 == KindOfCard.ATTACK)
		{
			GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(otherDamage);
		}
		else
		{
			GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(actualDamage);
		}

	}

	public override void Upgrade(int level, bool highlight)
	{
		actualDamage = damagePerLevel[level];
		otherDamage = otherDamagePerLevel[level];
		// string color;
		// if (highlight)
		// {
		// 	color = GameUIManager.Instance.colorGreen;
		// }
		// else
		// {
		// 	color = GameUIManager.Instance.cardDescriptionColor;
		// }
		// thisCardStatus.ChangeNumber("Unite", otherDamage, color);
		RefreshCardStrings();
	}

	// Cartas de dano modificadas pelo modificador de Poison causam poison igual a metade do dano original da carta
	public override void ApplyPoisonModifierEffect()
	{
		ApplyEffect();
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakePoison(GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot);
	}

	public override void ApplyProtectionEffect()
	{
		ApplyEffect();
		player.GetComponent<PlayerBehaviour>().playerBehaviourData.defenseCurrent +=  GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot;
	}

	public override void ApplyPiercingEffect()
	{
		int thisCardIndex = GameManager.Instance.battleManager.FindIndexOnTable(gameObject);
		List<int> choices = new List<int>();
		choices.Add(0);
		choices.Add(1);
		choices.Add(2);
		choices.Remove(thisCardIndex);
		KindOfCard type1 = DeckBehaviour.Instance.onTable[choices[0]].GetComponent<CardStatus>().kind;
		KindOfCard type2 = DeckBehaviour.Instance.onTable[choices[1]].GetComponent<CardStatus>().kind;
		if(type1 == KindOfCard.ATTACK && type2 == KindOfCard.ATTACK)
		{
			GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakePiercingDamage(otherDamage);
		}
		else
		{
			GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakePiercingDamage(actualDamage);
		}

		actualDamage = damagePerLevel[thisCardStatus.level]; // para corrigir o bug da batata
		otherDamage = otherDamagePerLevel[thisCardStatus.level]; // Mesma lógica da linha anterior, não sei se é necessário fazer isso para o dano extra;
	}

	public override void ApplyStrengthEffect()
	{
		int thisCardIndex = GameManager.Instance.battleManager.FindIndexOnTable(gameObject);
		List<int> choices = new List<int>();
		choices.Add(0);
		choices.Add(1);
		choices.Add(2);
		choices.Remove(thisCardIndex);
		KindOfCard type1 = DeckBehaviour.Instance.onTable[choices[0]].GetComponent<CardStatus>().kind;
		KindOfCard type2 = DeckBehaviour.Instance.onTable[choices[1]].GetComponent<CardStatus>().kind;
		if(type1 == KindOfCard.ATTACK && type2 == KindOfCard.ATTACK)
		{
			GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(otherDamage + GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot);
		}
		else
		{
			GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(actualDamage + GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot);
		}

		actualDamage = damagePerLevel[thisCardStatus.level]; // para corrigir o bug da batata
		otherDamage = otherDamagePerLevel[thisCardStatus.level]; // Mesma lógica da linha anterior, não sei se é necessário fazer isso para o dano extra;
	}

	public override string CardDescription()
	{
		return string.Format(effectText, actualDamage);
	}
}
