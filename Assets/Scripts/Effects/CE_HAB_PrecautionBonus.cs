﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CE_HAB_PrecautionBonus : CardsEffects
{
	public int precautionsTaken; // quantos efeitos sao evitados
	public override void ApplyEffect()
	{
		GameManager.Instance.battleManager.precautionsTaken = precautionsTaken;
	}

	public override void Upgrade(int level, bool highlight)
	{

	}
}
