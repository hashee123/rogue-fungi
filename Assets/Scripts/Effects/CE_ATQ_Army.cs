﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CE_ATQ_Army : CardsEffects
{
	public CardUnit cardUnit; // scriptable para pegar o prefab desta carta
	public int[] troopDamagePerLevel; // dano de uma tropa
	public int actualTroopDamage;
	public int actualTroopCount;

	void Start()
	{
		actualTroopDamage = troopDamagePerLevel[thisCardStatus.level];
		actualTroopCount = countTroops();
		RefreshCardStrings();
	}

	int countTroops()
	{
		int count = 0;
		foreach (GameObject card in DeckBehaviour.Instance.deckInstantiatedList)
		{
			if (card.GetComponent<CE_ATQ_Army>() != null)
			{
				count++;
			}
		}

		return count;
	}

	private void CreateNewTroop()
	{
		GameObject newTroop = Instantiate(cardUnit.cardPrefab, battleUIManagerScript.battleUI.transform, false);
		DeckBehaviour.Instance.AddTemporaryCardToDeckList(newTroop);
		newTroop.SetActive(false);
		//DeckBehaviour.Instance.deckInstantiatedQueue.Enqueue(newTroop);
		// newTroop.SetActive(false);
		battleUIManagerScript.ShowDeckNumberCards();
		battleUIManagerScript.ShowGraveyardNumberCards();
		//Logging.Log("New troop: " + numberOfTroops);
	}

	public override void ApplyEffect()
	{
		actualTroopCount = countTroops();
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(actualTroopCount * troopDamagePerLevel[thisCardStatus.level]);
		CreateNewTroop();
		actualTroopCount += 1;
		RefreshCardStrings();
	}

	public override void Upgrade(int level, bool highlight)
	{
		actualTroopDamage = troopDamagePerLevel[level];
		// string color;
		// if (highlight)
		// {
		//     color = GameUIManager.Instance.colorGreen;
		// }
		// else
		// {
		//     color = GameUIManager.Instance.cardDescriptionColor;
		// }
		// thisCardStatus.ChangeNumber("ArmyDamage", troopDamagePerLevel[level], color);
		RefreshCardStrings();
	}

	// Cartas de dano modificadas pelo modificador de Poison causam poison igual a metade do dano original da carta
	public override void ApplyPoisonModifierEffect()
	{
		ApplyEffect();
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakePoison(GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot);
	}

	public override void ApplyProtectionEffect()
	{
		ApplyEffect();
		player.GetComponent<PlayerBehaviour>().playerBehaviourData.defenseCurrent += GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot;
	}

	public override void ApplyPiercingEffect()
	{
		actualTroopCount= countTroops();
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakePiercingDamage(actualTroopCount * troopDamagePerLevel[thisCardStatus.level]);
		CreateNewTroop();
		actualTroopCount += 1;
		RefreshCardStrings();
	}

	public override void ApplyStrengthEffect()
	{
		actualTroopCount = countTroops();
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(actualTroopCount * troopDamagePerLevel[thisCardStatus.level] + GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot);
		CreateNewTroop();
		actualTroopCount += 1;
		RefreshCardStrings();
	}

	public override string CardDescription()
	{
		int count = countTroops();
		return string.Format(effectText, count * troopDamagePerLevel[thisCardStatus.level]);
	}
}
