﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CE_ATQ_DamageSacrifice : CardsEffects
{
	public int[] damagePerLevel;
	public int autoDamage;
	public int actualDamage;

	public void Start()
	{
		actualDamage = damagePerLevel[thisCardStatus.level];
		RefreshCardStrings();
	}

	// Aplica o efeito original da carta
	public override void ApplyEffect()
	{
		PlayerBehaviour.Instance.TakeDamage(autoDamage);
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(damagePerLevel[thisCardStatus.level]);
	}

	public override void Upgrade(int level, bool highlight)
	{
		actualDamage = damagePerLevel[level];
		// string color;
		// if (highlight)
		// {
		//     color = GameUIManager.Instance.colorGreen;
		// }
		// else
		// {
		//     color = GameUIManager.Instance.cardCostColor;
		// }
		RefreshCardStrings();
		// thisCardStatus.ChangeNumber("Damage", actualDamage, color);
	}

	// Cartas de dano modificadas pelo modificador de Poison causam 1 de poison
	public override void ApplyPoisonModifierEffect()
	{
		ApplyEffect();
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakePoison(GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot);
	}

	public override void ApplyProtectionEffect()
	{
		ApplyEffect();
		PlayerBehaviour.Instance.playerBehaviourData.defenseCurrent += GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot;
	}

	public override void ApplyPiercingEffect()
	{
		PlayerBehaviour.Instance.TakeDamage(autoDamage);
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakePiercingDamage(damagePerLevel[thisCardStatus.level]);
	}

	public override void ApplyStrengthEffect()
	{
		PlayerBehaviour.Instance.TakeDamage(autoDamage);
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(damagePerLevel[thisCardStatus.level] + GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot);
	}

	public override string CardDescription()
	{
		return string.Format(effectText, damagePerLevel);
	}
}
