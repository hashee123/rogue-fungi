﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CE_PAS_Anger : CardsEffects
{
	public int[] addDamagePerLevel; // dano acrescido a cada turno
	public CE_ATQ_DealDamage ce_atq_dealDamage;
	public int actualAddDamage;

	public void Start()
	{
		hasPassiveEffect = true;
		actualAddDamage = addDamagePerLevel[thisCardStatus.level];
		RefreshCardStrings();
	}

	public override void ApplyPassiveEffect()
	{
		ce_atq_dealDamage.actualDamage += actualAddDamage;
		ce_atq_dealDamage.dhos[0] = true;
		// Muda o texto da descricao da carta para mostrar o dano novo
		// thisCardStatus.ChangeNumber("Damage", ce_atq_dealDamage.actualDamage, GameUIManager.Instance.colorGreen);
		RefreshCardStrings();
	}
	public override void Upgrade(int level, bool highlight)
	{
		actualAddDamage = addDamagePerLevel[level];
		// string color;
		// if (highlight)
		// {
		//     color = GameUIManager.Instance.colorGreen;
		// }
		// else
		// {
		//     color = null;
		// }
		// thisCardStatus.ChangePlusNumber("Strengthen", actualAddDamage, color);
		RefreshCardStrings();
	}


	public void OnDisable()
	{
		ce_atq_dealDamage.dhos[0] = false;
		// thisCardStatus.ResetDescription(); // volta a descricao com o dano original
	}
}
