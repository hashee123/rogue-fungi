using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CE_HAB_ScarletMirror : CardsEffects
{
	public GameObject slotDrop;
	public GameObject instantiatedSlotDrop;

	// esses aqui so sao chamados para modificar o actualDamage
	/*public int damageLvl1;
	  public int damageLvl2;
	  public int damageLvl3;*/

	// Aplica o efeito original da carta

	public void Start()
	{
		RefreshCardStrings();
	}

	public override void ApplyEffect()
	{
		GameManager.Instance.battleManager.cardSelectEvent = true;
		GameManager.Instance.battleManager.allowCardRefill = false;
		battleUIManagerScript.ActivateEffectLayout("Escolha uma carta para clonar");

		instantiatedSlotDrop = Instantiate(slotDrop,
										   GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].transform.position,
										   Quaternion.identity,
										   battleUIManagerScript.battleUI.transform);
		instantiatedSlotDrop.transform.localScale = new Vector3(0f, 0f, 0f);
		GenericSlotDrop genericSlotDropScript = instantiatedSlotDrop.GetComponent<GenericSlotDrop>();
		genericSlotDropScript.battleUIManagerScript = battleUIManagerScript;
		genericSlotDropScript.originalCardStatus = gameObject.GetComponent<CardStatus>();
		genericSlotDropScript.onTableCount = GameManager.Instance.battleManager.FindIndexOnTable(gameObject);
	}

	// public override void Upgrade(int level)
	// {
	//     actualDamage = damagePerLevel[level];
	//     thisCardStatus.ChangeColoredText(actualDamage.ToString(), GameManager.Instance.cardDescriptionColor);
	// }

	// Cartas de dano modificadas pelo modificador de Poison causam poison igual a metade do dano original da carta
	public override void ApplyPoisonModifierEffect()
	{
		ApplyEffect();
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakePoison(1);
	}

	public override void ApplyProtectionEffect()
	{
		ApplyEffect();
		player.GetComponent<PlayerBehaviour>().playerBehaviourData.defenseCurrent += 1;
	}

	public override void ApplyPiercingEffect()
	{
		ApplyEffect();
	}

	public override void ApplyStrengthEffect()
	{
		ApplyEffect();
	}

	public override string CardDescription()
	{
		return string.Format(effectText, 0);
	}
}
