﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CE_MOD_PiercingModifier : CardsEffects
{
	public string descriptionToSlot;
	public int[] costPerLevel;
	public int actualCost;

	public void Start()
	{
		RefreshCardStrings();
	}

	public override void ApplyEffect()
	{
		GameObject slot = GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn];
		SlotBehaviour thisSlotBehaviour = slot.GetComponent<SlotBehaviour>();

		thisSlotBehaviour.actualModifier = CardModifier.PIERCING;
		thisSlotBehaviour.slotImage.color = battleUIManagerScript.color_PiercingSlot;
		//slot.GetComponent<Image>().color = battleUIManagerScript.color_PiercingSlot;
		thisSlotBehaviour.localizedString.StringReference.SetReference("SlotDescriptions", "Piercing");
	}

	public override void Upgrade(int level, bool highlight)
	{
		actualCost = costPerLevel[level];
		thisCardStatus.actionPoints = actualCost;
		string color;
		if (highlight)
		{
			color = GameUIManager.Instance.colorGreen;
		}
		else
		{
			color = GameUIManager.Instance.cardDescriptionColor;
		}
		thisCardStatus.ChangeCost(actualCost, color);
	}
}
