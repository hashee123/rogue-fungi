﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CE_ATQ_ForbiddenSacrifice : CardsEffects
{
	public int[] damagePerLevel;
	public int actualDamage;

	public void Start()
	{
		actualDamage = damagePerLevel[thisCardStatus.level];
		RefreshCardStrings();
	}

	private void DestroyACard()
	{
		List<int> choices = new List<int>();
		choices.Add(0);
		choices.Add(1);
		choices.Add(2);
		choices.RemoveAt(thisCardStatus.slotIsOn); // Nao destruir a carta que acabou de substituir a que foi jogada
		GameManager.Instance.battleManager.DestroyCardAt(choices[Random.Range(0, 2)]);
	}

	// Aplica o efeito original da carta
	public override void ApplyEffect()
	{
		DestroyACard();
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(actualDamage);
	}

	public override void Upgrade(int level, bool highlight)
	{
		actualDamage = damagePerLevel[level];
		string color;
		if (highlight)
		{
			color = GameUIManager.Instance.colorGreen;
		}
		else
		{
			color = GameUIManager.Instance.cardDescriptionColor;
		}
		// thisCardStatus.ChangeNumber("Damage", actualDamage, color);
		RefreshCardStrings();
	}

	// Cartas de dano modificadas pelo modificador de Poison causam poison igual a metade do dano original da carta
	public override void ApplyPoisonModifierEffect()
	{
		ApplyEffect();
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakePoison(GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot);
	}

	public override void ApplyProtectionEffect()
	{
		ApplyEffect();
		player.GetComponent<PlayerBehaviour>().playerBehaviourData.defenseCurrent += GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot;
	}

	public override void ApplyPiercingEffect()
	{
		DestroyACard();
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakePiercingDamage(damagePerLevel[thisCardStatus.level]);
	}

	public override void ApplyStrengthEffect()
	{
		DestroyACard();
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(damagePerLevel[thisCardStatus.level] + GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot);
	}

	public override string CardDescription()
	{
		return string.Format(effectText, damagePerLevel[thisCardStatus.level]);
	}
}
