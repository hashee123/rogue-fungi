﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Localization.SmartFormat.PersistentVariables;

public class CE_MOD_ProtectionModifier : CardsEffects
{
	public int[] protectionPerLevel;
	public int actualProtection;

	void Start()
	{
		actualProtection = protectionPerLevel[thisCardStatus.level];
		RefreshCardStrings();
	}

	public override void ApplyEffect()
	{
		GameObject slot = GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn];
		SlotBehaviour thisSlotBehaviour = slot.GetComponent<SlotBehaviour>();

		thisSlotBehaviour.actualModifier = CardModifier.PROTECTION;
		thisSlotBehaviour.slotImage.color = battleUIManagerScript.color_ProtectionSlot;
		//slot.GetComponent<Image>().color = battleUIManagerScript.color_ProtectionSlot;
		thisSlotBehaviour.powerSlot = actualProtection;
		thisSlotBehaviour.localizedString.StringReference.SetReference("SlotDescriptions", "Protection");
		thisSlotBehaviour.localizedString.StringReference.Add("v", new ObjectVariable {Value = thisSlotBehaviour});
	}

	public override void Upgrade(int level, bool highlight)
	{
		actualProtection = protectionPerLevel[level];
		// string color;
		// if (highlight)
		// {
		//     color = GameUIManager.Instance.colorGreen;
		// }
		// else
		// {
		//     color = GameUIManager.Instance.cardDescriptionColor;
		// }
		// thisCardStatus.ChangeNumber("ProtectionMod", protectionPerLevel[level], color);
		RefreshCardStrings();
	}

	public override string CardDescription()
	{
		return string.Format(effectText, protectionPerLevel[thisCardStatus.level]);
	}
}
