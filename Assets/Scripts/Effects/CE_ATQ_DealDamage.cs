﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CE_ATQ_DealDamage : CardsEffects
{
	public int[] damagePerLevel;
	public int actualDamage;

	public bool upgrades = true;

	// Aplica o efeito original da carta
	public override void ApplyEffect()
	{
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(actualDamage);
		actualDamage = damagePerLevel[thisCardStatus.level]; // para corrigir o bug da batata
		RefreshCardStrings();
	}

	public override void Upgrade(int level, bool highlight)
	{
		if (upgrades)
		{
			actualDamage = damagePerLevel[level];
			// string color;
			// if (highlight)
			// {
			//     color = GameUIManager.Instance.colorGreen;
			// }
			// else
			// {
			// 			color = GameUIManager.Instance.cardDescriptionColor;
			// }
			// thisCardStatus.ChangeNumber("Damage", actualDamage, color);
		}
		RefreshCardStrings();
	}

	// Cartas de dano modificadas pelo modificador de Poison causam poison igual a metade do dano original da carta
	public override void ApplyPoisonModifierEffect()
	{
		ApplyEffect();
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakePoison(GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot);
	}

	public override void ApplyProtectionEffect()
	{
		ApplyEffect();
		player.GetComponent<PlayerBehaviour>().playerBehaviourData.defenseCurrent +=  GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot;
	}

	public override void ApplyPiercingEffect()
	{
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakePiercingDamage(actualDamage);
		actualDamage = damagePerLevel[thisCardStatus.level]; // para corrigir o bug da batata
	}

	public override void ApplyStrengthEffect()
	{
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(actualDamage + GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot);
		actualDamage = damagePerLevel[thisCardStatus.level]; // para corrigir o bug da batata
	}

	public override string CardDescription()
	{
		return string.Format(effectText, actualDamage);
	}
}
