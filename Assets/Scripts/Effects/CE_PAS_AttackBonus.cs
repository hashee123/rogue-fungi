﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CE_PAS_AttackBonus : CardsEffects
{
	public bool global; // esse bonus fica ativo enquanto a carta esta na mesa?
	public int[] bonusDamagePerLevel; // bonus de dano enquanto carta esta' na mesa
	public int actualBonus;
	private int originalBonus; // dano de bonus original para ser recuperado


	public void Start()
	{
		actualBonus = bonusDamagePerLevel[thisCardStatus.level];
		RefreshCardStrings();
	}

	public override void OnDrawEffect()
	{
		if (GameManager.Instance.state == GameState.BATTLE_SCREEN)
		{
			actualBonus = bonusDamagePerLevel[thisCardStatus.level];
			originalBonus = GameManager.Instance.battleManager.bonusDamage;
			if (global)
			{
				GameManager.Instance.battleManager.bonusDamage += actualBonus;
				battleUIManagerScript.ShowPlayerBonusDamage();
				/*battleUIManagerScript.bonusDamageUI.SetActive(true);
				  battleUIManagerScript.bonusDamageText.text = "+" + GameManager.Instance.battleManager.bonusDamage;*/
			}
		}
	}

	public override void ApplyEffect()
	{
		if (!global)
		{
			actualBonus = bonusDamagePerLevel[thisCardStatus.level];
			GameManager.Instance.battleManager.bonusDamageThisTurn += actualBonus;
			GameManager.Instance.battleManager.bonusDamage += actualBonus;
			battleUIManagerScript.ShowPlayerBonusDamage();
			/*battleUIManagerScript.bonusDamageUI.SetActive(true);
			  battleUIManagerScript.bonusDamageText.text = "+" + GameManager.Instance.battleManager.bonusDamage;*/
		}
	}

	public override void Upgrade(int level, bool highlight)
	{
		actualBonus = bonusDamagePerLevel[level];
		// string color;
		// if (highlight)
		// {
		//     color = GameUIManager.Instance.colorGreen;
		// }
		// else
		// {
		//     color = GameUIManager.Instance.cardDescriptionColor;
		// }

		// if (global) // Eco do Poder
		// {
		//     thisCardStatus.ChangePlusNumber("AttackBonus", actualBonus, color);
		// }
		// else // Sopa de Legumes
		// {
		//     thisCardStatus.ChangeNumber("Strength", actualBonus, color);
		// }
		RefreshCardStrings();
	}

	public override void OnDiscardEffect()
	{
		Logging.Log("Discard: chamei o discard do power echo");
		if (GameManager.Instance.state == GameState.BATTLE_SCREEN)
		{
			if (global)
			{
				GameManager.Instance.battleManager.bonusDamage -= actualBonus;
				if (GameManager.Instance.battleManager.bonusDamage < 0)
				{
					GameManager.Instance.battleManager.bonusDamage = 0;
				}

				battleUIManagerScript.ShowPlayerBonusDamage();
			}
		}
	}
}
