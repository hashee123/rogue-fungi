﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Localization.SmartFormat.PersistentVariables;

public class CE_MOD_StrengthModifier : CardsEffects
{
	public int[] strengthPerLevel;
	public int actualStrength;

	void Start()
	{
		actualStrength = strengthPerLevel[thisCardStatus.level];
		RefreshCardStrings();
	}

	public override void ApplyEffect()
	{
		GameObject slot = GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn];
		SlotBehaviour thisSlotBehaviour = slot.GetComponent<SlotBehaviour>();

		thisSlotBehaviour.actualModifier = CardModifier.FISICAL_DMG;
		thisSlotBehaviour.slotImage.color = battleUIManagerScript.color_StrengthSlot;
		//slot.GetComponent<Image>().color = battleUIManagerScript.color_StrengthSlot;
		thisSlotBehaviour.powerSlot = actualStrength;
		thisSlotBehaviour.localizedString.StringReference.SetReference("SlotDescriptions", "Strength");
		thisSlotBehaviour.localizedString.StringReference.Add("v", new ObjectVariable {Value = thisSlotBehaviour});
	}

	public override void Upgrade(int level, bool highlight)
	{
		actualStrength = strengthPerLevel[level];
		// string color;
		// if (highlight)
		// {
		//     color = GameUIManager.Instance.colorGreen;
		// }
		// else
		// {
		//     color = GameUIManager.Instance.cardDescriptionColor;
		// }
		// thisCardStatus.ChangeNumber("StrengthMod", strengthPerLevel[level], color);
		RefreshCardStrings();
	}

	public override string CardDescription()
	{
		return string.Format(effectText, strengthPerLevel[thisCardStatus.level]);
	}
}
