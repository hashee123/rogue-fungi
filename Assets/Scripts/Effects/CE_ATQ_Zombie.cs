﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Components;

public class CE_ATQ_Zombie : CardsEffects
{
	public int[] damageBoostPerLevel;
	public int initialDamage;
	public int actualDamage;
	public int otherDamage;

	public void Start()
	{
		actualDamage = initialDamage;
		otherDamage = damageBoostPerLevel[thisCardStatus.level];
		RefreshCardStrings();
	}

	public override void ApplyEffect()
	{
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(actualDamage);

		actualDamage += damageBoostPerLevel[thisCardStatus.level];
		dhos[0] = true;
		// thisCardStatus.ChangeNumber("Damage", actualDamage, GameUIManager.Instance.colorGreen);
		RefreshCardStrings();
	}

	public override void Upgrade(int level, bool highlight)
	{
		// string color;
		// if (highlight)
		// {
		//     color = GameUIManager.Instance.colorGreen;
		// }
		// else
		// {
		//     color = null;
		// }
		// thisCardStatus.ChangePlusNumber("Enthuse", damageBoostPerLevel[level], color);
		otherDamage = damageBoostPerLevel[level];
		RefreshCardStrings();
	}

	public override void ApplyPoisonModifierEffect()
	{
		ApplyEffect();
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakePoison(GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot);
	}

	public override void ApplyProtectionEffect()
	{
		ApplyEffect();
		player.GetComponent<PlayerBehaviour>().playerBehaviourData.defenseCurrent += (GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot);
	}

	public override void ApplyPiercingEffect()
	{
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakePiercingDamage(actualDamage);

		actualDamage += damageBoostPerLevel[thisCardStatus.level];
		dhos[0] = true;
		RefreshCardStrings();
		// thisCardStatus.ChangeNumber("Damage", actualDamage, GameUIManager.Instance.colorGreen);
	}

	public override void ApplyStrengthEffect()
	{
		GameManager.Instance.battleManager.enemyCard[0].GetComponent<EnemyBehaviour>().TakeDamage(actualDamage + GameManager.Instance.battleManager.objectReferences.slots[thisCardStatus.slotIsOn].GetComponent<SlotBehaviour>().powerSlot); // Esse dois e' do slot, deve ser uma variavel

		actualDamage += damageBoostPerLevel[thisCardStatus.level];
		dhos[0] = true;
		RefreshCardStrings();
		// thisCardStatus.ChangeNumber("Damage", actualDamage, GameUIManager.Instance.colorGreen);
	}

	public override string CardDescription()
	{
		return string.Format(effectText, actualDamage);
	}
}
