using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FMODUnity;

public class DefeatVisualEvents : MonoBehaviour
{
	public float animationDuration;
	public ParticleSystem defeatParticles;
	public BattleUIManager battleUIManager;
	public StudioEventEmitter eventEmitter;

	private float currentTime = 0.0f;
	private bool countTime = false;

	void OnEnable()
	{
		defeatParticles.gameObject.SetActive(true);
		defeatParticles.Pause();
	}

	public void StartAnimation()
	{
		defeatParticles.gameObject.SetActive(true);
		defeatParticles.Play(true);
		countTime = true;
		eventEmitter.Play();
	}

    void Update()
    {
		if (!countTime) return;

		if (currentTime > animationDuration)
		{
			defeatParticles.Pause();
			battleUIManager.RestartRunButton();
			countTime = false;
			currentTime = 0.0f;
		}

		currentTime += Time.deltaTime;
    }
}
