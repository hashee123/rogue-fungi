﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateObjectsOnEnable : MonoBehaviour
{
	[Header("To enable and disable same objects")]
	public GameObject[] objectsToEnable;
	public bool alsoDisableSameObjects;

	[Header("If bool is false, these will be disabled")]
	public GameObject[] objectsToDisable;

	public void OnEnable()
	{
		for (int i = 0; i < objectsToEnable.Length; i++)
		{
			objectsToEnable[i].SetActive(true);
		}
	}

	public void OnDisable()
	{
		if (alsoDisableSameObjects)
		{
			for (int i = 0; i < objectsToEnable.Length; i++)
			{
				if (objectsToEnable[i] != null)
				{
					objectsToEnable[i].SetActive(false);
				}
			}
		}
		else
		{
			for (int i = 0; i < objectsToDisable.Length; i++)
			{
				objectsToDisable[i].SetActive(false);
			}
		}
	}
}
