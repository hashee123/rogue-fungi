using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GenericSlotDrop : MonoBehaviour, IDropHandler
{
	public BattleUIManager battleUIManagerScript;
	public CardStatus originalCardStatus;
	public GameObject droppedCard;
	public GameObject instantiatedCard;
	public int onTableCount;
	public Animator animator;

	public void DestroyThisObject()
	{
		GameObject.Destroy(gameObject);
	}

	public void OnDrop(PointerEventData pointerEventData)
	{
		droppedCard = pointerEventData.pointerDrag.gameObject;
		Animator cardAnimator = droppedCard.GetComponent<Animator>();
		cardAnimator.Rebind();
		cardAnimator.Update(0f);

		instantiatedCard = Instantiate(droppedCard, gameObject.transform.position, Quaternion.identity, battleUIManagerScript.battleUI.transform);
		DeckBehaviour.Instance.destroyAtEndOfBattle.Add(instantiatedCard);
		CardStatus instantiatedCardStatus = instantiatedCard.GetComponent<CardStatus>();
		instantiatedCardStatus.slotIsOn = originalCardStatus.slotIsOn;
		instantiatedCardStatus.actualModifier = originalCardStatus.actualModifier;
		DeckBehaviour.Instance.onTable[onTableCount] = instantiatedCard;

		GameManager.Instance.battleManager.cardSelectEvent = false;
		battleUIManagerScript.DeactivateEffectLayout();
		GameManager.Instance.battleManager.allowCardRefill = true;
		if (PlayerBehaviour.Instance.playerBehaviourData.actionPointsCurrent == 0)
		{
			GameManager.Instance.battleManager.EndPlayerTurn();
		}
		animator.SetTrigger("Disable");
	}
}
