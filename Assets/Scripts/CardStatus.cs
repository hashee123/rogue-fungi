﻿/* Autor: Klinsmann Silva Hengles Cordeiro
   Agosto de 2020
   Contem os dados da carta quando sao copiadas do ScriptableObject para serem usadas no jogo*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class CardStatus : MonoBehaviour
{
	[Header("Setup")]
	public CardUnit card; // scriptable object que contem o prefab desta carta
	public KindOfCard kind; // tipo da carta
	public int actionPoints = 1; // quantos action points essa carta consome
	public int level; // Raridade da carta: 0, 1 ou 2 - equivalente a 1, 2 ou 3 estrelas
	public List<CardsEffects> cardEffects = new List<CardsEffects>(); // todos os efeitos que essa carta contem
	public Sprite cardCenterSprite;
	public GameObject cardDescriptions;
	public string fileContent;
	public RectTransform cardTransform;

	[Header("References")]
	public Animator animator; // deve ser referenciado no inspector
	public SpriteRenderer stars; // imagem da raridade da carta, muda conforme o nivel. Deve ser referenciado no inspector
	public Transform cardDescriptionTransform; // O transform com os textos de descricao
	public TextMeshPro costText; // texto do custo da carta
	public RuntimeAnimatorController defaultAnimation;
	public IDragger iDraggerScript;
	public BattleUIManager battleUIManagerScript;
	private List<string> originalCardDescription; // descricao original, para ser recuperada apos usar a carta

	[Header("Current information")]
	public int slotIsOn; // 0 = left, 1 = center, 2 = right
	public int indexOnDeck; // indice no deckList
	public CardModifier actualModifier = CardModifier.ORIGINAL; // modificador ativo no momento

	public bool[] hasUpgradeOnString; // Set on Editor


	public void Start()
	{
		GameObject battleManager = GameObject.FindGameObjectWithTag("BattleManager");
		if (battleManager != null)
		{
			battleUIManagerScript = battleManager.GetComponent<BattleUIManager>();
		}
		cardTransform = gameObject.GetComponent<RectTransform>();

		// fileContent = GameManager.Instance.cardEffectDescriptions.text;
		// effectAndExplanation = new Dictionary<string, string>();
		// string[] lines = fileContent.Split('\n');
		// foreach (string line in lines)
		// {
		//     if (line != "")
		//     {
		// 	string[] linePair = line.Split(',');
		// 	effectAndExplanation.Add(linePair[0], linePair[1]);
		//     }
		// }

		// battleUIManagerScript.cardsEffectsExplainers[slotIsOn].SetDescriptions(this);
	}

	public void Setup() // Configura todos os efeitos dessa carta
	{
		foreach (CardsEffects effect in cardEffects)
		{
			effect.Setup();
		}
	}

	public void Awake()
	{
		originalCardDescription = new List<string>();
		for (int i = 0; i < cardDescriptionTransform.childCount; i++)
		{
			TextMeshPro effectText = cardDescriptionTransform.GetChild(i).GetComponent<TextMeshPro>();
			originalCardDescription.Add(effectText.text);
		}
	}

	// public string GetExplanation(LocalizeStringEvent localizedString, string effectName)
	// {
	// 	string effectExplanation;
	//     // effectAndExplanation.TryGetValue(effectName, out effectExplanation);
	//     return effectExplanation;
	// }

	// public bool NeedsExplanation(string effectName)
	// {
	// 	// effectsExplainerStringTable.GetEntry(effectName)
	//     return effectAndExplanation.ContainsKey(effectName);
	// }

	// public void ChangeNumber(string effect, int newNumber, string newColor)
	// {
	//     TextMeshPro effectText = cardDescriptionTransform.Find(effect).GetComponent<TextMeshPro>();
	//     System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("<color=#[0-9A-F]{6}>([0-9]+)</color>");
	//     string replace = "<color=" + newColor + ">" + newNumber + "</color>";
	//     effectText.text = regex.Replace(effectText.text, replace);
	// }

	// public void ChangePlusNumber(string effect, int newNumber, string newColor)
	// {
	//     TextMeshPro effectText = cardDescriptionTransform.Find(effect).GetComponent<TextMeshPro>();
	//     System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("<color=#[0-9A-F]{6}>\\+([0-9]+)</color>");
	//     string replace = "<color=" + newColor + ">+" + newNumber + "</color>";
	//     effectText.text = regex.Replace(effectText.text, replace);
	// }

	// public void ChangeWord(string effect, string oldWord, string newWord, string newColor)
	// {
	//     TextMeshPro effectText = cardDescriptionTransform.Find(effect).GetComponent<TextMeshPro>();
	//     System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(oldWord);
	//     if (newColor != null)
	//     {
	//         string replace = "<color=" + newColor + ">+" + newWord + "</color>";
	//         effectText.text = regex.Replace(effectText.text, replace);
	//     }
	//     else
	//     {
	//         effectText.text = regex.Replace(effectText.text, newWord);
	//     }
	// }

	public void ChangeCost(int newNumber, string newColor) // muda o texto de custo da carta
	{
		System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("<color=#[0-9A-F]{6}>([0-9]+)</color>");
		string replace = "<color=" + newColor + ">" + newNumber + "</color>";
		costText.text = regex.Replace(costText.text, replace);
	}

	// public void ResetDescription()
	// {
	//     for (int i = 0; i < cardDescriptionTransform.childCount; i++)
	//     {
	//         TextMeshPro effectText = cardDescriptionTransform.GetChild(i).GetComponent<TextMeshPro>();
	//         effectText.text = originalCardDescription[i];
	//     }
	// }

	public void DisableAndResetCard()
	{
		transform.position = Vector3.zero;
		transform.rotation = Quaternion.identity;
		transform.localScale = new Vector3(0.17f, 0.17f, 0f);
		animator.runtimeAnimatorController = defaultAnimation as RuntimeAnimatorController;
		gameObject.SetActive(false);
	}

}
