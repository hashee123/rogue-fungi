﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class PlayerBehaviourData
{
	public int lifeInitial;
	public int actionPointsInitial;
	public int defenseInitial;
	public int coinsRunStart;
	public int coins;
	public int xpRunStart;
	public int xp;
	public int lifeCurrent;
	public int actionPointsCurrent;
	public int defenseCurrent;
	public int poisonCurrent;
	public int tirednessCurrent;
	public int tirednessTurns;
	public int inflationCurrent;
	public int curseCurrent;
	public int clumsinessCurrent;
	public int destructionCurrent;
	public int[] slotBlockCurrent = new int[3];
}

public class PlayerBehaviour : MonoBehaviour
{
	public static PlayerBehaviour Instance { get; private set; }

	public PlayerBehaviourData playerBehaviourData;

	public Relic relicHolding;
	public Image relicImage;

	public BattleUIManager battleUIManagerScript;

	public PlayerHealthBar healthBar;
	public Animator playerHealthBarAnimator;

	public Dictionary<CauldronBuffTypes, int[]> playerBuffs = new Dictionary<CauldronBuffTypes, int[]>()
	{
		   {CauldronBuffTypes.AFTER_BATTLE_RECOVERY, new int[]{0,0}}, {CauldronBuffTypes.REST_RESTORE_RATE, new int[]{0,0}},
           {CauldronBuffTypes.CHEST_RATE_2_STARS, new int[]{0,0}}, {CauldronBuffTypes.REWARD_RATE_2_STARS, new int[]{0,0}},
		   {CauldronBuffTypes.CHEST_RATE_3_STARS, new int[]{0,0}}, {CauldronBuffTypes.REWARD_RATE_3_STARS, new int[]{0,0}},
		   {CauldronBuffTypes.FIRST_TURN_ACTION, new int[]{0,0}}, {CauldronBuffTypes.ACTION_POINTS, new int[]{0,0}}, 
		   {CauldronBuffTypes.SLOT_CHANGE_COST, new int[]{0,0}}
	};

	void Awake()
	{
		if (Instance != null && Instance != this)	Destroy(gameObject);
		else
		{
			Instance = this;
			DontDestroyOnLoad(gameObject);
		}

		playerBehaviourData.lifeCurrent = playerBehaviourData.lifeInitial;
		playerBehaviourData.actionPointsCurrent = playerBehaviourData.actionPointsInitial;
		playerBehaviourData.defenseCurrent = playerBehaviourData.defenseInitial;

		if (relicHolding != null)	SetRelic(relicHolding);
	}

	// Aplica dano ignorando o escudo
	public void TakePiercingDamage(int enemyAtk)
	{
		battleUIManagerScript.TakeDamage(enemyAtk);
		playerBehaviourData.lifeCurrent -= enemyAtk;
		healthBar.SetHealth(Mathf.Max(playerBehaviourData.lifeCurrent, 0));
		battleUIManagerScript.ShowLifePoints();

		if (playerBehaviourData.lifeCurrent <= 0)
		{
			GameManager.Instance.battleManager.LostBattle();
			battleUIManagerScript.PlayerLose();
		}
	}

	// Tira vida do jogador recebendo o ataque do inimigo
	public void TakeDamage(int enemyAtk)
	{
		int damageToTake = enemyAtk - playerBehaviourData.defenseCurrent;

		if (playerBehaviourData.defenseCurrent > 0)
		{
			playerBehaviourData.defenseCurrent -= enemyAtk;
			if (playerBehaviourData.defenseCurrent < 0)	playerBehaviourData.defenseCurrent = 0;
			battleUIManagerScript.ShowPlayerProtection();
		}

		if (damageToTake > 0)
		{
			battleUIManagerScript.TakeDamage(enemyAtk);
			playerBehaviourData.lifeCurrent -= damageToTake;
			healthBar.SetHealth(Mathf.Max(playerBehaviourData.lifeCurrent, 0));
			battleUIManagerScript.ShowLifePoints();
		}

		if (playerBehaviourData.lifeCurrent <= 0)
		{
			GameManager.Instance.battleManager.LostBattle();
			battleUIManagerScript.PlayerLose();
		}
	}

	// O jogador leva o Poison mas ainda não leva o dano
	public void TakePoison(int enemyPoison)
	{
		playerBehaviourData.poisonCurrent += enemyPoison;
	}

	// Chamada para causar o dano de poison no player
	public void TakePoisonDamage(int poison)
	{
		playerBehaviourData.lifeCurrent -= poison;
		healthBar.SetHealth(playerBehaviourData.lifeCurrent);
		battleUIManagerScript.ShowLifePoints();
		playerBehaviourData.poisonCurrent -= 1;
	}

	public void SetRelic(Relic newRelic)
	{
		relicHolding = newRelic;
		relicImage.sprite = newRelic.sprite;
	}

	public void ResetPlayerStatus()
	{
		playerBehaviourData.defenseCurrent = 0;
		playerBehaviourData.poisonCurrent = 0;
		playerBehaviourData.tirednessCurrent = 0;
		playerBehaviourData.tirednessTurns = 0;
		playerBehaviourData.inflationCurrent = 0;
		playerBehaviourData.curseCurrent = 0;
	}

	// Chamada para curar a vida do jogador
	public void HealLife(int toHeal)
	{
		playerBehaviourData.lifeCurrent += toHeal;

		if (playerBehaviourData.lifeCurrent > playerBehaviourData.lifeInitial)
			playerBehaviourData.lifeCurrent = playerBehaviourData.lifeInitial;

		healthBar.SetHealth(playerBehaviourData.lifeCurrent);
		battleUIManagerScript.ShowLifePoints();
	}

	public bool IsDead()
	{
		return (playerBehaviourData.lifeCurrent <= 0);
	}
}
