﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class DeckVisualizer : MonoBehaviour
{   
	[Header("Cards")]
	public DeckBehaviour deck; // deck do player

	[Header("General UI")]
	public GameObject panel; // painel com a lista das cartas
	public RectTransform content; // onde a lista vai ser inserida
	public RectTransform attackLabel; // etiqueta da secao de ataques
	public RectTransform habilityLabel; // etiqueta da secao de habilidades
	public RectTransform modifierLabel; // etiquetas da secao de modificadores

	public GameObject rowPrefab; // prefab da fileira utilizada
	private float heightOfRow; // altura da fileira usada

	private int attackRows = 0; // Quantas fileiras existem atualmente nessa secao
	private int cardsInAttackRow = 0; // Quantas cartas ha' na ultima fileira, 0 = fileira cheia (3)
	private int habilityRows = 0;
	private int cardsInHabilityRow = 0;
	private int modifierRows = 0;
	private int cardsInModifierRow = 0;
	// Lista dos CardStatus das cartas instanciadas dentro do visualizador
	public List<CardStatus> displayCardsStatus = new List<CardStatus>();

	public void Setup()
	{
		heightOfRow = rowPrefab.GetComponent<RectTransform>().rect.height; // Pega o tamanho da fileira usada
		// Adiciona todas as cartas
		foreach (GameObject card in deck.deckList)
		{
			AddCard(card);
		}
	}

	public void TogglePanel() // Desliga e liga o painel com a lista de cartas
	{
		panel.SetActive(!panel.activeInHierarchy);
	}

	public void AddCard(GameObject card)
	{
		switch (card.GetComponent<CardStatus>().kind)
		{
			case KindOfCard.ATTACK:
				if (AdditionCreatedNewRow(card, attackLabel, cardsInAttackRow, attackRows))
				{
					attackRows++; // Mais uma fileira
					// Empurrar a secao de habilidades para baixo
					habilityLabel.position -= new Vector3(0, heightOfRow, 0);
					// Empurrar a secao de modificadores para baixo
					modifierLabel.position -= new Vector3(0, heightOfRow, 0);
				}
				cardsInAttackRow = (cardsInAttackRow + 1) % 3;
				break;

			case KindOfCard.SKILL:
				if (AdditionCreatedNewRow(card, habilityLabel, cardsInHabilityRow, habilityRows))
				{
					habilityRows++; // Mais uma fileira
					// Empurrar a secao de modificadores para baixo
					modifierLabel.position -= new Vector3(0, heightOfRow, 0);
				}
				cardsInHabilityRow = (cardsInHabilityRow + 1) % 3;
				break;

			case KindOfCard.MODIFIER:
				if (AdditionCreatedNewRow(card, modifierLabel, cardsInModifierRow, modifierRows))
				{
					modifierRows++; // Mais uma fileira
				}
				cardsInModifierRow = (cardsInModifierRow + 1) % 3;
				break;

			default:
				Logging.Log("Carta temporária");
				break;
		}
	}

	private bool AdditionCreatedNewRow(GameObject cardPrefab, RectTransform label, int cardsInLastRow, int rows)
	{
		/* Essa funcao insere uma carta na secao indicada pela label,
		 * criando uma fileira nova caso necessario.
		 * Devolve True caso criou uma nova fileira e False caso contrario
		 */
		GameObject row;
		GameObject card;
		bool answer;

		switch (cardsInLastRow)
		{
			case 0: // Precisa criar outra fileira
				row = Instantiate(rowPrefab, label.position, label.rotation, label);
				Transform place1 = row.transform.GetChild(0).transform; // Primeiro lugar da fileira nova

				card = Instantiate(cardPrefab, place1.position, place1.rotation, place1); // Cria a carta no lugar certo
				//card.GetComponent<IDragger>().enabled = false; // desativa o IDragger
				//card.GetComponent<CanvasGroup>().blocksRaycasts = false;
				//card.GetComponent<EventTrigger>().enabled = false; // desativa efeitos ao clicar na carta
				// Coloca a carta na lsita interna
				displayCardsStatus.Add(card.GetComponent<CardStatus>());
				// Coloca a fileira inteira no lugar certo
				RectTransform rowTransform = row.GetComponent<RectTransform>();
				float offsetY = rows * heightOfRow + heightOfRow / 2 + label.rect.height;
				rowTransform.position = label.position - new Vector3(0, offsetY, 0);
				// Aumenta a zona com a lista das cartas
				content.sizeDelta += new Vector2(0, heightOfRow + label.rect.height);

				answer = true;
				break;

			case 1:
				row = label.GetChild(label.childCount - 1).gameObject; // Ultima fileira
				Transform place2 = row.transform.GetChild(1).transform; // Segundo lugar

				card = Instantiate(cardPrefab, place2.position, place2.rotation, place2);
				//card.GetComponent<IDragger>().enabled = false; // desativa o IDragger
				//card.GetComponent<CanvasGroup>().blocksRaycasts = false;
				//card.GetComponent<EventTrigger>().enabled = false; // desativa efeitos ao clicar na carta
				// Coloca a carta na lsita interna
				displayCardsStatus.Add(card.GetComponent<CardStatus>());

				answer = false;
				break;

			case 2:
				row = label.GetChild(label.childCount - 1).gameObject; // Ultima fileira
				Transform place3 = row.transform.GetChild(2).transform; // Terceiro lugar

				card = Instantiate(cardPrefab, place3.position, place3.rotation, place3);
				//card.GetComponent<IDragger>().enabled = false; // desativa o IDragger
				//card.GetComponent<CanvasGroup>().blocksRaycasts = false;
				//card.GetComponent<EventTrigger>().enabled = false; // desativa efeitos ao clicar na carta
				// Coloca a carta na lsita interna
				displayCardsStatus.Add(card.GetComponent<CardStatus>());

				answer = false;
				break;

			default:
				Logging.LogWarning("Incorrect number of cards in last row");
				return false;
		}

		// Configura tudo que precisa para fazer upgrade nas cartas do visualizador
		card.GetComponent<CardStatus>().Setup();
		return answer;
	}
}
